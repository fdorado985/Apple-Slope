# Binary

On this app we'll be looking, how the computer works converting
binary to decimal and viceversa.

## Demo
![binary_demo](https://raw.githubusercontent.com/fdorado985/Apple-Slope/master/Projects/binary/screenshots/binary_demo.gif)
