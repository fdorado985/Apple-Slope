//
//  MainVC.swift
//  binary
//
//  Created by Juan Francisco Dorado Torres on 11/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var txtValueEntry: UITextField!
  @IBOutlet weak var btnBinary: UIButton!
  @IBOutlet weak var btnDecimal: UIButton!

  // MARK: - Properties

  let placeholder = NSAttributedString(
    string: "Enter a value...",
    attributes: [
      NSAttributedStringKey.foregroundColor : UIColor.lightGray,
      NSAttributedStringKey.font : UIFont(name: "Menlo", size: 36.0)!
    ]
  )

  var isFieldEmpty: Bool {
    return txtValueEntry.text == ""
  }

  // MARK: - MainVC Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    txtValueEntry.attributedPlaceholder = placeholder
    txtValueEntry.addTarget(self, action: #selector(textFieldTextDidChange), for: .editingChanged)
    enableButtons()
  }

  // MARK: - IBActions

  @IBAction func btnDecimalTapped(_ sender: UIButton) {
    if !isFieldEmpty {
      btnBinary.alpha = 0.5
      btnDecimal.alpha = 1.0
      guard let txtBits = txtValueEntry.text else { return }
      let arrBits = txtBits.map{ Int(String($0))! }
      let binaryDigit = BinaryDecimal(bits: arrBits)
      txtValueEntry.text = "\(binaryDigit.calculateIntValueForBinary())"
    }
  }

  @IBAction func btnBinaryTapped(_ sender: UIButton) {
    if !isFieldEmpty {
      btnBinary.alpha = 1.0
      btnDecimal.alpha = 0.5
      guard let txtDecimal = txtValueEntry.text, let intDecimal = Int(txtDecimal) else { return }
      let binaryDigit = BinaryDecimal(decimal: intDecimal)
      txtValueEntry.text = "\(binaryDigit.calculateBinaryValueForInt())"
    }
  }

  // MARK: - Functions

  @objc private func textFieldTextDidChange() {
    enableButtons()
  }

  private func enableButtons() {
    let isTextEmpty = txtValueEntry.text == ""
    btnBinary.isEnabled = !isTextEmpty
    btnDecimal.isEnabled = !isTextEmpty
    btnBinary.alpha = isTextEmpty ? 0.5 : 1.0
    btnDecimal.alpha = isTextEmpty ? 0.5 : 1.0
  }
}

