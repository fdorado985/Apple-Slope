//
//  BinaryDecimal.swift
//  binary
//
//  Created by Juan Francisco Dorado Torres on 11/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct BinaryDecimal {

  private var bits: [Int]?
  private var integer: Int?

  init(bits: [Int]? = nil, decimal: Int? = nil) {
    self.bits = bits
    self.integer = decimal
  }

  func calculateBinaryValueForInt() -> String {
    guard var integer = integer else { return "" }
    let rows = [128, 64, 32, 16, 8, 4, 2, 1]
    var binaryRows: [Int] = []
    for row in rows {
      let binaryDigit = oneOrZero(for: integer, with: row)
      binaryRows.append(binaryDigit)
      integer = integer - row * binaryDigit
    }
    return binaryRows.map{ String($0) }.joined()
  }

  func calculateIntValueForBinary() -> String {
    var value = 0
    var multiplier = 1
    guard let bits = bits?.reversed() else { return "" }
    for bit in bits {
      let newValue = bit * multiplier
      value = value + newValue
      multiplier = multiplier * 2
    }
    return String(value)
  }

  func oneOrZero(for value: Int, with bitValue: Int) -> Int {
    return value - bitValue >= 0 ? 1 : 0
  }
}
