//
//  ProductCell.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 16/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

  // MARK: - IBOutlets

  @IBOutlet weak var productImage: UIImageView!
  @IBOutlet weak var productTitle: UILabel!
  @IBOutlet weak var productPrice: UILabel!

  // MARK: - Functions

  func updateViews(product: Product) {
    productImage.image = UIImage(named: product.imageName)
    productTitle.text = product.title
    productPrice.text = product.price
  }
}
