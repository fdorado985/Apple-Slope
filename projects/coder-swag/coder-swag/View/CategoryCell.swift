//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 15/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var imgCategory: UIImageView!
  @IBOutlet weak var lbTitle: UILabel!

  // MARK: - Functions

  func updateViews(category: Category) {
    imgCategory.image = UIImage(named: category.imgName)
    lbTitle.text = category.title
  }
}
