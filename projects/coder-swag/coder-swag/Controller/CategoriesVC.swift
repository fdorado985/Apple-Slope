//
//  ViewController.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 15/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CategoriesVC: UIViewController {
  
  // MARK: - IBOutlets
  
  @IBOutlet weak var tblCategories: UITableView!
  
  // MARK: - Initialization
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: - Perform Segue

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let productsVC = segue.destination as? ProductsVC,
      let category = sender as? Category {
      productsVC.initProducts(category: category)

      let barBtnItem = UIBarButtonItem()
      barBtnItem.title = ""
      navigationItem.backBarButtonItem = barBtnItem
    }
  }
}

// MARK: - UITableViewDelegate

extension CategoriesVC: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return DataService.instance.getCategories().count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as? CategoryCell {
      let category = DataService.instance.getCategories()[indexPath.row]
      cell.updateViews(category: category)
      return cell
    }
    return CategoryCell()
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let category = DataService.instance.getCategories()[indexPath.row]
    performSegue(withIdentifier: "ProductsVC", sender: category)
  }
}

// MARK: - UITableViewDataSource

extension CategoriesVC: UITableViewDataSource {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 150
  }
}
