//
//  ProductsVC.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 17/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var productsCollection: UICollectionView!

  // MARK: - Properties

  private(set) public var products = [Product]()

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  func initProducts(category: Category) {
    products = DataService.instance.getProducts(forCategoryType: category.type)
    navigationItem.title = category.title
  }

  // MARK: - Functions
}

// MARK: - UICollectionViewDelegate

extension ProductsVC: UICollectionViewDelegate {

}

// MARK: - UICollectionViewDataSource

extension ProductsVC: UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return products.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductCell {
      let product = products[indexPath.row]
      cell.updateViews(product: product)
      return cell
    }
    return ProductCell()
  }
}
