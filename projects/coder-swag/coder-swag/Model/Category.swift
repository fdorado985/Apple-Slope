//
//  Category.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 16/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Category {

  private(set) public var title: String
  private(set) public var imgName: String
  private(set) public var type: CategoryType

  init(title: String, imgName: String, type: CategoryType) {
    self.title = title
    self.imgName = imgName
    self.type = type
  }
}
