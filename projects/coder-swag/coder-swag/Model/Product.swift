//
//  Product.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 16/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Product {

  // MARK: - Properties

  private(set) public var title: String
  private(set) public var price: String
  private(set) public var imageName: String

  // MARK: - Initialization

  init(title: String, price: String, imageName: String) {
    self.title = title
    self.price = price
    self.imageName = imageName
  }

  // MARK: - Functions
}
