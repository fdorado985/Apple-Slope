//
//  DataService.swift
//  coder-swag
//
//  Created by Juan Francisco Dorado Torres on 16/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum CategoryType {
  case shirts
  case hats
  case hoodies
  case digitalGoods
}

class DataService {

  // MARK: - Singleton

  static let instance = DataService()

  // MARK: - Properties

  private let categories = [
    Category(title: "SHIRTS", imgName: "shirts.png", type: .shirts),
    Category(title: "HOODIES", imgName: "hoodies.png", type: .hoodies),
    Category(title: "HATS", imgName: "hats.png", type: .hats),
    Category(title: "DIGITAL", imgName: "digital.png", type: .digitalGoods),
    ]

  private let hats = [
    Product(title: "Devslopes Logo Graphic Beanie", price: "$18", imageName: "hat01.png"),
    Product(title: "Devslopes Logo Hat Black", price: "$22", imageName: "hat02.png"),
    Product(title: "Devslopes Logo Hat White", price: "$22", imageName: "hat03.png"),
    Product(title: "Devslopes Logo Snapback", price: "$20", imageName: "hat04.png")
  ]

  private let hoodies = [
    Product(title: "Devslopes Logo Hoodie Grey", price: "$32", imageName: "hoodie01.png"),
    Product(title: "Devslopes Logo Hoodie Red", price: "$32", imageName: "hoodie02.png"),
    Product(title: "Devslopes Logo Hoodie Grey", price: "$32", imageName: "hoodie03.png"),
    Product(title: "Devslopes Logo Hoodie Black", price: "$32", imageName: "hoodie04.png")
  ]

  private let shirts = [
    Product(title: "Devslopes Logo Shirt Black", price: "$18", imageName: "shirt01.png"),
    Product(title: "Devslopes Logo Shirt Light Grey", price: "$18", imageName: "shirt02.png"),
    Product(title: "Devslopes Logo Shirt Red", price: "$18", imageName: "shirt03.png"),
    Product(title: "Hustle Delegate Grey", price: "$18", imageName: "shirt04.png"),
    Product(title: "Kickflip Studios Black", price: "$18", imageName: "shirt05.png")
  ]

  private let digitalGoods = [Product]()

  // MARK: - Functions

  func getCategories() -> [Category] {
    return categories
  }

  func getProducts(forCategoryType title: CategoryType) -> [Product] {
    switch title {
    case .shirts:
      return getShirts()
    case .hats:
      return getHats()
    case .hoodies:
      return getHoodies()
    case .digitalGoods:
      return getDigitalGoods()
    }
  }

  func getHats() -> [Product] {
    return hats
  }

  func getHoodies() -> [Product] {
    return hoodies
  }

  func getShirts() -> [Product] {
    return shirts
  }

  func getDigitalGoods() -> [Product] {
    return digitalGoods
  }
}
