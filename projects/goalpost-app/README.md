# GoalPost-App

On this app, we work with `CoreData` for iOS 11, to create a goalApp... this will persist some data that are your goals, and will save how many points do are you getting to reach your goal.

## Demo
![Demo-01](screenshots/goal-post-demo-01.gif)

![Demo-02](goal-post-demo-02.gif)
