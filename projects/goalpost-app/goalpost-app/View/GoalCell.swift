//
//  GoalCell.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 24/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class GoalCell: UITableViewCell {

  // MARK: - IBOutlets

  @IBOutlet weak var lblDescription: UILabel!
  @IBOutlet weak var lblType: UILabel!
  @IBOutlet weak var lblProgress: UILabel!
  @IBOutlet weak var completionView: UIView!

  // MARK: - Configure Cell

  func configureCell(goal: Goal) {
    lblDescription.text = goal.goalDescription
    lblType.text = goal.goalType
    lblProgress.text = "\(goal.goalProgress)"

    completionView.isHidden = goal.goalProgress != goal.goalCompletionValue
  }
}
