//
//  FinishGoalVC.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 28/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class FinishGoalVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var txtPoints: UITextField!
  @IBOutlet weak var btnCreateGoal: UIButton!

  // MARK: - Private Properties

  var goalDescription: String?
  var goalType: GoalType?

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    btnCreateGoal.bindToKeyboard()
  }

  // MARK: - IBActions

  @IBAction func btnCreateGoalWasPressed(_ sender: UIButton) {
    save { (finished) in
      if finished {
        self.navigationController?.popToRootViewController(animated: true)
      }
    }
  }

  // MARK: - Functions

  func initData(description: String,  type: GoalType) {
    self.goalDescription = description
    self.goalType = type
  }

  // MARK: - CoreData Functions

  private func save(completion: (_ finish: Bool) -> ()) {
    guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
    let goal = Goal(context: managedContext)

    goal.goalDescription = goalDescription
    goal.goalType = goalType?.rawValue
    goal.goalCompletionValue = Int32(txtPoints.text ?? "0") ?? 0
    goal.goalProgress = Int32(0)

    do {
      try managedContext.save()
      completion(true)
    } catch {
      debugPrint("Could not save: \(error.localizedDescription)")
      completion(false)
    }
  }
}

extension FinishGoalVC: UITextFieldDelegate {
  
}
