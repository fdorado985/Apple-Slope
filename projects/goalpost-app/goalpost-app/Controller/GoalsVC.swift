//
//  GoalsVC.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 23/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate

class GoalsVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var tableGoals: UITableView!

  // MARK: - Properties

  private let reuseIdentifierGoalCell = "goalCell"
  private let storyboardID_CreateGoalVC = "CreateGoalVC"
  private let segueID_CreateGoalVC = "CreateGoalVC"
  private let goalEntityName = "Goal"

  private var goals: [Goal] = []

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    addRightBarButtons()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    fetchCoreDataObject()
    tableGoals.reloadData()
  }

  // MARK: - Private Functions

  private func addRightBarButtons() {
    self.navigationController?.navigationBar.tintColor = UIColor.white
    let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addGoal))
    addButton.tintColor = UIColor.white
    self.navigationItem.rightBarButtonItem = addButton
  }

  // MARK: - OBJC Private Functions

  @objc private func addGoal() {
    guard let createGoalVC = storyboard?.instantiateViewController(withIdentifier: storyboardID_CreateGoalVC) else { return }
    presentDetail(createGoalVC)
  }
}

// MARK: - UITableView Delegate/DataSource

extension GoalsVC: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return goals.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierGoalCell, for: indexPath) as? GoalCell else {
      return UITableViewCell()
    }
    let goal = goals[indexPath.row]
    cell.configureCell(goal: goal)
    return cell
  }

  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    return .none
  }

  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
      self.removeGoal(at: indexPath)
      self.fetchCoreDataObject()
      tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    let addAction = UITableViewRowAction(style: .normal, title: "ADD +1") { (rowAction, indexPath) in
      self.setProgress(at: indexPath)
      tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    deleteAction.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
    addAction.backgroundColor = #colorLiteral(red: 0.4274509804, green: 0.737254902, blue: 0.3882352941, alpha: 1)
    return [deleteAction, addAction]
  }
}

// MARK: - Core Data

extension GoalsVC {

  private func setProgress(at indexPath: IndexPath) {
    guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
    let chosenGoal = goals[indexPath.row]
    if chosenGoal.goalProgress < chosenGoal.goalCompletionValue {
      chosenGoal.goalProgress += 1
    } else {
      return
    }

    do {
      try managedContext.save()
    } catch {
      debugPrint("Could not setProgress: \(error.localizedDescription)")
    }
  }

  private func fetchCoreDataObject() {
    fetch { (completed) in
      if completed {
        tableGoals.isHidden = goals.count == 0
      }
    }
  }

  private func fetch(completion: (_ complete: Bool) -> ()) {
    guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
    let fetchRequest = NSFetchRequest<Goal>(entityName: goalEntityName)
    do {
      goals = try managedContext.fetch(fetchRequest)
      completion(true)
    } catch {
      debugPrint("Could not fetch: \(error.localizedDescription)")
      completion(false)
    }
  }

  private func removeGoal(at indexPath: IndexPath) {
    guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
    managedContext.delete(goals[indexPath.row])
    do {
      try managedContext.save()
    } catch {
      debugPrint("Could not remove: \(error.localizedDescription)")
    }
  }
}
