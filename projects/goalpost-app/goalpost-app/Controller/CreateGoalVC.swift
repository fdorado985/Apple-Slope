//
//  CreateGoalVC.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 24/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CreateGoalVC: UIViewController {

  // MARK: - IBOutlets
  
  @IBOutlet weak var txtGoal: UITextView!
  @IBOutlet weak var btnShortTerm: UIButton!
  @IBOutlet weak var btnLongTerm: UIButton!
  @IBOutlet weak var btnNext: UIButton!

  // Properties
  private var goalType: GoalType = .shortTerm
  private var txtGoalPlaceholder = "What is your goal?"
  private var storyboardID_FinishGoalVC = "FinishGoalVC"

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    btnNext.bindToKeyboard()
    btnShortTerm.setSelectedColor()
    btnLongTerm.setDeselectedColor()
  }

  // MARK: - IBActions

  @IBAction func btnShortTermWasPressed(_ sender: UIButton) {
    goalType = .shortTerm
    btnShortTerm.setSelectedColor()
    btnLongTerm.setDeselectedColor()
  }

  @IBAction func btnLongTermWasPressed(_ sender: UIButton) {
    goalType = .longTerm
    btnShortTerm.setDeselectedColor()
    btnLongTerm.setSelectedColor()
  }

  @IBAction func btnNextWasPressed(_ sender: UIButton) {
    if txtGoal.text != "" && txtGoal.text != txtGoalPlaceholder {
      guard let finishGoalVC = storyboard?.instantiateViewController(withIdentifier: storyboardID_FinishGoalVC) as? FinishGoalVC else { return }
      finishGoalVC.initData(description: txtGoal.text, type: goalType)
      presentDetail(finishGoalVC)
    }
  }
}

extension CreateGoalVC: UITextViewDelegate {

  func textViewDidBeginEditing(_ textView: UITextView) {
    txtGoal.text = ""
    txtGoal.textColor = UIColor.black
  }
}
