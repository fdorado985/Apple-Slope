//
//  UIViewExt.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 24/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIView {

  func bindToKeyboard() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(keyboardWillChange(_:)),
      name: NSNotification.Name.UIKeyboardWillChangeFrame,
      object: nil
    )
  }

  @objc func keyboardWillChange(_ notification: NSNotification) {
    guard let userInfo = notification.userInfo,
      let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
      let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
      let startingFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
      let endingFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }

    let deltaY = endingFrame.origin.y - startingFrame.origin.y
    UIView.animateKeyframes(
      withDuration: duration,
      delay: 0.0,
      options: UIViewKeyframeAnimationOptions(rawValue: curve),
      animations: {
        self.frame.origin.y += deltaY
      },
      completion: nil
    )
  }
}
