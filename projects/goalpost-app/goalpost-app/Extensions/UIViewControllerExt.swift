//
//  UIViewControllerExt.swift
//  goalpost-app
//
//  Created by Juan Francisco Dorado Torres on 24/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIViewController {

  func presentDetail(_ viewControllerToPresent: UIViewController) {
    let transition = CATransition()
    transition.duration = 0.3
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromRight
    self.view.window?.layer.add(transition, forKey: kCATransition)
    navigationController?.pushViewController(viewControllerToPresent, animated: false)
    //present(viewControllerToPresent, animated: false, completion: nil)
  }

  func dismissDetail() {
    let transition = CATransition()
    transition.duration = 0.3
    transition.type = kCATransitionPush
    transition.subtype = kCATransitionFromLeft
    self.view.window?.layer.add(transition, forKey: kCATransition)
    dismiss(animated: false, completion: nil)
  }
}
