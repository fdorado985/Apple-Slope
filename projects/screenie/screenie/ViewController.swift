//
//  ViewController.swift
//  screenie
//
//  Created by Juan Francisco Dorado Torres on 11/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import ReplayKit

class ViewController: UIViewController {
    
    // MARK: IBActions
    
    @IBOutlet weak private var lblStatus: UILabel!
    @IBOutlet weak private var segmentImagePicker: UISegmentedControl!
    @IBOutlet weak private var imgSelected: UIImageView!
    @IBOutlet weak private var switchMic: UISwitch!
    @IBOutlet weak private var btnRecord: UIButton!
    
    // MARK: Properties
    
    private var recorder = RPScreenRecorder.shared()
    private var isRecording = false
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBActions
    
    @IBAction func imagePickedValueChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            imgSelected.image = UIImage(named: "skate")!
        case 1:
            imgSelected.image = UIImage(named: "food")!
        case 2:
            imgSelected.image = UIImage(named: "cat")!
        case 3:
            imgSelected.image = UIImage(named: "nature")!
        default:
            imgSelected.image = UIImage(named: "skate")!
        }
    }
    
    @IBAction func btnRecordTapped(_ sender: Any) {
        if !isRecording {
            startRecording()
        } else {
            stopRecording()
        }
    }
    
    // MARK: Functions
    
    private func startRecording() {
        guard recorder.isAvailable else {
            debugPrint("Recording is not available at this time")
            return
        }
        
        recorder.isMicrophoneEnabled = switchMic.isOn
        recorder.startRecording { (error) in
            if let error = error {
                debugPrint("\(#function): \(error.localizedDescription)")
                return
            }
            
            debugPrint("STARTED RECORDING SUCCESS")
            DispatchQueue.main.async {
                self.switchMic.isEnabled = false
                self.btnRecord.setTitleColor(UIColor.red, for: .normal)
                self.btnRecord.setTitle("STOP", for: .normal)
                self.lblStatus.text = "Recording..."
                self.lblStatus.textColor = UIColor.red
                self.isRecording = true
            }
        }
    }
    
    private func stopRecording() {
        recorder.stopRecording { (preview, error) in
            guard let preview = preview else {
                debugPrint("Preview controller is not available")
                return
            }
            
            let alert = UIAlertController(title: "Recording Finished", message: "Would you like to edit or delete your recording?", preferredStyle: .alert)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.recorder.discardRecording {
                    debugPrint("Recording successfully discarded!")
                }
            })
            let editAction = UIAlertAction(title: "Edit", style: .default, handler: { (action) in
                preview.previewControllerDelegate = self
                self.present(preview, animated: true)
            })
            alert.addAction(deleteAction)
            alert.addAction(editAction)
            self.present(alert, animated: true)
            
            DispatchQueue.main.async {
                self.viewReset()
            }
        }
    }
    
    private func viewReset() {
        self.isRecording = false
        switchMic.isEnabled = true
        lblStatus.text = "Ready To Record"
        lblStatus.textColor = UIColor.black
        btnRecord.setTitle("Record", for: .normal)
        btnRecord.setTitleColor(UIColor.blue, for: .normal)
    }
}

extension ViewController: RPPreviewViewControllerDelegate {
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
}
