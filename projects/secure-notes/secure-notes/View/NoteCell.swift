//
//  NoteCell.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak private var lblMessage: UILabel!
    @IBOutlet weak private var imgLock: UIImageView!
    
    // MARK: Functions
    
    func configureCell(note: Note) {
        switch note.lockStatus {
        case .locked:
            lblMessage.text = "This note is locked. Unlock to read."
            imgLock.isHidden = false
        case .unlocked:
            lblMessage.text = note.message
            imgLock.isHidden = true
        }
    }
}
