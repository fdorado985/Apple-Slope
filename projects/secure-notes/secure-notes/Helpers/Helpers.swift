//
//  Helpers.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

func isNoteLocked(_ lockStatus: LockStatus) -> Bool {
    return lockStatus == .locked
}

func lockStatusFlipper(_ lockStatus: LockStatus) -> LockStatus {
    return lockStatus == .locked ? .unlocked : .locked
}
