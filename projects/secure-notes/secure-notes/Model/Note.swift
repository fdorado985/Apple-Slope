//
//  Note.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Note {
    
    // MARK: Properties
    
    private(set) var message: String
    private(set) var lockStatus: LockStatus
    
    // MARK: Init
    
    init(message: String, lockStatus: LockStatus) {
        self.message = message
        self.lockStatus = lockStatus
    }
    
    // MARK: Functions
    
    mutating func setLockStatus(to status: LockStatus) {
        self.lockStatus = status
    }
}
