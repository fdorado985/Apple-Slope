//
//  ViewController.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import LocalAuthentication

class NoteListVC: UITableViewController {

    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: Functions
    
    private func authenticateBiometrics(_ completion: @escaping (Bool) -> Void) {
        let context = LAContext()
        let localizedReasonString = "Our app uses TouchID|FaceID to secure your notes."
        var authError: NSError?
        
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReasonString) { (success, evaluateError) in
                    if success {
                        completion(true)
                    } else {
                        guard let errorMessage = evaluateError?.localizedDescription else { return }
                        self.showAlert("Error", message: errorMessage)
                        completion(false)
                    }
                }
            } else {
                guard let errorMessage = authError?.localizedDescription else { return }
                showAlert(message: errorMessage)
                completion(false)
            }
        } else {
            completion(false)
        }
        
    }
    
    private func showAlert(_ title: String = "Secure Notes", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let btnAcceptAction = UIAlertAction(title: "Accept", style: .default)
        alert.addAction(btnAcceptAction)
        present(alert, animated: true)
    }
    
    private func pushNote(_ note: Note, at index: Int) {
        let note = (note, index)
        performSegue(withIdentifier: "NoteDetailsVC", sender: note)
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NoteDetailsVC",
            let note = sender as? (Note, Int),
            let vc = segue.destination as? NoteDetailsVC {
            vc.note = note.0
            vc.index = note.1
        }
    }
}

// MARK: UITableView Delegate|Datasource
extension NoteListVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath) as? NoteCell else { return UITableViewCell() }
        let note = notesArray[indexPath.row]
        cell.configureCell(note: note)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        let selectedNote = notesArray[index]
        
        if selectedNote.lockStatus == .locked {
            authenticateBiometrics { (authenticated) in
                if authenticated {
                    let lockStatus = selectedNote.lockStatus
                    let flippedLockStatus = lockStatusFlipper(lockStatus)
                    notesArray[indexPath.row].setLockStatus(to: flippedLockStatus)
                    
                    DispatchQueue.main.async {
                        self.pushNote(notesArray[indexPath.row], at: index)
                    }
                }
            }
        } else {
            pushNote(selectedNote, at: index)
        }
    }
}

