//
//  NoteDetailsVC.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class NoteDetailsVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtMessage: UITextView!
    
    // MARK: Properties
    
    var note: Note?
    var index: Int?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    // MARK: IBActions
    
    @IBAction func btnLockTapped(_ sender: UIBarButtonItem) {
        guard let index = index, let note = note else { return }
        let lockStatus = lockStatusFlipper(note.lockStatus)
        notesArray[index].setLockStatus(to: lockStatus)
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: Private
    
    private func setupView() {
        guard let note = note else { return }
        txtMessage.text = note.message
    }
    
}
