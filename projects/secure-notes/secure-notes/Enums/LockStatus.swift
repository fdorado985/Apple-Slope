//
//  LockStatus.swift
//  secure-notes
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum LockStatus {
    case locked, unlocked
}
