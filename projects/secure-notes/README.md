#  Secure Notes

This is a simple Notes app where you'll be able to see how works the `Biometrics` - `TouchID` | `FaceID`

## Demo
![secure_notes_biometrics](screenshots/secure_notes_demo.gif)
