# XYZ Machine

On this app we are gonna be looking how accelerometer works, easy way just to see the `x`, `y` and `z` values changing when we move our device.

## Preview

![xyz-machine](screenshots/xyz-machine.png)
