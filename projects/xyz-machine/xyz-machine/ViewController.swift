//
//  ViewController.swift
//  xyz-machine
//
//  Created by Juan Francisco Dorado Torres on 20/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {

  // MARK: - IBOutlet

  @IBOutlet weak var lblX: UILabel!
  @IBOutlet weak var lblY: UILabel!
  @IBOutlet weak var lblZ: UILabel!

  // MARK: - Properties

  var motionManager: CMMotionManager!

  // MARK: - ViewController ViewCycle

  override func viewDidLoad() {
    super.viewDidLoad()
    motionManager = CMMotionManager()
    motionManager.startAccelerometerUpdates(to: .main, withHandler: updateLabels)
  }

  // MARK: - Functions

  private func updateLabels(data: CMAccelerometerData?, error: Error?) {
    guard let accelerometerData = data else { return }

    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = 1
    formatter.maximumFractionDigits = 1

    guard let x = formatter.string(for: accelerometerData.acceleration.x),
      let y = formatter.string(for: accelerometerData.acceleration.y),
      let z = formatter.string(for: accelerometerData.acceleration.z) else {
        return
    }

    lblX.text = "X: \(x)"
    lblY.text = "Y: \(y)"
    lblZ.text = "Z: \(z)"
  }
}

