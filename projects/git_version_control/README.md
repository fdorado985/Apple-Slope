# Git & Version Control

## Table of Content
* [Terminal Basics](#terminal-basics)
  * [Change Directories](#change-directories)
  * [Short Listing](#change-directories)
  * [Creating Directories](#change-directories)
  * [Creating Files](#change-directories)
  * [Move Files](#change-directories)
  * [Copy Files](#change-directories)
  * [Rename Files](#change-directories)
  * [Remove Files](#change-directories)
  * [Remove an Empty Directory](#change-directories)
  * [Remove a Directory with Files](#change-directories)
* [Git Basics](#git-basics)
  * [Initialize Git](#initialize-git)
  * [Tracking Status](#tracking-status)
  * [Log](#log)
  * [Return To a Previous Commit](#return-to-a-previous-commit)
  * [Create branch](#create-branch)

## Terminal basics
### Change Directories
* `cd` : Go to the Root directory.
* `cd ..` : Go back one directory.
* `cd [folder]` : Go to a specific directory.

### Short Listing
* `ls` : Show the content of the current directory
* `ls -l` : Show the content in list form of the current directory.

### Creating Directories
* `mkdir [folder_name]` : Creates a new folder with the given name.

### Creating Files
* `touch [file_name]` : Creates a new file with the given name... remember to add the extension.

### Move Files
* `mv [file] [dir]` : Move a file to a new path.

### Copy Files
* `cp [file] [dir]` : Copy a file to a new path.

### Rename Files
* `mv [file] [new_file_name]` : Rename a file with the new file_name

### Remove Files
* `rm [file]` : Remove a given file_name

### Remove an Empty Directory
* `rm [dir]` : Remove a given folder name

### Remove a Directory with Files
* `rm -R [dir]` : Remove a directory with files on it.  

## Git Basics

### Initialize Git
* `git init` : Initialize a git repo (locally) in your current path.

### Tracking Status
* `git status` : Gives you the current status of your repo... modified, added, deleted, not tracked and tracked files/folders.

### Add Files
* `git add [. | file]` : Add the files or all files to your tracking git.

### Commit
* `git commit -m [message]` : Save the current tracked files on your git with a given message

### Log
* `git log` : Gives you the current log of all commits you have made.

### Return To a Previous Commit
* `git checkout [first 7 chars of id commit]` : It returns you to the state of the given id commit.

### Create branch
* `git checkout -b [branch_name]` : It changes you to a new branch with the given name... and brings all the current not commited changes.
