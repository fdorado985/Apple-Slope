# Swoosh

On this app we are going to see how work with more than 1 viewcontroller, also we are going to see how to use the constraints and autolayouts including with this stackviews, some of data modeling and how to send data between viewcontrollers.

## Demo
![swoosh_demo](screenshots/demo_swoosh.gif)

## Pro Tips

> As a minimum, Always make @2x and
> @3x images for ALL images in your apps.

> Never promise pixel perfect. iOS is not
> designed for it and it is very expensive to
> achieve.
> It can take 2-3x the hours and increase the
> cost of work by 2-3x.

> D.R.Y. - Don't repeat yourself!.
> If you are writting the same code over and
> over again, you are doing it WRONG.

> Instead of `class` use `struct` wherever you can!.
> It is much faster!

> `prepareForSegue` is always called before
> `viewDidLoad` (on the destination view controller).

> ```swift
> var myData: String! // Implicity unwrapped optional
> ```
> Use when you can guarantee variable will have data in it before used
> ```swift
> var myData: String? // Optional
> ```
> Use when there may or may not be data in the variable at runtime
