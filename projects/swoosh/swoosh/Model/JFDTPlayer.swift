//
//  JFDTPlayer.swift
//  swoosh
//
//  Created by Juan Francisco Dorado Torres on 29/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Player {

  var desiredLeague: String?
  var selectedSkillLevel: String?
}
