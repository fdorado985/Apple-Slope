//
//  JFDTBorderButton.swift
//  swoosh
//
//  Created by Juan Francisco Dorado Torres on 29/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JFDTBorderButton: UIButton {

  // MARK: - Initialization

  override func awakeFromNib() {
    super.awakeFromNib()
    borderLine()
  }

  // MARK: - Private Functions

  private func borderLine() {
    layer.borderWidth = 2.0
    layer.borderColor = UIColor.white.cgColor
  }
}
