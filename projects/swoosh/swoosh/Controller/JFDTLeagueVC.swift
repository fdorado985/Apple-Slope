//
//  JFDTLeagueVC.swift
//  swoosh
//
//  Created by Juan Francisco Dorado Torres on 29/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JFDTLeagueVC: UIViewController {

  // MARK: - Properties

  var player: Player!

  // MARK: - IBOutlets

  @IBOutlet weak var btnNext: JFDTBorderButton!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()

    player = Player()
  }

  // MARK: - IBActions

  @IBAction func btnMenPressed(_ sender: JFDTBorderButton) {
    selectLeague("men")
  }

  @IBAction func btnWomenPressed(_ sender: JFDTBorderButton) {
    selectLeague("women")
  }

  @IBAction func btnCoedPressed(_ sender: JFDTBorderButton) {
    selectLeague("coed")
  }
  
  @IBAction func btNextPressed(_ sender: UIButton) {
    self.performSegue(withIdentifier: "JFDTSkillVCSegue", sender: nil)
  }

  @IBAction func unwindFromSkillVC(unwindSegue: UIStoryboardSegue) {}

  // MARK: - Functions

  private func selectLeague(_ leagueType: String) {
    player.desiredLeague = leagueType
    btnNext.isEnabled = true
  }

  // MARK - Prepare For Segue

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let skillVC = segue.destination as? JFDTSkillVC {
      skillVC.player = player
    }
  }
}
