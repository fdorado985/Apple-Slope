//
//  JFDTSkillVC.swift
//  swoosh
//
//  Created by Juan Francisco Dorado Torres on 29/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JFDTSkillVC: UIViewController {

  // MARK: - Properties

  var player: Player!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()

    if let desiredLeague = player.desiredLeague {
      print(desiredLeague)
    }
  }
}
