//
//  JFDTWelcomeVC.swift
//  swoosh
//
//  Created by Juan Francisco Dorado Torres on 29/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JFDTWelcomeVC: UIViewController {

  // MARK: - IBOutlets

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: - IBActions

  @IBAction func unwindFromSkillVC(unwindSegue: UIStoryboardSegue) {}
}

