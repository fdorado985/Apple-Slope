//
//  InputVC.swift
//  todo_tdd
//
//  Created by Juan Francisco Dorado Torres on 11/12/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreLocation

class InputVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    // MARK: Properties
    
    lazy var geocoder = CLGeocoder()
    var itemManager: ItemManager?
    let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter
    }()

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Functions
    
    @IBAction func save() {
        guard let titleString = txtTitle.text, !titleString.isEmpty else { return }
        
        let date: Date?
        if let dateText = self.txtDate.text, !dateText.isEmpty {
            date = dateFormatter.date(from: dateText)
        } else {
            date = nil
        }
        
        let descriptionString = txtDescription.text
        if let locationName = txtLocation.text, !locationName.isEmpty {
            if let address = txtAddress.text, !address.isEmpty {
                geocoder.geocodeAddressString(address) { [unowned self] (placemarks, error) in
                    let placemark = placemarks?.first
                    
                    let item = Item(title: titleString, description: descriptionString, timestamp: date?.timeIntervalSince1970, location: Location(name: locationName, coordinate: placemark?.location?.coordinate))
                    
                    DispatchQueue.main.async(execute: {
                        self.itemManager?.add(item)
                        self.dismiss(animated: true)
                    })
                }
            } else {
                let item = Item(title: titleString, description: descriptionString, timestamp: date?.timeIntervalSinceNow, location: nil)
                self.itemManager?.add(item)
                dismiss(animated: true)
            }
        } else {
            let item = Item(title: titleString, description: descriptionString, timestamp: date?.timeIntervalSinceNow, location: nil)
            self.itemManager?.add(item)
            dismiss(animated: true)
        }
    }

}
