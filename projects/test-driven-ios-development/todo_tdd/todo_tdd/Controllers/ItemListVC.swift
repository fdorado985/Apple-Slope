//
//  ItemListVC.swift
//  todo_tdd
//
//  Created by Juan Francisco Dorado Torres on 10/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ItemListVC: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var dataProvider: (UITableViewDelegate & UITableViewDataSource & ItemManagerSettable)!
    
    // MARK: Properties
    
    let itemManager = ItemManager()

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
        dataProvider.itemManager = itemManager
        
        NotificationCenter.default.addObserver(self, selector: #selector(showDetails(_:)), name: Notification.Name("ItemSelectedNotification"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: IBActions
    
    @IBAction func btnAddItem(_ sender: UIBarButtonItem) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "InputVC") as? InputVC {
            nextVC.itemManager = itemManager
            present(nextVC, animated: true)
        }
    }
    
    // MARK: Functions
    
    @objc func showDetails(_ notification: Notification) {
        guard let index = notification.userInfo?["index"] as? Int else { fatalError() }
        
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "DetailVC") as? DetailVC {
            nextVC.itemInfo = (itemManager, index)
            navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}

