//
//  StoryboardTests.swift
//  todo_tddTests
//
//  Created by Juan Francisco Dorado Torres on 11/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest

@testable import todo_tdd
class StoryboardTests: XCTestCase {

    // MARK: Test cycle
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Tests
    
    func test_InitialViewController_IsItemListViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = navigationController.viewControllers[0]
        
        XCTAssertTrue(rootViewController is ItemListVC)
    }
}
