//
//  InputVCTests.swift
//  todo_tddTests
//
//  Created by Juan Francisco Dorado Torres on 11/12/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest
import CoreLocation

@testable import todo_tdd
class InputVCTests: XCTestCase {
    
    // MARK: Properties
    
    var sut: InputVC!
    var placemark: MockPlacemark!

    // MARK: Test cycle
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "InputVC") as? InputVC
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        sut.itemManager?.removeAll()
        super.tearDown()
    }
    
    // MARK: Tests
    
    func test_HasTitleTextField() {
        let txtTitleIsSubView = sut.txtTitle?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(txtTitleIsSubView)
    }
    
    func test_HasDateTextfield() {
        let txtDateIsSubView = sut.txtDate?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(txtDateIsSubView)
    }
    
    func test_HasLocationTextfield() {
        let txtLocationIsSubView = sut.txtLocation?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(txtLocationIsSubView)
    }
    
    func test_HasAddressTextfield() {
        let txtAddressIsSubView = sut.txtAddress?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(txtAddressIsSubView)
    }
    
    func test_HasDescriptionTextfield() {
        let txtDescriptionIsSubView = sut.txtDescription?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(txtDescriptionIsSubView)
    }
    
    func test_HasSaveButton() {
        let btnSaveIsSubView = sut.btnSave?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(btnSaveIsSubView)
    }
    
    func test_HasCancelButton() {
        let btnCancelIsSubView = sut.btnCancel?.isDescendant(of: sut.view) ?? false
        XCTAssertTrue(btnCancelIsSubView)
    }
    
    func test_Save_UsesGeocoderToGetCoordinateFromAddress() {
        let mockSut = MockInputVC()
        
        mockSut.txtTitle = UITextField()
        mockSut.txtDate = UITextField()
        mockSut.txtLocation = UITextField()
        mockSut.txtAddress = UITextField()
        mockSut.txtDescription = UITextField()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let timestamp = 1456034400.0
        let date = Date(timeIntervalSince1970: timestamp)
        
        mockSut.txtTitle.text = "Foo"
        mockSut.txtDate.text = dateFormatter.string(from: date)
        mockSut.txtLocation.text = "Bar"
        mockSut.txtAddress.text = "Infinite Loop 1, Cupertino"
        mockSut.txtDescription.text = "Baz"
        
        let mockGeocoder = MockGeocoder()
        mockSut.geocoder = mockGeocoder
        mockSut.itemManager = ItemManager()
        
        let dismissExpectation = expectation(description: "Dismiss")
        
        mockSut.completionHandler = {
            dismissExpectation.fulfill()
        }
        
        mockSut.save()
        
        placemark = MockPlacemark()
        let coordinate = CLLocationCoordinate2DMake(37.3316851,
                                                    -122.0300674)
        placemark.mockCoordinate = coordinate
        mockGeocoder.completionHandler?([placemark], nil)
        
        waitForExpectations(timeout: 1, handler: nil)
        
        let item = mockSut.itemManager?.item(at: 0)
        let testItem = Item(title: "Foo",
                            description: "Baz",
                            timestamp: timestamp,
                            location: Location(name: "Bar", coordinate: coordinate))
        XCTAssertEqual(item, testItem)
        mockSut.itemManager?.removeAll()
    }
    
    func test_SaveButtonHasSaveAction() {
        let btnSave: UIButton = sut.btnSave
        
        guard let actions = btnSave.actions(forTarget: sut, forControlEvent: .touchUpInside) else {
            XCTFail()
            return
        }
        
        XCTAssertTrue(actions.contains("save"))
    }
    
    func test_Geocoder_FetchesCoordinates() {
        let geocoderAnswered = expectation(description: "Geocoder")
        let address = "Infinite Loop 1, Cupertino"
        CLGeocoder().geocodeAddressString(address) { (placemarks, error) in
            let coordinate = placemarks?.first?.location?.coordinate
            guard let latitude = coordinate?.latitude else {
                XCTFail()
                return
            }
            
            guard let longitude = coordinate?.longitude else {
                XCTFail()
                return
            }
            
            XCTAssertEqual(latitude, 37.3316, accuracy: 0.001)
            XCTAssertEqual(longitude, -122.0300, accuracy: 0.001)
            geocoderAnswered.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testSave_DismissesViewController() {
        let mockInputVC = MockInputVC()
        mockInputVC.txtTitle = UITextField()
        mockInputVC.txtDate = UITextField()
        mockInputVC.txtLocation = UITextField()
        mockInputVC.txtAddress = UITextField()
        mockInputVC.txtDescription = UITextField()
        mockInputVC.txtTitle.text = "Test Title"
        
        mockInputVC.save()
        
        XCTAssertTrue(mockInputVC.dismissGotCalled)
    }
}

extension InputVCTests {
    
    class MockGeocoder: CLGeocoder {
        
        var completionHandler: CLGeocodeCompletionHandler?
        
        override func geocodeAddressString(_ addressString: String, completionHandler: @escaping CLGeocodeCompletionHandler) {
            self.completionHandler = completionHandler
        }
    }
    
    class MockPlacemark: CLPlacemark {
        
        var mockCoordinate: CLLocationCoordinate2D?
        
        override var location: CLLocation? {
            guard let coordinate = mockCoordinate else { return CLLocation() }
            return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }
    
    class MockInputVC: InputVC {
        
        var dismissGotCalled = false
        var completionHandler: (() -> Void)?
        
        override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
            dismissGotCalled = true
            completionHandler?()
        }
    }
}
