//
//  ItemListViewController.swift
//  todo_tddTests
//
//  Created by Juan Francisco Dorado Torres on 10/18/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest

@testable import todo_tdd
class ItemListVCTests: XCTestCase {
    
    // MARK: Properties
    
    var sut: ItemListVC!
    var inputVC: InputVC!
    var btnAdd: UIBarButtonItem!
    var btnAddSelector: Selector!
    
    // MARK: View cycle

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemListVC")
        sut = viewController as? ItemListVC
        sut.loadViewIfNeeded()
        
        UIApplication.shared.keyWindow?.rootViewController = sut
        btnAdd = sut.navigationItem.rightBarButtonItem
        btnAddSelector = btnAdd.action
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Tests

    func test_TableView_AfterViewDidLoadIsNotNil() {
        sut.loadViewIfNeeded()
        
        XCTAssertNotNil(sut.tableView)
    }
    
    func test_LoadingView_SetsTableViewDataSource() {
        sut.loadViewIfNeeded()
        
        XCTAssertTrue(sut.tableView?.dataSource is ItemListDataProvider)
    }
    
    func test_LoadingView_SetsTableViewDelegate() {
        XCTAssertTrue(sut.tableView.delegate is ItemListDataProvider)
    }
    
    func test_LoadingView_DataSourceEqualDelegate() {
        XCTAssertEqual(sut.tableView.dataSource as? ItemListDataProvider, sut.tableView.delegate as? ItemListDataProvider)
    }
    
    func test_ItemListViewController_HasAddBarButtonWithSelfAsTarget() {
        let target = sut.navigationItem.rightBarButtonItem?.target
        XCTAssertEqual(target as? UIViewController, sut)
    }
    
    func test_AddItem_PresentsAddItemViewController() {
        XCTAssertNil(sut.presentedViewController)
        
        sut.performSelector(onMainThread: btnAddSelector, with: btnAdd, waitUntilDone: true)
        
        XCTAssertNotNil(sut.presentedViewController)
        XCTAssertTrue(sut.presentedViewController is InputVC)
        
        inputVC = sut.presentedViewController as? InputVC
        XCTAssertNotNil(inputVC.txtTitle)
    }
    
    func testItemListVC_SharesItemManagerWithInputVC() {
        sut.performSelector(onMainThread: btnAddSelector, with: btnAdd, waitUntilDone: true)
        
        inputVC = sut.presentedViewController as? InputVC
        guard let inputItemManager = inputVC.itemManager else { XCTFail(); return }
        
        XCTAssertTrue(sut.itemManager === inputItemManager)
    }
    
    func test_ViewDidLoad_SetsItemManagerToDataProvider() {
        XCTAssertTrue(sut.itemManager === sut.dataProvider.itemManager)
    }
    
    // TODO: Fix Test crashes on super.pushViewController on MockNavigationController
    func xtestItemSelectedNotification_PushesDetailVC() {
        let mockNavigationController = MockNavigationController(rootViewController: sut)
        UIApplication.shared.keyWindow?.rootViewController = mockNavigationController
        sut.loadViewIfNeeded()
        
        sut.itemManager.add(Item(title: "foo"))
        sut.itemManager.add(Item(title: "bar"))
        
        NotificationCenter.default.post(name: NSNotification.Name("ItemSelectedNotification"), object: self, userInfo: ["index": 1])
        
        guard let detailViewController = mockNavigationController.lastPushedViewController as? DetailVC else { return XCTFail() }
        guard let detailItemManager = detailViewController.itemInfo?.0 else { return XCTFail() }
        guard let index = detailViewController.itemInfo?.1 else { return XCTFail() }
        
        detailViewController.loadViewIfNeeded()
        XCTAssertNotNil(detailViewController.titleLabel)
        XCTAssertTrue(detailItemManager === sut.itemManager)
        XCTAssertEqual(index, 1)
    }
}

extension ItemListVCTests {
    
    class MockNavigationController : UINavigationController {
        
        var lastPushedViewController: UIViewController?
        
        override func pushViewController(_ viewController: UIViewController, animated: Bool)
        {
            lastPushedViewController = viewController
            super.pushViewController(viewController, animated: animated)
        }
    }
}
