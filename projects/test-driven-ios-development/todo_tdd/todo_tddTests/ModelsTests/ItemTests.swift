//
//  ItemTests.swift
//  todo_tddTests
//
//  Created by Juan Francisco Dorado Torres on 10/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

/*

 Test Name Pattern
 
 func test_<methodName>_<precondition>_<expectedBehavior>() {}
 
 */

import XCTest

@testable import todo_tdd
class ItemTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // MARK: Tests
    
    func test_Init_WhenGivenTitle_SetsTitle() {
        let item = Item(title: "Foo")
        XCTAssertEqual(item.title, "Foo", "should set title")
    }
    
    func test_Init_WhenGivenDescription_SetsDescription() {
        let item = Item(title: "", description: "Bar")
        XCTAssertEqual(item.description, "Bar", "should set itemDescription")
    }
    
    func test_Init_SetsTimestamp() {
        let item = Item(title: "", timestamp: 0.0)
        XCTAssertEqual(item.timestamp, 0.0, "should set timestamp")
    }
    
    func test_Init_WhenGivenLocation_SetsLocation() {
        let location = Location(name: "Foo")
        let item = Item(title: "", location: location)
        XCTAssertEqual(item.location?.name, location.name, "should set location")
    }
    
    func test_EqualItems_AreEqual() {
        let first = Item(title: "Foo")
        let second = Item(title: "Foo")
        
        XCTAssertEqual(first, second)
    }
    
    func test_Items_WhenLocationDifferents_AreNotEqual() {
        let first = Item(title: "", location: Location(name: "Foo"))
        let second = Item(title: "", location: Location(name: "Bar"))
        
        XCTAssertNotEqual(first, second)
    }
    
    func test_Items_WhenOneLocationIsNil_AreNotEqual() {
        var first = Item(title: "", location: Location(name: "Foo"))
        var second = Item(title: "", location: nil)
        XCTAssertNotEqual(first, second)
        
        first = Item(title: "", location: nil)
        second = Item(title: "", location: Location(name: "Foo"))
        XCTAssertNotEqual(first, second)
    }
    
    func test_Items_WhenTimestampsDiffer_AreNotEqual() {
        let first = Item(title: "Foo", timestamp: 1.0)
        let second = Item(title: "Foo", timestamp: 0.0)
        
        XCTAssertNotEqual(first, second)
    }
    
    func test_Items_WhenDescriptionsDiffer_AreNotEqual() {
        let first = Item(title: "Foo", description: "Bar")
        let second = Item(title: "Foo", description: "Baz")
        
        XCTAssertNotEqual(first, second)
    }
    
    func test_Items_WhenTitlesDiffer_AreNotEqual() {
        let first = Item(title: "Foo")
        let second = Item(title: "Bar")
        
        XCTAssertNotEqual(first, second)
    }
    
    func test_HasPlistDictionaryProperty() {
        let item = Item(title: "First")
        let dictionary = item.plistDict
        
        XCTAssertNotNil(dictionary)
        XCTAssertTrue(dictionary is [String : Any])
    }
    
    func test_CanBeCreatedFromPlistDictionary() {
        let location = Location(name: "Bar")
        let item = Item(title: "Foo", description: "Baz", timestamp: 1.0, location: location)
        
        let dict = item.plistDict
        let recreatedItem = Item(dict: dict)
        
        XCTAssertEqual(item, recreatedItem)
    }
}
