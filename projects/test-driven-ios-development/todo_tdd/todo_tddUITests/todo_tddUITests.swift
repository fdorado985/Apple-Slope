//
//  todo_tddUITests.swift
//  todo_tddUITests
//
//  Created by Juan Francisco Dorado Torres on 11/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest

class todo_tddUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let app = XCUIApplication()
        app.navigationBars["todo_tdd.ItemListVC"].buttons["Add"].tap()
        
        let titleTextField = app.textFields["Call Mom"]
        titleTextField.tap()
        titleTextField.typeText("Meeting")
        
        let dateTextField = app.textFields["01/01/2016"]
        dateTextField.tap()
        dateTextField.typeText("02/22/2018")
        
        let locationTextField = app.textFields["Home"]
        locationTextField.tap()
        locationTextField.typeText("Office")
        
        let addressTextField = app.textFields["Address"]
        addressTextField.tap()
        addressTextField.typeText("Inifinite Loop 1, Cupertino")
        
        let descriptionTextField = app.textFields["Description"]
        descriptionTextField.tap()
        descriptionTextField.typeText("Bring iPad")
        
        app.buttons["Save"].tap()
        
        XCTAssertTrue(app.tables.staticTexts["Meeting"].exists)
        XCTAssertTrue(app.tables.staticTexts["02/22/2018"].exists)
        XCTAssertTrue(app.tables.staticTexts["Office"].exists)
        
    }

}
