//
//  AvatarPickerVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class AvatarPickerVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var segmentColor: UISegmentedControl!
    @IBOutlet weak var collectionAvatars: UICollectionView!
    
    // MARK: Properties
    
    private var avatarType = AvatarType.dark

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBActions
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func segmentColorChanged(_ sender: UISegmentedControl) {
        avatarType = sender.selectedSegmentIndex == 0 ? .dark : .light
        collectionAvatars.reloadData()
    }
}

extension AvatarPickerVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 28
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath) as? AvatarCell else { return UICollectionViewCell() }
        cell.configureCell(index: indexPath.item, type: avatarType)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfColumns: CGFloat = UIScreen.main.bounds.width > 320 ? 4 : 3
        let spaceBetweenCells: CGFloat = 10
        let padding: CGFloat = 40
        let cellDimension = ((collectionView.bounds.width - padding) - (numberOfColumns - 1) * spaceBetweenCells) / numberOfColumns
        return CGSize(width: cellDimension, height: cellDimension)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDataService.shared.setAvatarName(avatarName: avatarType == .dark ? "dark\(indexPath.item)" : "light\(indexPath.item)")
        dismiss(animated: true)
    }
}
