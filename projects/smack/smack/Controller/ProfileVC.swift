//
//  ProfileVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet var viewBackground: UIView!
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: IBActions

    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        UserDataService.shared.logoutUser()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        dismiss(animated: true)
    }
    
    // Functions
    
    private func setupView() {
        txtName.text = UserDataService.shared.name
        txtEmail.text = UserDataService.shared.email
        imgAvatar.image = UIImage(named: UserDataService.shared.avatarName)
        imgAvatar.backgroundColor = UserDataService.shared.returnUIColor(components: UserDataService.shared.avatarColor)
        
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(closeTap(_:)))
        viewBackground.addGestureRecognizer(closeTouch)
    }
    
    @objc private func closeTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
}
