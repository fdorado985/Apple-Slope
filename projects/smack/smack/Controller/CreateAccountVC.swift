//
//  CreateAccountVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    // MARK: Properties
    
    var avatarName = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    var bgColor: UIColor?

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDataService.shared.avatarName != "" {
            imgAvatar.image = UIImage(named: UserDataService.shared.avatarName)
            avatarName = UserDataService.shared.avatarName
            
            if avatarName.contains("light") && bgColor == nil {
                imgAvatar.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnCreateAccountTapped(_ sender: UIButton) {
        guard let username = txtUsername.text, !username.isEmpty else { return }
        guard let email = txtEmail.text, !email.isEmpty else { return }
        guard let password = txtPassword.text, !password.isEmpty else { return }
        
        activitySpinner.stopAnimating()
        activitySpinner.startAnimating()
        
        AuthService.shared.registerUser(email: email, password: password) { (success) in
            if success {
                AuthService.shared.login(email: email, password: password, completion: { (sucess) in
                    AuthService.shared.createUser(name: username, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, completion: { (success) in
                        if success {
                            self.activitySpinner.stopAnimating()
                            self.performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
                            NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                        }
                    })
                })
            }
        }
    }
    
    @IBAction func btnChooseAvatarTapped(_ sender: UIButton) {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
    }
    
    @IBAction func btnPickBackgroundColorTapped(_ sender: UIButton) {
        let red = CGFloat(arc4random_uniform(255)) / 255
        let green = CGFloat(arc4random_uniform(255)) / 255
        let blue = CGFloat(arc4random_uniform(255)) / 255
        
        bgColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        avatarColor = "[\(red), \(green), \(blue), 1]"
        UIView.animate(withDuration: 0.2) {
            self.imgAvatar.backgroundColor = self.bgColor
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
        //performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil) // Use it in User Creation
    }
    
    // MARK: Functions
    
    private func setupView() {
        activitySpinner.stopAnimating()
        txtUsername.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func handleTap() {
        view.endEditing(true)
    }
    
}
