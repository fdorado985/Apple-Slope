//
//  LoginVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        guard let email = txtUsername.text, !email.isEmpty else { return }
        guard let password = txtPassword.text, !password.isEmpty else { return }
        activitySpinner.startAnimating()
        AuthService.shared.login(email: email, password: password) { (success) in
            if success {
                AuthService.shared.findUserByEmail(completion: { (success) in
                    NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                    self.activitySpinner.stopAnimating()
                    self.dismiss(animated: true)
                })
            }
        }
    }
    
    @IBAction func btnCreateAccountTapped(_ sender: UIButton) {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    
    // MARK: Functions
    
    private func setupView() {
        activitySpinner.stopAnimating()
        txtUsername.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
    }
}
