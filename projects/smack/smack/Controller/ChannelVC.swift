//
//  ChannelVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {

    // MARK: IBActions
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var tableChannels: UITableView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        NotificationCenter.default.addObserver(self, selector: #selector(userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(channelsLoaded(_:)), name: NOTIF_CHANNELS_LOADED, object: nil)
        
        SocketService.shared.getChannel { (success) in
            if success {
                self.tableChannels.reloadData()
            }
        }
        
        SocketService.shared.getChatMessage { (newMessage) in
            if newMessage.channelId != MessageService.shared.selectedChannel?.id && AuthService.shared.isLoggedIn {
                MessageService.shared.unreadChannels.append(newMessage.channelId)
                self.tableChannels.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUserInfo()
    }
    
    // MARK: IBActions

    @IBAction func btnLoginTapped(_ sender: UIButton) {
        if AuthService.shared.isLoggedIn {
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true)
        } else {
            performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
    }
    
    @IBAction func btnCreateChannelTapped(_ sender: UIButton) {
        if AuthService.shared.isLoggedIn {
            let createChannel = CreateChannelVC()
            createChannel.modalPresentationStyle = .custom
            present(createChannel, animated: true)
        }
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    
    // MARK: Functions
    
    @objc private func userDataDidChange(_ notification: Notification) {
        setupUserInfo()
    }
    
    @objc private func channelsLoaded(_ notification: Notification) {
        tableChannels.reloadData()
    }
    
    func setupUserInfo() {
        if AuthService.shared.isLoggedIn {
            btnLogin.setTitle(UserDataService.shared.name, for: .normal)
            imgAvatar.image = UIImage(named: UserDataService.shared.avatarName)
            imgAvatar.backgroundColor = UserDataService.shared.returnUIColor(components: UserDataService.shared.avatarColor)
        } else {
            btnLogin.setTitle("Login", for: .normal)
            imgAvatar.image = UIImage(named: "menuProfileIcon")
            imgAvatar.backgroundColor = UIColor.clear
            tableChannels.reloadData()
        }
    }
}

extension ChannelVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.shared.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelCell", for: indexPath) as? ChannelCell else { return UITableViewCell() }
        let channel = MessageService.shared.channels[indexPath.row]
        cell.configureCell(channel: channel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.shared.channels[indexPath.row]
        MessageService.shared.selectedChannel = channel
        
        if MessageService.shared.unreadChannels.count > 0 {
            MessageService.shared.unreadChannels = MessageService.shared.unreadChannels.filter { $0 != channel.id }
        }
        
        let index = IndexPath(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [index], with: .none)
        tableView.selectRow(at: index, animated: false, scrollPosition: .none)
        
        NotificationCenter.default.post(name: NOTIF_CHANNEL_SELECTED, object: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    
}
