//
//  CreateChannelVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CreateChannelVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var viewBackground: UIView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnCreateTapped(_ sender: UIButton) {
        guard let name = txtName.text, !name.isEmpty else { return }
        guard let description = txtDescription.text else { return }
        SocketService.shared.addChannel(name: name, description: description) { (success) in
            if success {
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // Functions
    
    private func setupView() {
        txtName.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        txtDescription.attributedPlaceholder = NSAttributedString(string: "Description", attributes: [NSAttributedStringKey.foregroundColor : smackPurplePlaceHolder])
        
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(closeTap(_:)))
        viewBackground.addGestureRecognizer(closeTouch)
    }
    
    @objc private func closeTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true)
    }
}
