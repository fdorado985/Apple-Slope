//
//  ChatVC.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var tableMessages: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblUserTyping: UILabel!
    
    // MARK: Properties
    
    private var isTyping = false
    
    // MARK: ViewCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.bindToKeyboard()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "smackBurger"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        NotificationCenter.default.addObserver(self, selector: #selector(userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(channelSelected(_:)), name: NOTIF_CHANNEL_SELECTED, object: nil)
        
        SocketService.shared.getChatMessage { (newMessage) in
            if newMessage.channelId == MessageService.shared.selectedChannel?.id && AuthService.shared.isLoggedIn {
                MessageService.shared.messages.append(newMessage)
                self.tableMessages.reloadData()
                if MessageService.shared.messages.count > 0 {
                    let endIndex = IndexPath(row: MessageService.shared.messages.count - 1, section: 0)
                    self.tableMessages.scrollToRow(at: endIndex, at: .bottom, animated: false)
                }
            }
        }
        
        SocketService.shared.getTypingUsers { (typingUsers) in
            guard let channelId = MessageService.shared.selectedChannel?.id else { return }
            var names = ""
            var numberOfTypers = 0
            
            for (typingUser, channel) in typingUsers {
                if typingUser != UserDataService.shared.name && channel == channelId {
                    if names == "" {
                        names = typingUser
                    } else {
                        names = "\(names), \(typingUser)"
                    }
                    numberOfTypers += 1
                }
            }
            
            if numberOfTypers > 0 && AuthService.shared.isLoggedIn {
                var verb = "is"
                if numberOfTypers > 1 {
                    verb = "are"
                }
                
                self.lblUserTyping.text = "\(names) \(verb) typing a message"
            } else {
                self.lblUserTyping.text = ""
            }
        }
        
        if AuthService.shared.isLoggedIn {
            AuthService.shared.findUserByEmail { (success) in
                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        if AuthService.shared.isLoggedIn {
            guard let channelId = MessageService.shared.selectedChannel?.id else { return }
            guard let message = txtMessage.text, !message.isEmpty else { return }
            
            SocketService.shared.addMessage(messageBody: message, userId: UserDataService.shared.id, channelId: channelId) { (success) in
                if success {
                    self.txtMessage.text = ""
                    self.txtMessage.resignFirstResponder()
                    SocketService.shared.socket.emit("stopType", UserDataService.shared.name, channelId)
                }
            }
        }
    }
    
    @IBAction func txtMessageIsEditing(_ sender: UITextField) {
        guard let channelId = MessageService.shared.selectedChannel?.id else { return }
        if sender.text == "" {
            isTyping = false
            btnSend.isHidden = true
            SocketService.shared.socket.emit("stopType", UserDataService.shared.name, channelId)
        } else {
            if isTyping == false {
                btnSend.isHidden = false
                SocketService.shared.socket.emit("startType", UserDataService.shared.name, channelId)
            }
            isTyping = true
        }
    }
    // MARK: Functions
    
    @objc func userDataDidChange(_ notif: Notification) {
        if AuthService.shared.isLoggedIn {
            onLoginGetMessages()
        } else {
            self.title = "Please Log In"
        }
    }
    
    @objc func channelSelected(_ notif: Notification) {
        updateWithChannel()
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    private func updateWithChannel() {
        let channelName = MessageService.shared.selectedChannel?.name
        self.title = channelName != nil ? "#\(channelName!)" : "Smack"
        getMessages()
    }
    
    private func onLoginGetMessages() {
        MessageService.shared.findAllChannel(completion: { (success) in
            if success {
                if MessageService.shared.channels.count > 0 {
                    MessageService.shared.selectedChannel = MessageService.shared.channels[0]
                    self.updateWithChannel()
                } else {
                    self.title = "No channels yet!"
                }
            }
        })
    }
    
    private func getMessages() {
        guard let channelId = MessageService.shared.selectedChannel?.id else { return }
        MessageService.shared.findallMessagesForChannel(channelId: channelId) { (success) in
            if success {
                self.tableMessages.reloadData()
            }
        }
    }

}

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.shared.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as? MessageCell else { return UITableViewCell() }
        let message = MessageService.shared.messages[indexPath.row]
        cell.configureCell(message: message)
        return cell
    }
}
