//
//  Channel.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Channel {
    
    // MARK: Properties
    
    private(set) var name: String
    private(set) var description: String
    private(set) var id: String
}
