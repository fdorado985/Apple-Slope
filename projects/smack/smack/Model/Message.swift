//
//  Message.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Message {
    
    private(set) var id: String
    private(set) var messageBody: String
    private(set) var userId: String
    private(set) var channelId: String
    private(set) var username: String
    private(set) var avatar: String
    private(set) var avatarColor: String
    private(set) var timestamp: String
}
