//
//  MessageService.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    
    // MARK: Singleton
    
    static let shared = MessageService()
    
    // MARK: Properties
    
    var channels = [Channel]()
    var selectedChannel: Channel?
    var messages = [Message]()
    var unreadChannels = [String]()
    
    // MARK: Functions
    
    func findAllChannel(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data,
                let json = try? JSON(data: data),
                let jsonArray = json.array else {
                completion(false)
                return
            }
            
            self.setChannelInfo(allJSONs: jsonArray)
            NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
            completion(true)
        }
    }
    
    func findallMessagesForChannel(channelId: String, completion: @escaping CompletionHandler) {
        Alamofire.request("\(URL_GET_MESSAGES)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            self.clearMessages()
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data,
                let json = try? JSON(data: data),
                let jsonArray = json.array else {
                    completion(false)
                    return
            }
            
            self.setMessageInfo(allJSONs: jsonArray)
            completion(true)
        }
    }
    
    func clearChannels() {
        channels.removeAll()
    }
    
    func clearMessages() {
        messages.removeAll()
    }
    
    private func setChannelInfo(allJSONs: [JSON]) {
        for json in allJSONs {
            let id = json["_id"].stringValue
            let name = json["name"].stringValue
            let description = json["description"].stringValue
            let channel = Channel(name: name, description: description, id: id)
            channels.append(channel)
        }
    }
    
    private func setMessageInfo(allJSONs: [JSON]) {
        for json in allJSONs {
            let id = json["_id"].stringValue
            let messageBody = json["messageBody"].stringValue
            let userId = json["userId"].stringValue
            let channelId = json["channelId"].stringValue
            let username = json["userName"].stringValue
            let avatar = json["userAvatar"].stringValue
            let avatarColor = json["userAvatarColor"].stringValue
            let timestamp = json["timeStamp"].stringValue
            let message = Message(id: id, messageBody: messageBody, userId: userId, channelId: channelId, username: username, avatar: avatar, avatarColor: avatarColor, timestamp: timestamp)
            messages.append(message)
        }
    }
    
    

}
