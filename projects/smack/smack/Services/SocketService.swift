//
//  SocketService.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import SocketIO

class SocketService: NSObject {
    
    // MARK: Singleton
    
    static let shared = SocketService()
    
    // MARK: Properties
    
    private let manager: SocketManager = SocketManager(socketURL: URL(string: BASE_URL)!)
    private(set) var socket: SocketIOClient
    
    // MARK: Initialization
    
    override init() {
        socket = manager.defaultSocket
        super.init()
    }
    
    // MARK: Functions
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func addChannel(name: String, description: String, completion: @escaping CompletionHandler) {
        socket.emit("newChannel", name, description)
        completion(true)
    }
    
    func getChannel(completion: @escaping CompletionHandler) {
        socket.on("channelCreated") { (dataArray, ack) in
            guard let name = dataArray[0] as? String else { return }
            guard let description = dataArray[1] as? String else { return }
            guard let id = dataArray[2] as? String else { return }
            
            let newChannel = Channel(name: name, description: description, id: id)
            MessageService.shared.channels.append(newChannel)
            completion(true)
        }
    }
    
    func addMessage(messageBody: String, userId: String, channelId: String, completion: @escaping CompletionHandler) {
        let user = UserDataService.shared
        socket.emit("newMessage", messageBody, userId, channelId, user.name, user.avatarName, user.avatarColor)
        completion(true)
    }
    
    func getChatMessage(_ completion: @escaping (_ newMessage: Message) -> Void) {
        socket.on("messageCreated") { (dataArray, ack) in
            guard let message = dataArray[0] as? String else { return }
            guard let userId = dataArray[1] as? String else { return }
            guard let channelId = dataArray[2] as? String else { return }
            guard let username = dataArray[3] as? String else { return }
            guard let avatar = dataArray[4] as? String else { return }
            guard let avatarColor = dataArray[5] as? String else { return }
            guard let id = dataArray[6] as? String else { return }
            guard let timestamp = dataArray[7] as? String else { return }
            
            let newMessage = Message(id: id, messageBody: message, userId: userId, channelId: channelId, username: username, avatar: avatar, avatarColor: avatarColor, timestamp: timestamp)
            completion(newMessage)
        }
    }
    
    func getTypingUsers(_ completion: @escaping (_ typingUsers: [String : String]) -> Void) {
        socket.on("userTypingUpdate") { (dataArray, ack) in
            guard let typingUsers = dataArray[0] as? [String : String] else { return }
            completion(typingUsers)
        }
    }
}
