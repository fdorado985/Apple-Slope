//
//  UserDataService.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class UserDataService {
    
    static let shared = UserDataService()
    
    public private(set) var id = ""
    public private(set) var avatarColor = ""
    public private(set) var avatarName = ""
    public private(set) var email = ""
    public private(set) var name = ""
    
    func setUserData(id: String, name: String, email: String, avatarName: String, avatarColor: String) {
        self.id = id
        self.name = name
        self.email = email
        self.avatarName = avatarName
        self.avatarColor = avatarColor
    }
    
    func setAvatarName(avatarName: String) {
        self.avatarName = avatarName
    }
    
    func returnUIColor(components: String) -> UIColor {
        let scanner = Scanner(string: components)
        let skipped = CharacterSet(charactersIn: "[], ")
        let comma = CharacterSet(charactersIn: ",")
        scanner.charactersToBeSkipped = skipped
        
        var r, g, b, a: NSString?
        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)
        
        let defaultColor = UIColor.lightGray
        
        guard let red = r else { return defaultColor }
        guard let green = g else { return defaultColor }
        guard let blue = b else { return defaultColor }
        guard let alpha = a else { return defaultColor }
        
        let redFloat = CGFloat(red.doubleValue)
        let greenFloat = CGFloat(green.doubleValue)
        let blueFloat = CGFloat(blue.doubleValue)
        let alphaFloat = CGFloat(alpha.doubleValue)
        
        let newColor = UIColor(red: redFloat, green: greenFloat, blue: blueFloat, alpha: alphaFloat)
        return newColor
    }
    
    func logoutUser() {
        self.id = ""
        self.name = ""
        self.email = ""
        self.avatarName = ""
        self.avatarColor = ""
        AuthService.shared.isLoggedIn = false
        AuthService.shared.authToken = ""
        AuthService.shared.userEmail = ""
        MessageService.shared.clearChannels()
        MessageService.shared.clearMessages()
    }
}
