//
//  AuthService.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    
    // MARK: Singleton
    
    static let shared = AuthService()
    
    // Properties
    
    private let defaults = UserDefaults.standard
    
    var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: LOOGED_IN_KEY)
        }
        
        set {
            defaults.set(newValue, forKey: LOOGED_IN_KEY)
        }
    }
    
    var authToken: String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as? String ?? ""
        }
        
        set {
            defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }
    
    var userEmail: String {
        get {
            return defaults.value(forKey: USER_EMAIL_KEY) as? String ?? ""
        }
        
        set {
            defaults.set(newValue, forKey: USER_EMAIL_KEY)
        }
    }
    
    // MARK: Functions
    
    func registerUser(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = [
            "email" : lowerCaseEmail,
            "password" : password
        ]
        
        Alamofire.request(URL_REGISTER, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            completion(response.result.error == nil)
        }
    }
    
    func login(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = [
            "email" : lowerCaseEmail,
            "password" : password
        ]
        
        Alamofire.request(URL_LOGIN, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data, let json = try? JSON(data: data) else {
                completion(false)
                return
            }
            
            self.userEmail = json["user"].stringValue
            self.authToken = json["token"].stringValue
            
            self.isLoggedIn = true
            completion(true)
        }
    }
    
    func createUser(name: String, email: String, avatarName: String, avatarColor: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = [
            "name" : name,
            "email" : lowerCaseEmail,
            "avatarName" : avatarName,
            "avatarColor" : avatarColor
        ]
        
        Alamofire.request(URL_ADD_USER, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data, let json = try? JSON(data: data) else {
                completion(false)
                return
            }
            
            self.setUserInfo(json: json)
            completion(true)
        }
    }
    
    func findUserByEmail(completion: @escaping CompletionHandler) {
        Alamofire.request("\(URL_USER_BY_EMAIL)\(userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data, let json = try? JSON(data: data) else {
                completion(false)
                return
            }
            
            self.setUserInfo(json: json)
            completion(true)
        }
    }
    
    private func setUserInfo(json: JSON) {
        let id = json["_id"].stringValue
        let color = json["avatarColor"].stringValue
        let avatarName = json["avatarName"].stringValue
        let email = json["email"].stringValue
        let name = json["name"].stringValue
        
        UserDataService.shared.setUserData(id: id, name: name, email: email, avatarName: avatarName, avatarColor: color)
    }
}
