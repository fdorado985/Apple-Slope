//
//  ChannelCell.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var name: UILabel!

    // MARK: View cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.layer.backgroundColor = selected ? UIColor(white: 1, alpha: 0.2).cgColor : UIColor.clear.cgColor
    }
    
    // MARK: Functions
    
    func configureCell(channel: Channel) {
        self.name.text = "#\(channel.name)"
        
        for id in MessageService.shared.unreadChannels {
            if id == channel.id {
                name.font = UIFont(name: "AvenirNext-Bold", size: 22)
            }
        }
    }
}
