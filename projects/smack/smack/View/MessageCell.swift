//
//  MessageCell.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    // MARK: IBOutlets
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblTimeStamp: UILabel!
    @IBOutlet weak var imgAvatar: CircleImage!
    @IBOutlet weak var lblMessage: UILabel!
    
    // MARK: Functions
    
    func configureCell(message: Message) {
        lblMessage.text = message.messageBody
        lblUsername.text = message.username
        imgAvatar.image = UIImage(named: message.avatar)
        imgAvatar.backgroundColor = UserDataService.shared.returnUIColor(components: message.avatarColor)
        
        var dateISO = message.timestamp
        let end = dateISO.index(dateISO.endIndex, offsetBy: -5)
        dateISO = dateISO.substring(to: end)
        
        let dateFormatter = ISO8601DateFormatter()
        let chatDate = dateFormatter.date(from: dateISO.appending("Z"))
        
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "MMM d, h:mm a"
        
        if let finalDate = chatDate {
            let finalDate = newFormatter.string(from: finalDate)
            lblTimeStamp.text = finalDate
        }
    }
    
}
