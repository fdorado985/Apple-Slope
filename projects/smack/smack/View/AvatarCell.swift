//
//  AvatarCell.swift
//  smack
//
//  Created by Juan Francisco Dorado Torres on 8/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

enum AvatarType {
    case dark, light
}

class AvatarCell: UICollectionViewCell {
    
    // MARK: IBOutlet
    
    @IBOutlet weak var imgAvatar: UIImageView!
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: Function
    
    func configureCell(index: Int, type: AvatarType) {
        if type == .dark {
            imgAvatar.image = UIImage(named: "dark\(index)")
            self.layer.backgroundColor = UIColor.lightGray.cgColor
        } else {
            imgAvatar.image = UIImage(named: "light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor
        }
    }
    
    private func setupView() {
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}
