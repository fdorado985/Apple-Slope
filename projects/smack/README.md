#  Smack

This is a little clone of the popular app **Slack** you are able to...
* Create Users
* Login with an existing account
* Create Channels
* Send Messages
* See for a user typing
* See unread channels
* All in real time

What do we use special here?
* Socket IO (Swift Client)

## Create Account

![create_account_demo](screenshots/create_account_demo.gif)

## Login

![login_demo](screenshots/login_demo.gif)

## Create Channel

![create_channel_demo](screenshots/create_channel_demo.gif)

## Send Message

![send_message_demo](screenshots/send_message_demo.gif)

## Unread Channel

![unread_channel_demo](screenshots/unread_channels.gif)

### Mongo Account

devslopes_mlabs@mailinator.com
