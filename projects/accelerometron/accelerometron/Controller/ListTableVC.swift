//
//  ListTableVC.swift
//  accelerometron
//
//  Created by Juan Francisco Dorado Torres on 23/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ListTableVC: UITableViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return imagesArray.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "parallaxCell", for: indexPath) as? ParallaxCell else {
      return UITableViewCell()
    }
    cell.configureCell(withImage: imagesArray[indexPath.row], andName: namesArray[indexPath.row])
    return cell
  }
}

