//
//  Images+Names.swift
//  accelerometron
//
//  Created by Juan Francisco Dorado Torres on 23/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

let camera = UIImage(named: "1")!
let city = UIImage(named: "2")!
let animals = UIImage(named: "3")!
let flowers = UIImage(named: "4")!
let stand = UIImage(named: "5")!
let urban = UIImage(named: "6")!

let imagesArray = [camera, city, animals, flowers, stand, urban]
let namesArray = ["CAMERAS", "CITY", "ANIMALS", "FLOWERS", "STANDS", "URBAN"]
