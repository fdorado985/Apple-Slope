//
//  ParallaxCell.swift
//  accelerometron
//
//  Created by Juan Francisco Dorado Torres on 23/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ParallaxCell: UITableViewCell {

  // MARK: - IBOutlets

  @IBOutlet weak var imgBackground: UIImageView!
  @IBOutlet weak var lblName: UILabel!
  
  // UITableViewCell ViewCycle

  override func awakeFromNib() {
    super.awakeFromNib()
    setupParallax()
  }

  // MARK: - Functions

  private func setupParallax() {
    let min: CGFloat = -30
    let max: CGFloat = 30

    let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
    xMotion.minimumRelativeValue = min
    xMotion.maximumRelativeValue = max

    let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
    yMotion.minimumRelativeValue = min
    yMotion.maximumRelativeValue = max

    let motionEffectGroup = UIMotionEffectGroup()
    motionEffectGroup.motionEffects = [xMotion, yMotion]

    imgBackground.addMotionEffect(motionEffectGroup)
  }

  func configureCell(withImage image: UIImage, andName name: String) {
    imgBackground.image = image
    lblName.text = name
  }
}
