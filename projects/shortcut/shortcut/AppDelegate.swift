//
//  AppDelegate.swift
//  shortcut
//
//  Created by Juan Francisco Dorado Torres on 04/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    enum ShortcutType: String {
        case mountains = "mountains"
        case space = "space"
        case ocean = "ocean"
    }

    var window: UIWindow?
    var viewControllersArray = [UIViewController]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mountainVC = storyboard.instantiateViewController(withIdentifier: "mountainsVC") as! MountainsVC
        let spaceVC = storyboard.instantiateViewController(withIdentifier: "spaceVC") as! SpaceVC
        let oceanVC = storyboard.instantiateViewController(withIdentifier: "oceanVC") as! OceanVC
        viewControllersArray = [mountainVC, spaceVC, oceanVC]
        
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        if let type = shortcutItem.type.components(separatedBy: ".").last {
            let navVC = window?.rootViewController as! UINavigationController
            navVC.setViewControllers(viewControllersArray, animated: false)
            
            switch type {
            case ShortcutType.mountains.rawValue:
                navVC.popToRootViewController(animated: true)
                completionHandler(true)
            case ShortcutType.space.rawValue:
                navVC.popToViewController(viewControllersArray[1], animated: true)
                completionHandler(true)
            case ShortcutType.ocean.rawValue:
                navVC.popToViewController(viewControllersArray[2], animated: true)
                completionHandler(true)
            default:
                navVC.popToRootViewController(animated: true)
                completionHandler(true)
            }
        }
        completionHandler(false)
    }
}

