# Protocol Oriented Programming

On Swift we have two different of types.
* **Compound**
  * Tuples
  * Functions
    * Functions
    * Closures
* **Named**
  * Value
    * Primitives
    * Structures
    * Enums
  * Reference
    * Classes


## Reference Type
### Class
A class being a reference type it works like a pointer, so once you change one value... all the pointers of this are changed.

```swift
var car1 = Car()
var car2 = car1

car2.color = UIColor.red

print(car1.color) // out: Red
print(car2.color) // out: Red
```

## Value Type
### Struct
A struct being a value type... it works as a copy, so once you initialize a variable with another variable it is gonna be just a copy... so it doesn't matter if you change the value on one of them because they are independent.

```swift
var car1 = Car()
var car2 = car1

car2.color = UIColor.red

print(car1.color) // out: Blue
print(car2.color) // out: Red
```
