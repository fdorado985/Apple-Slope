//: Playground - noun: a place where people can play

import UIKit

// MARK: - Protocol Oriented Programming

protocol Vehicle: CustomStringConvertible {
    
    // MARK: Properties
    
    var make: String { get set }
    var model: String { get set }
    var isRunning: Bool { get set }
    
    // MARK: Functions
    
    mutating func start()
    mutating func turnOff()
}

extension Vehicle {
    
    var makeModel: String {
        return "\(make): \(model)"
    }
    
    var description: String {
        return makeModel
    }
    
    mutating func start() {
        if isRunning {
            debugPrint("Already started")
        } else {
            isRunning = true
            debugPrint("\(self.description) fired up!")
        }
    }
    
    mutating func turnOff() {
        if isRunning {
            isRunning = false
            debugPrint("\(self.description) shut down!")
        } else {
            debugPrint("Already turned off!")
        }
    }
}

struct SportsCar: Vehicle {
    
    // MARK: Protocol Properties
    
    var make: String = ""
    var model: String = ""
    var isRunning: Bool = false
}

class SemiTruck: Vehicle {
    
    // MARK: Protocol Properties
    
    var make: String = ""
    var model: String = ""
    var isRunning: Bool = false
    
    // Initialization
    
    init(make: String, model: String, isRunning: Bool) {
        self.make = make
        self.model = model
        self.isRunning = isRunning
    }
    
    // MARK: Functions
    
    func blowAirHown() {
        debugPrint("Tooooooth!!!")
    }
}


var car1 = SportsCar(make: "Porshe", model: "911", isRunning: false)
var truck1 = SemiTruck(make: "Peterbuilt", model: "Verago", isRunning: false)

car1.start()
truck1.start()
truck1.blowAirHown()
truck1.turnOff()

let vehicles: [Vehicle] = [car1, truck1]
for vehicle in vehicles {
    debugPrint(vehicle.description)
}
