//: Playground - noun: a place where people can play

import UIKit

func intAdder(number: Int) -> Int {
    return number + 1
}

func doubleAdder(number: Double) -> Double {
    return number + 1
}

intAdder(number: 15)
doubleAdder(number: 15.0)

func genericAdder<T: Strideable>(number: T) -> T {
    return number.advanced(by: 1)
}

genericAdder(number: 4)
genericAdder(number: 5.23)

func intMultiplier(lhs: Int, rhs: Int) -> Int {
    return lhs * rhs
}

func doubleMultiplier(lhs: Double, rhs: Double) -> Double {
    return lhs * rhs
}

intMultiplier(lhs: 5, rhs: 2)
doubleMultiplier(lhs: 3.2, rhs: 5.2)


protocol Numeric {
    static func *(lhs: Self, rhs: Self) -> Self
}

extension Double: Numeric {}
extension Float: Numeric {}
extension Int: Numeric {}

func genericMultiplier<T: Numeric>(lhs: T, rhs: T) -> T {
    return lhs * rhs
}

genericMultiplier(lhs: 5.5, rhs: 4.2)
genericMultiplier(lhs: 5, rhs: 4)
