# Intro To MVC

As a basic introduction of MVC, MVC is the most popular design pattern used to create and develop most of the iOS apps... although is a common design pattern used on different technologies... iOS is the most popular using it.

## How it works
MVC are the initials of *Model*, *View*, *Controller*, so this design pattern works on this 3 layers, so... let's give a little explain about each layer.

For this explanation i will use a little example that i have seen.

### Model
Imagine this layer as a blue print from anything to build, for example... a Watch.
This example is for explain that the blue print will have all the things need it for make this watch works.
On a project... the model will have all the information that the app will need to work.

Easy to understand right? i guess 😅.

### View
Using the same watch example... the view... is gonna be all the things that you are looking... for example the numbers or indicators used for the hour, tha hour hand, the minutes hand, the seconds hand and of course the view it looks like it supposes to.
On a project... the view is gonna be what the user is looking on its screen, all the labels, buttons, all the stuffs to see that beautiful app.

### Controller
Now, one of the most important things are this, the controller, why... because this is the joiner for the *Model* and the *View*, so... this could be the *watchmaker*, for example the buttons on the watch will use the *model* to make things happen to be able to see for the user through the *view*
On the project, this will be... the IBActions, and all the ways for the user to interact through the view to make things happen!

I hope this little explanation will help you to understand how the MVC pattern works... more less 😅, sometimes little words are easier to understand than a full book 📕

Of course if you want to get deeper on it... i recommend you to look for a good source to know much much more about this.