//
//  ViewController.swift
//  mvc-ifyme-capn
//
//  Created by Juan Francisco Dorado Torres on 09/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainController: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var lblName: UILabel!
  @IBOutlet weak var lblColor: UILabel!
  @IBOutlet weak var lblPrice: UILabel!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()

    let appleProduct = AppleProduct(name: "iPhone X", color: "Space Grey", price: 999.99)
    lblName.text = appleProduct.name
    lblColor.text = "in \(appleProduct.color)"
    lblPrice.text = "$\(appleProduct.price)"
  }
}

