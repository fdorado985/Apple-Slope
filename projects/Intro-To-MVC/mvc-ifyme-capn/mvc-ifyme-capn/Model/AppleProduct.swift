//
//  AppleProduct.swift
//  mvc-ifyme-capn
//
//  Created by Juan Francisco Dorado Torres on 09/12/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class AppleProduct {

  public private(set) var name: String
  public private(set) var color: String
  public private(set) var price: Double

  init(name: String, color: String, price: Double) {
    self.name = name
    self.color = color
    self.price = price
  }
}
