//
//  TimerIC.swift
//  RunningMan WatchKit Extension
//
//  Created by Juan Francisco Dorado Torres on 23/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import WatchKit
import Foundation


class TimerIC: WKInterfaceController {

  // MARK: - IBOutlets

  @IBOutlet var timerCountdown: WKInterfaceTimer!
  @IBOutlet var lblCalories: WKInterfaceLabel!
  @IBOutlet var btnPlay: WKInterfaceButton!
  @IBOutlet var btnPause: WKInterfaceButton!
  @IBOutlet var btnCancel: WKInterfaceButton!

  // MARK: - Properties

  private var isPaused = false
  private var endTime: Date!
  private var timePointPaused: Date?
  private var calorieTimer: Timer!
  private var calories = 0.0
  private var calorieBurnRate = 0.2

  // MARK: - View Cycle

  override func awake(withContext context: Any?) {
    super.awake(withContext: context)

    guard let runTime = context as? Int else { return }
    endTime = Date(timeIntervalSinceNow: TimeInterval(runTime * 60))
    timerCountdown.setDate(endTime)
    timerCountdown.start()
    btnPlay.setAlpha(0.5)
    startCalorieTimer()
  }

  // MARK: - IBActions

  @IBAction func btnPlayTapped() {
    if isPaused {
      guard let timePaused = timePointPaused else { return }
      let durationPaused = Date().timeIntervalSince(timePaused)
      endTime = Date(timeInterval: durationPaused, since: endTime)
      timerCountdown.setDate(endTime)
      timerCountdown.start()
      startCalorieTimer()
      btnPlay.setAlpha(0.5)
      btnPause.setAlpha(1.0)
      isPaused = false

    }
  }

  @IBAction func btnPauseTapped() {
    if !isPaused {
      timerCountdown.stop()
      calorieTimer.invalidate()
      btnPlay.setAlpha(1.0)
      btnPause.setAlpha(0.5)
      isPaused = true
      timePointPaused = Date()
    }
  }

  @IBAction func btnCancelTapped() {
    popToRootController()
  }

  // MARK: - Functions

  @objc
  private func updateCalories() {
    calories += calorieBurnRate
    lblCalories.setText("Calories : \(Int(calories))")
  }

  private func startCalorieTimer() {
    calorieTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCalories), userInfo: nil, repeats: true)
  }

}
