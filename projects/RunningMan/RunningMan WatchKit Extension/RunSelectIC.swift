//
//  RunSelectIC.swift
//  RunningMan WatchKit Extension
//
//  Created by Juan Francisco Dorado Torres on 23/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import WatchKit
import Foundation


class RunSelectIC: WKInterfaceController {

  // MARK: - IBOutlets

  @IBOutlet private var btnStart: WKInterfaceButton!
  @IBOutlet var pickerRun: WKInterfacePicker!

  // MARK: - Properties

  private var selectedRunTime = 5
  private var runItems = [WKPickerItem]()
  private var runTimes = [Int]()

  // MARK: - View Cycle

  override func awake(withContext context: Any?) {
    super.awake(withContext: context)

    for i in 0...16 {
      let runTime = i * 5
      runTimes.append(runTime)
      let run = WKPickerItem()
      run.contentImage = WKImage(imageName: "run\(i + 1)")
      runItems.append(run)
    }
    pickerRun.setItems(runItems)
    pickerRun.setSelectedItemIndex(1)
  }

  override func didDeactivate() {
    super.didDeactivate()
  }

  // MARK: - IBActions

  @IBAction func pickerRunValueChanged(_ value: Int) {
    selectedRunTime = Int(value)
    btnStart.setTitle("Start \(selectedRunTime * 5) min run")
  }

  // MARK: - Navigation

  override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
    guard segueIdentifier == "toTimer" else { return nil }
    return selectedRunTime
  }
}
