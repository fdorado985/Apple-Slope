//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// Declare an empty array of type String
var movieTitles = [String]()

// Declare an explicit array of type Double and initialize it with 4 values
var dailyTips: [Double] = [12.0, 14.5, 23.0, 35.5]

// Declare an array and initialize it with 5 values of any type using type inference
var myFamilyAges = [26, 28, 13, 56, 58]

// Use append 3 times in each array
movieTitles.append("Annabelle")
movieTitles.append("Dead Silence")
movieTitles.append("The Conjure")

dailyTips.append(23.4)
dailyTips.append(53.8)
dailyTips.append(12.0)

myFamilyAges.append(24)
myFamilyAges.append(10)
myFamilyAges.append(2)

// Use remove(at:) in eachArray
movieTitles.remove(at: 2)
dailyTips.remove(at: 0)
myFamilyAges.remove(at: 3)

// Use removeAll on one array
myFamilyAges.removeAll()

// Use a new array feature that you haven't learned yer
movieTitles.contains { (value) -> Bool in
  value == "Annabelle"
}
