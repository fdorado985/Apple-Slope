//: Playground - noun: a place where people can play

import UIKit

class Vehicle {

  var tires = 4
  var headlights = 2
  var horsepower = 468
  var model = ""

  func drive() {
    // Accelerate The Vehicle
  }

  func brake() {
    // Stop The Vehicle
  }
}

let bmw = Vehicle()
bmw.model = "328i"

let ford = Vehicle()
ford.model = "F150"
ford.brake()

// The data is passed by reference not value
func passByReference(vehicle: Vehicle) {
  vehicle.model = "Cheese"
}

print(ford.model)
passByReference(vehicle: ford)
print(ford.model)

var someonesAge = 20

// The data is passed by value, not reference
func passByValue(age: Int) {
  //age = 10 // <== Cannot change a value
}

passByValue(age: someonesAge)
