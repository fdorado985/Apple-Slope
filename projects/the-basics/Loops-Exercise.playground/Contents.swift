//: Playground - noun: a place where people can play

import UIKit

// Create an empty Array of type Int Called oddNumbers
var oddNumbers = [Int]()

// Using a standard for-in loop add all odd numbers less than or equal to 100
for x in 1...100 {
  if x % 2 == 0 {
    oddNumbers.append(x)
  }
}

// Create a second array called sums of type Int
var sums = [Int]()

// Using a for-in, iterate through oddNumbers array and add the current iteration value + 5 to the sums array
for item in oddNumbers {
  sums.append(item + 5)
}

// Using a repeat while loop, iterate through the sums array and print "The sum is x" where x is the current value of the iteration (ie. The sum is: 15)
var index = 0
repeat {
  print("The sum is \(sums[index])")
  index += 1
} while (index < oddNumbers.count)
