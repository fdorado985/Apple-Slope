# The Basics

This folder has some playgrounds with examples of different kind of basic topics on Swift

On the table of content you will see the `playground` name on parentheses.

## Table of Content
* Constants & Logical Operators (Logical Operators)
* Arrays (Arrays)
  * Arrays Exercise (Arrays-Exercise)
* Loops (loopity-loops)
  * Loops Exercise (Loops-Exercise)
* Dictionaries (Dictionaries)
* Object Oriented Programming (Objects and Classes)
* Inheritance (Inheritance)
* Polymorphism (Polymorphism)
