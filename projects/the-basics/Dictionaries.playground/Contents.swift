//: Playground - noun: a place where people can play

import UIKit

var namesOfIntegers = [Int : String]()

namesOfIntegers[3] = "three"
namesOfIntegers[44] = "forty four"

namesOfIntegers = [:]

var airports = ["YYZ" : "Toronto Pearson", "LAX" : "Los Angeles International"]
print("The Airports Dictionary has: \(airports.count) items")

if airports.isEmpty {
  print("The airport dictionary is empty")
}

airports["LHR"] = "London"
airports["LHR"] = "London Heathrow"
airports["DEV"] = "Devslopes International"

airports["DEV"] = nil // This helps you to erase a value

for (airportCode, airportName) in airports {
  print("\(airportCode) : \(airportName)")
}

for key in airports.keys {
  print("Key: \(key)")
}

for value in airports.values {
  print("Value: \(value)")
}
