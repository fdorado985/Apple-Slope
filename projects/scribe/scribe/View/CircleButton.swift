//
//  CircleButton.swift
//  Scribe
//
//  Created by Juan Francisco Dorado Torres on 08/11/16.
//  Copyright © 2016 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

@IBDesignable
class CircleButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 30.0 {
        didSet {
            setupView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = cornerRadius
    }
}
