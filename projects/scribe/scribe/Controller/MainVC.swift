//
//  MainVC.swift
//  scribe
//
//  Created by Juan Francisco Dorado Torres on 8/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

class MainVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var activitySpinner: UIActivityIndicatorView!
    @IBOutlet weak var transcriptionTextField: UITextView!
    
    // MARK: Properties
    
    private var audioPlayer: AVAudioPlayer!

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activitySpinner.isHidden = true
    }
    
    // MARK: IBActions
    
    @IBAction func btPlayToTranscribePressed(_ sender: CircleButton) {
        activitySpinner.isHidden = false
        activitySpinner.startAnimating()
        requestSpeechAuth()
    }
    
    // MARK: Private
    
    private func requestSpeechAuth() {
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            if authStatus == SFSpeechRecognizerAuthorizationStatus.authorized {
                if let path = Bundle.main.url(forResource: "demo", withExtension: "m4a") {
                    do {
                        let sound = try AVAudioPlayer(contentsOf: path)
                        self.audioPlayer = sound
                        self.audioPlayer.delegate = self
                        sound.play()
                    } catch let err {
                        print(err.localizedDescription)
                    }
                    
                    let recognizer = SFSpeechRecognizer()
                    let request = SFSpeechURLRecognitionRequest(url: path)
                    recognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            //print(result?.bestTranscription.formattedString ?? "")
                            self.transcriptionTextField.text = result?.bestTranscription.formattedString
                        }
                    })
                }
            }
        }
    }
}

// MARK: - AVAudioPlayer Delegate

extension MainVC: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        player.stop()
        activitySpinner.stopAnimating()
        activitySpinner.isHidden = true
    }
}
