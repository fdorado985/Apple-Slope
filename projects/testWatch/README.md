
# Test Watch

## Description
On this app we are going to see how Apple Watch ⌚️ apps work, creating an `iOS` project and extending to a `watchOS`.

## Rules To Take In Mind
* First, on `iOS` and `macOS` apps we want to the user spend the most time he can on our app.
* `watchOS` because of the position of the hand, the small screen and the limited buttons to action, Apple use the apps as extension of the `iOS` app for a quick interaction, as Apple says 2 seconds of interaction.
* This philosophy brings three maintenance of the Apple Watch experience.
  * Glanceable
  * Actionable
  * Responsive

## Glanceable
Glanceable apps make the most important information readily available to the user. Interactions occur over short periods of time, so apps most convey the most important information up front and communicate that information clearly and without distraction.

## Actionable
Actionable apps are mindful of the information they present to the user. An actionable app anticipates the user's needs by ensuring that what's onscreen is always current and relevant. It takes advantage of opportunities to update its interface, using background time to fetch new data, update its complication, or refresh its snapshot.

## Responsive
Interactions with your app should be quick. A responsive app provides a complication and keeps its snapshot up-to-date. It minimizes the time it takes to launch and load new screens. It responds to user interactions with immediate feedback about what the app is going to do, and then uses notifications to deliver progress updates later.

## Images on Apple watchOS
As you know on `iOS` apps we use to use PNG images, for Watch use PNG is ok, but we have to take care to don't use transparency.

> For better and complete information i recommend you to take a look to the [Human Interface Guidelines for watchOS](https://developer.apple.com/watchos/human-interface-guidelines/overview/themes/)
