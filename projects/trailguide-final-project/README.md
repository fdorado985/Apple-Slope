#  TRAILguide

This app is the final project of the Beginner section of the Apple Slope.

## Description
It is an app that shows the types of things you might need to bring with you if you are participating in an outdoor activity like camping, diving, fishing, etc.

## Demo
![trailguide_demo](screenshots/trailguide_demo.gif)
