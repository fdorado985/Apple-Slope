//
//  Category.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 17/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

struct Category {

  // MARK: - Properties

  private(set) var name: String
  private(set) var image: UIImage?
  private(set) var type: CategoryType

  // MARK: - Initialization
  
  init(name: String, imageName: String, type: CategoryType) {
    self.name = name
    self.image = UIImage(named: imageName)
    self.type = type
  }
}
