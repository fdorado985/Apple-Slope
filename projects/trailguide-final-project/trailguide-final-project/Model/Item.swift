//
//  Item.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 17/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

struct Item {

  // MARK: - Properties

  private(set) var name: String
  private(set) var details: String
  private(set) var image: UIImage?

  // MARK: - Initialization

  init(name: String, details: String, image: String) {
    self.name = name
    self.details = details
    self.image = UIImage(named: image)
  }
}
