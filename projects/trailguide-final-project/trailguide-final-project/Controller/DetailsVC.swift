//
//  DetailsVC.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 17/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var imgItem: UIImageView!
  @IBOutlet weak var txtName: UILabel!
  @IBOutlet weak var txtDetails: UILabel!
  @IBOutlet weak var imgBackground: UIImageView!

  var item: Item?
  var background: UIImage?

  // MARK: - DetailsVC View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }

  // MARK: - Functions

  private func setupView() {
    guard let item = item else { return }
    imgItem.image = item.image
    txtName.text = item.name
    txtDetails.text = item.details
    imgBackground.image = background
  }
}
