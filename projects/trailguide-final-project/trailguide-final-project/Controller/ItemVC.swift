//
//  ItemVC.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 16/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ItemVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var imgBackground: UIImageView!
  @IBOutlet weak var txtTitle: UILabel!
  @IBOutlet weak var txtEmpty: UILabel!
  @IBOutlet weak var viewEmptyContainer: UIView!
  @IBOutlet weak var collectionItems: UICollectionView!

  // MARK: - Properties

  var category: Category?
  var items: [Item] = []

  // MARK: - Private Properties

  private let emptyMessage = "😮 Oh Oh!, it looks that we don't have anything, maybe if you go back and return several times we could load something 😅 or maybe not 🤷🏽‍♂️"

  // MARK: - SpecificCategoryVC View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    guard let category = category else { return }
    items = DataService.instance.getItems(for: category.type)
    setupView()
  }

  // MARK: - Functions

  private func setupView() {
    guard let category = category else { return }
    txtTitle.text = category.name
    imgBackground.image = category.image
    txtEmpty.isHidden = items.count > 0
    viewEmptyContainer.isHidden = items.count > 0
    collectionItems.isHidden = items.count == 0
    txtEmpty.text = emptyMessage
  }

  // MARK: - Navigation

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "DetailsVC",
      let vc = segue.destination as? DetailsVC,
      let selectedItem = sender as? Item {
      vc.item = selectedItem
      vc.background = imgBackground.image
    }
  }
}

extension ItemVC: UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    let selectedItem = items[indexPath.row]
    performSegue(withIdentifier: "DetailsVC", sender: selectedItem)
  }
}

extension ItemVC: UICollectionViewDataSource {

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemcell", for: indexPath) as? ItemCell else {
      return UICollectionViewCell()
    }
    let selectedItem = items[indexPath.row]
    cell.configureCell(with: selectedItem)

    return cell
  }

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
}
