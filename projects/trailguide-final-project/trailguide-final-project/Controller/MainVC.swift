//
//  MainVC.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 14/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

  // MARK: - Properties

  var allCategories: [Category] = []

  // MARK: - MainVC View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    allCategories = DataService.instance.getCategories()
  }

  // MARK: - Navigation

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ItemVC",
      let specificCategoryVC = segue.destination as? ItemVC,
      let category = sender as? Category {
      specificCategoryVC.category = category
    }
  }
}

extension MainVC: UITableViewDelegate {

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 130
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    let categorySelected = allCategories[indexPath.row]
    performSegue(withIdentifier: "ItemVC", sender: categorySelected)
  }
}

extension MainVC: UITableViewDataSource {

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "categorycell", for: indexPath) as? CategoryCell else {
      return UITableViewCell()
    }
    let category = allCategories[indexPath.row]
    cell.configureCell(with: category)
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return allCategories.count
  }
}
