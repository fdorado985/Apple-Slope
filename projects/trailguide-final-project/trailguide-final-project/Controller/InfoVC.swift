//
//  InfoVC.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 15/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class InfoVC: UIViewController {

  // MARK: - InfoVC View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: - IBActions

  @IBAction func btnBackToTheAppTapped(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
  }
}
