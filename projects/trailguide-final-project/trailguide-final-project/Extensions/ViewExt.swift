//
//  ViewExt.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 17/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

private var _cornerRadius: CGFloat = 0.0

@IBDesignable
extension UIView {


  @IBInspectable var cornerRadius: CGFloat {
    get {
      return _cornerRadius
    }

    set {
      _cornerRadius = newValue
      self.layer.cornerRadius = _cornerRadius
      self.layer.masksToBounds = _cornerRadius != 0.0
    }
  }

}

