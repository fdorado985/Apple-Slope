//
//  NavigationItemExt.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 14/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

private var _titleImage = UIImage(named: "TRAILguide")
private var _isBackTextHidden = false

@IBDesignable
extension UINavigationItem {

  @IBInspectable var titleImage: UIImage? {
    get {
      return _titleImage
    }

    set {
      _titleImage = newValue
      let imageView = UIImageView(image:_titleImage)
      self.titleView = imageView
    }
  }

  @IBInspectable var isBackTextHidden: Bool {
    get {
      return _isBackTextHidden
    }

    set {
      _isBackTextHidden = newValue
      if isBackTextHidden {
        backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
      }
    }
  }
}
