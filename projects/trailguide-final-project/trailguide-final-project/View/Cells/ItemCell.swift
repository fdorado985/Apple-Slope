//
//  ItemCell.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 17/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {

  @IBOutlet weak var imgItem: UIImageView!

  func configureCell(with item: Item) {
    imgItem.image = item.image
  }
}
