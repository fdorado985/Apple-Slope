//
//  CategoryCell.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 14/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

  // MARK: - IBOutlet

  @IBOutlet weak var txtTitle: UILabel!
  @IBOutlet weak var imgBackround: UIImageView!

  // MARK: - Function

  func configureCell(with category: Category) {
    imgBackround.image = category.image
    txtTitle.text = category.name
  }
}
