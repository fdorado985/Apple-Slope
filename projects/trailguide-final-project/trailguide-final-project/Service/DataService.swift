//
//  DataService.swift
//  trailguide-final-project
//
//  Created by Juan Francisco Dorado Torres on 19/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum CategoryType {
  case backpacks
  case camping
  case diving
  case fishing
  case hiking
  case rv
}

class DataService {

  // MARK: - Singleton

  static let instance = DataService()

  // MARK: - Properties

  private let categories = [
    Category(name: "BACKPACKS", imageName: "backpackingBG", type: .backpacks),
    Category(name: "CAMPING", imageName: "campingBG", type: .camping),
    Category(name: "DIVING", imageName: "divingBG", type: .diving),
    Category(name: "FISHING", imageName: "fishingBG", type: .fishing),
    Category(name: "HIKING", imageName: "hikingBG", type: .hiking),
    Category(name: "RV LIFE", imageName: "rvBG", type: .rv)
  ]

  private let backpacks: [Item] = []
  private let camping = [
    Item(
      name: "CAMPING STOVE",
      details: "A portable stove is a cooking stove specially designed to be portable and lightweight, used in camping, picnicking, backpacking, or other use in remote locations where an easily transportable means of cooking or heating is needed.",
      image: "campingStove"),
    Item(
      name: "FOOD COOLER",
      details: "Coolers are often taken on picnics, and on vacation or holiday. Where summers are hot, they may also be used just for getting cold groceries home from the store, such as keeping ice cream from melting in a hot automobile. Even without adding ice, this can be helpful, particularly if the trip home will be lengthy. Some coolers have built-in cupholders in the lid.",
      image: "foodCooler"),
    Item(
      name: "PARACORD BRACELET",
      details: "Parachute cord (also paracord or 550 cord when referring to type-III paracord) is a lightweight nylon kernmantle rope originally used in the suspension lines of parachutes. This cord is useful for many other tasks and is now used as a general purpose utility cord by both military personnel and civilians.",
      image: "paracordBracelet"),
    Item(
      name: "PUP TENT",
      details: "A tent About this is a shelter consisting of sheets of fabric or other material draped over, attached to a frame of poles or attached to a supporting rope. While smaller tents may be free-standing or attached to the ground, large tents are usually anchored using guy ropes tied to stakes or tent pegs.",
      image: "pupTent"),
    Item(
      name: "SLEEPING BAG",
      details: "A sleeping bag is an insulated covering for a person, essentially a lightweight quilt that can be closed with a zipper or similar means to form a tube, which functions as lightweight, portable bedding in situations where a person is sleeping outdoors (e.g. when camping, hiking, hill walking or climbing). Its primary purpose is to provide warmth and thermal insulation through its synthetic or down insulation.",
      image: "sleepingBag"),
    Item(
      name: "TACTICAL KNIFE",
      details: "A fighting knife is a knife with a blade designed to inflict a lethal injury in a physical confrontation between two or more individuals at very short range (grappling distance).[1][2][3][4] The combat knife and the trench knife are examples of military fighting knives.",
      image: "tacticalKnife")
  ]
  private let diving: [Item] = []
  private let fishing: [Item] = []
  private let hiking: [Item] = []
  private let rvLife: [Item] = []

  // MARK: - Functions

  func getCategories() -> [Category] {
    return categories
  }

  func getItems(for category: CategoryType) -> [Item] {
    switch category {
    case .backpacks:
      return getBackpacks()
    case .camping:
      return getCamping()
    case .diving:
      return getDiving()
    case .fishing:
      return getFishing()
    case .hiking:
      return getHiking()
    case .rv:
      return getRVLife()
    }
  }

  // MARK: - Private Functions

  private func getBackpacks() -> [Item] {
    return backpacks
  }

  private func getCamping() -> [Item] {
    return camping
  }

  private func getDiving() -> [Item] {
    return diving
  }

  private func getFishing() -> [Item] {
    return fishing
  }

  private func getHiking() -> [Item] {
    return hiking
  }

  private func getRVLife() -> [Item] {
    return rvLife
  }
}
