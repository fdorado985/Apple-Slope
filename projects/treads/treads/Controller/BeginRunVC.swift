//
//  BeginRunVC.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class BeginRunVC: LocationVC {
    
    // MARK: IBOutlets
    
    @IBOutlet weak private var mapView: MKMapView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblPace: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var viewLastRun: UIView!
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationAuthStatus()
        mapView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager?.delegate = self
        manager?.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupMapView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        manager?.stopUpdatingLocation()
    }
    
    // MARK: IBActions
    
    @IBAction private func btnLocationCenterTapped(_ sender: UIButton) {
        centerMapOnUserLocation()
    }
    
    @IBAction private func btnCloseTapped(_ sender: UIButton) {
        viewLastRun.isHidden = true
        centerMapOnUserLocation()
    }
    
    // MARK: Private
    
    func addLastRunToMap() -> MKPolyline? {
        guard let lastRun = Run.getAllRuns()?.first else { return nil }
        lblPace.text = "Pace: \(lastRun.pace.formatTimeDurationToString())"
        lblDistance.text = "Distance: \(lastRun.distance.metersToMiles(places: 2)) mi"
        lblDuration.text = "Duration: \(lastRun.duration.formatTimeDurationToString())"
        
        var coordinate = [CLLocationCoordinate2D]()
        for location in lastRun.locations {
            coordinate.append(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        
        mapView.userTrackingMode = .none
        guard let locations = Run.getRun(by: lastRun.id)?.locations else { return MKPolyline() } // This part is redundant
        mapView.setRegion(centerMapOnPreviousRoute(locations: locations), animated: true) // Can use lastRun.locations instead of the assign value above
        
        return MKPolyline(coordinates: coordinate, count: lastRun.locations.count)
    }
    
    func setupMapView() {
        if let overlay = addLastRunToMap() {
            if mapView.overlays.count > 0 {
                mapView.removeOverlays(mapView.overlays)
            }
            mapView.add(overlay)
            viewLastRun.isHidden = false
        } else {
            viewLastRun.isHidden = true
            centerMapOnUserLocation()
        }
    }
    
    private func centerMapOnUserLocation() {
        mapView.userTrackingMode = .follow
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 500, 500)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func centerMapOnPreviousRoute(locations: List<Location>) -> MKCoordinateRegion {
        guard let initialLocation = locations.first else { return MKCoordinateRegion() }
        var minLat = initialLocation.latitude
        var minLon = initialLocation.longitude
        var maxLat = minLat
        var maxLon = minLon
        
        for location in locations {
            minLat = min(minLat, location.latitude)
            minLon = min(minLon, location.longitude)
            maxLat = max(maxLat, location.latitude)
            maxLon = max(maxLon, location.longitude)
        }
        
        return MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: (minLat + maxLat) / 2, longitude: (minLon + maxLon) / 2), span: MKCoordinateSpan(latitudeDelta: (maxLat - minLat) * 1.4, longitudeDelta: (maxLon - minLon) * 1.4))
    }
}

// MARK: - CLLocationManager Delegate

extension BeginRunVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
            mapView.showsUserLocation = true
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polyline = overlay as! MKPolyline
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = #colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
        renderer.lineWidth = 4
        return renderer
    }
}

