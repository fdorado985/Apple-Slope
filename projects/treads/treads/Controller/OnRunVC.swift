//
//  OnRunVC.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class OnRunVC: LocationVC {

    // MARK: IBOutlets
    
    @IBOutlet weak var imgSwipeBG: UIImageView!
    @IBOutlet weak var imgSlider: UIImageView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblPace: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnPause: UIButton!
    
    // MARK: Properties
    
    private var startLocation: CLLocation!
    private var lastLocation: CLLocation!
    private var runDistance = 0.0
    private var pace = 0
    private var counter = 0
    private var timer = Timer()
    private var coordinateLocations = List<Location>()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(endRunSwiped(_:)))
        swipeGesture.delegate = self as? UIGestureRecognizerDelegate
        imgSlider.addGestureRecognizer(swipeGesture)
        imgSlider.isUserInteractionEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager?.delegate = self
        manager?.distanceFilter = 10
        startRun()
    }
    
    // MARK: IBActions
    
    @IBAction func btnPauseTapped(_ sender: UIButton) {
        if timer.isValid {
            pauseRun()
        } else {
            startRun()
        }
    }
    
    // MARK: Private
    
    @objc private func endRunSwiped(_ sender: UIPanGestureRecognizer) {
        let minAdjust: CGFloat = 80
        let maxAdjust: CGFloat = 128
        if let sliderView = sender.view {
            if sender.state == UIGestureRecognizerState.began || sender.state == UIGestureRecognizerState.changed {
                let translation = sender.translation(in: self.view)
                if sliderView.center.x >= (imgSwipeBG.center.x - minAdjust) && sliderView.center.x <= (imgSwipeBG.center.x + maxAdjust) {
                    sliderView.center.x = sliderView.center.x + translation.x
                } else if sliderView.center.x >= (imgSwipeBG.center.x + maxAdjust) {
                    sliderView.center.x = imgSwipeBG.center.x + maxAdjust
                    endRun()
                    dismiss(animated: true)
                } else {
                    sliderView.center.x = imgSwipeBG.center.x - minAdjust
                }
                
                sender.setTranslation(CGPoint.zero, in: self.view)
            } else if sender.state == UIGestureRecognizerState.ended {
                UIView.animate(withDuration: 0.1) {
                    sliderView.center.x = self.imgSwipeBG.center.x - minAdjust
                }
            }
        }
    }
    
    private func startRun() {
        manager?.startUpdatingLocation()
        startTimer()
        btnPause.setImage(#imageLiteral(resourceName: "pauseButton"), for: .normal)
    }
    
    private func endRun() {
        manager?.stopUpdatingLocation()
        Run.addRunToRealm(pace: pace, distance: runDistance, duration: counter, locations: coordinateLocations)
    }
    
    private func startTimer() {
        lblDuration.text = counter.formatTimeDurationToString()
        timer = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(updateCounter),
            userInfo: nil,
            repeats: true
        )
    }
    
    private func pauseRun() {
        startLocation = nil
        lastLocation = nil
        timer.invalidate()
        manager?.stopUpdatingLocation()
        btnPause.setImage(#imageLiteral(resourceName: "resumeButton"), for: .normal)
    }
    
    @objc private func updateCounter() {
        counter += 1
        lblDuration.text = counter.formatTimeDurationToString()
    }
    
    private func calculatePace(time second: Int, miles: Double) -> String {
        pace = Int(Double(second) / miles)
        debugPrint(pace)
        return pace.formatTimeDurationToString()
    }
    
    // MARK: Public
}

// MARK: - CLLocationManager Delegate

extension OnRunVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            runDistance += lastLocation.distance(from: location)
            let newLocation = Location(latitude: Double(lastLocation.coordinate.latitude), longitude: Double(lastLocation.coordinate.longitude))
            coordinateLocations.insert(newLocation, at: 0)
            lblDistance.text = "\(runDistance.metersToMiles(places: 2))"
            if counter > 0 && runDistance > 0 {
                lblPace.text = calculatePace(time: counter, miles: runDistance.metersToMiles(places: 2))
            }
        }
        
        lastLocation = locations.last
    }
}
