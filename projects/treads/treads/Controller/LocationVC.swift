//
//  LocationVC.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit

class LocationVC: UIViewController {
    
    // MARK: Properties
    
    var manager: CLLocationManager?

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CLLocationManager()
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        manager?.activityType = .fitness
    }
    
    // MARK: Private
    
    func checkLocationAuthStatus() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            manager?.requestWhenInUseAuthorization()
        }
    }

}

extension LocationVC: MKMapViewDelegate {
    
}
