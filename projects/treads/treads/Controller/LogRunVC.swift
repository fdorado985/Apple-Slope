//
//  LogRunVC.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class LogRunVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableLogs: UITableView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension LogRunVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Run.getAllRuns()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LogRunCell", for: indexPath) as? LogRunCell else { return UITableViewCell() }
        
        if let logRun = Run.getAllRuns()?[indexPath.row] {
            cell.configureCell(run: logRun)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}

