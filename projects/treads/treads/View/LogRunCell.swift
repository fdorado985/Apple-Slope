//
//  LogRunCell.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class LogRunCell: UITableViewCell {

    // MARK: IBOutlets
    
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblAvgPace: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    // MARK: Public
    
    func configureCell(run: Run) {
        lblDuration.text = run.duration.formatTimeDurationToString()
        lblDistance.text = "\(run.distance.metersToMiles(places: 2)) mi"
        lblAvgPace.text = run.pace.formatTimeDurationToString()
        lblDate.text = run.date.getDateString()
    }

}
