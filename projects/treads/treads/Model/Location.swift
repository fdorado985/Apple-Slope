//
//  Location.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import RealmSwift

class Location: Object {
    
    // MARK: Properties
    
    @objc dynamic private(set) var latitude = 0.0
    @objc dynamic private(set) var longitude = 0.0
    
    // MARK: Init
    
    convenience init(latitude: Double, longitude: Double) {
        self.init()
        self.latitude = latitude
        self.longitude = longitude
    }
}
