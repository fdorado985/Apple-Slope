//
//  Run.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import RealmSwift

class Run: Object {
    
    // MARK: Properties
    
    @objc dynamic private(set) var id = ""
    @objc dynamic private(set) var date = NSDate()
    @objc dynamic private(set) var pace = 0
    @objc dynamic private(set) var distance = 0.0
    @objc dynamic private(set) var duration = 0
    private(set) var locations = List<Location>()
    
    // MARK: Primary Key (Realm)
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: Indexed Properties (Realm)
    
    override static func indexedProperties() -> [String] {
        return ["pace", "date", "duration"]
    }
    
    // MARK: Convenience Init
    
    convenience init(pace: Int, distance: Double, duration: Int, locations: List<Location>) {
        self.init()
        self.id = UUID().uuidString.lowercased()
        self.date = NSDate()
        self.pace = pace
        self.distance = distance
        self.duration = duration
        self.locations = locations
    }
    
    // MARK: Functions
    
    static func addRunToRealm(pace: Int, distance: Double, duration: Int, locations: List<Location>) {
        REALM_QUEUE.sync {
            let run = Run(pace: pace, distance: distance, duration: duration, locations: locations)
            do {
                let realm = try Realm(configuration: RealmConfig.runDataConfig)
                try realm.write {
                    realm.add(run)
                    try realm.commitWrite()
                }
            } catch let error {
                debugPrint("Error adding run to realm: \(error.localizedDescription)")
            }
        }
    }
    
    static func getAllRuns() -> Results<Run>? {
        do {
            let realm = try Realm(configuration: RealmConfig.runDataConfig)
            var runs = realm.objects(Run.self)
            runs = runs.sorted(byKeyPath: "date", ascending: false)
            return runs
        } catch let error {
            debugPrint("Error getting all Runs: \(error.localizedDescription)")
            return nil
        }
    }
    
    static func getRun(by id: String) -> Run? {
        do {
            let realm = try Realm(configuration: RealmConfig.runDataConfig)
            let run = realm.object(ofType: Run.self, forPrimaryKey: id)
            return run
        } catch let error {
            debugPrint("Cannot get Run by id: \(error.localizedDescription)")
            return nil
        }
    }
}
