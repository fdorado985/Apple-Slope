//
//  Constants.swift
//  treads
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

let REALM_QUEUE = DispatchQueue(label: "realmQueue")
let REALM_RUN_CONFIG = "realmRunConfig"
