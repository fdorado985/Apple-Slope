#  Treads
This is a clone of a popular app `Nike Run` where are able to track your run progress, also here you will be able to see how `Realm database` works... that we are gonna be using it to save our logs.

## Demo
![treads_demo](screenshots/treads_demo.gif)
