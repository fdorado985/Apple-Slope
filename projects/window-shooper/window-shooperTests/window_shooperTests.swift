//
//  window_shooperTests.swift
//  window-shooperTests
//
//  Created by Juan Francisco Dorado Torres on 05/08/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import XCTest

class window_shooperTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    super.tearDown()
  }

  func testGetHours() {
    XCTAssert(Wage.getHours(for: 25, and: 100) == 4)
    XCTAssert(Wage.getHours(for: 15.5, and: 250.53) == 17)
  }

}
