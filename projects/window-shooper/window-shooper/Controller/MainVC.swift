//
//  MainVC.swift
//  window-shooper
//
//  Created by Juan Francisco Dorado Torres on 05/08/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

  // MARK: - Initialization

  @IBOutlet weak var txtWage: CurrencyTextField!
  @IBOutlet weak var txtPrice: CurrencyTextField!
  @IBOutlet weak var lblResult: UILabel!
  @IBOutlet weak var lblHours: UILabel!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()

    createCalculateButton()
  }

  // MARK: - IBActions

  @IBAction func btnClearTapped(sender: UIButton) {
    lblResult.isHidden = true
    lblHours.isHidden = true
    txtWage.text = ""
    txtPrice.text = ""
  }

  // MARK: - Functions

  private func createCalculateButton() {
    // Create and customize Calculate button
    let calcBtn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 60))
    calcBtn.backgroundColor = #colorLiteral(red: 1, green: 0.7102370921, blue: 0.288365967, alpha: 1)
    calcBtn.setTitle("Calculate", for: .normal)
    calcBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
    calcBtn.addTarget(self, action: #selector(MainVC.calculate), for: .touchUpInside)

    // Add it ass accessory view
    txtWage.inputAccessoryView = calcBtn
    txtPrice.inputAccessoryView = calcBtn

    lblResult.isHidden = true
    lblHours.isHidden = true
  }

  @objc private func calculate() {
    if let txtWage = txtWage.text, let txtPrice = txtPrice.text {
      if let wage = Double(txtWage), let price = Double(txtPrice) {
        view.endEditing(true)
        lblResult.isHidden = false
        lblHours.isHidden = false
        lblResult.text = "\(Wage.getHours(for: wage, and: price))"
      }
    }
  }
}
