//
//  CurrencyTextField.swift
//  window-shooper
//
//  Created by Juan Francisco Dorado Torres on 05/08/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

@IBDesignable
class CurrencyTextField: UITextField {

  override func prepareForInterfaceBuilder() {
    customizeView()
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    customizeView()
  }

  override func draw(_ rect: CGRect) {
    let size: CGFloat = 20

    // Create the Label for the currency symbol
    let lblCurrency = UILabel(frame: CGRect(x: 5, y: (frame.size.height / 2) - size / 2, width: size, height: size))
    lblCurrency.backgroundColor = #colorLiteral(red: 0.9451516452, green: 0.9451516452, blue: 0.9451516452, alpha: 0.8021992723)
    lblCurrency.textAlignment = .center
    lblCurrency.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    lblCurrency.layer.cornerRadius = 5.0
    lblCurrency.clipsToBounds = true

    // Create the formatter symbol
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.locale = .current

    // Add the currency symbol to the label
    lblCurrency.text = formatter.currencySymbol

    // Add the label to the textfield view
    addSubview(lblCurrency)
  }

  private func customizeView() {
    // Configure the layer of the textField
    backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2492776113)
    layer.cornerRadius = 5.0
    clipsToBounds = true

    // Configure the text and placeholder inside textField
    textAlignment = .center
    textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    let place = NSAttributedString(string: placeholder ?? " ", attributes: [.foregroundColor : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)])
    attributedPlaceholder = place
  }
}
