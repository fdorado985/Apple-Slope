//
//  Wage.swift
//  window-shooper
//
//  Created by Juan Francisco Dorado Torres on 05/08/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Wage {

  class func getHours(for wage: Double, and price: Double) -> Int {
    return Int(ceil(price / wage))
  }
}
