# Window-Shooper

This app has been created using simple rules and coded it like if you were going to upload it to the AppStore!

You are gonna be using some `stackview`, `autolayouts`, `constraints`, add `subviews`.

And must important `Unit Test`

Let a button to be always on the top of your keyboard.

## Demo
![window_shooper_demo](screenshots/demo_window_shooper.gif)

## Pro Tips

> To set text on UIButton
> use `.setTitle` and
> DON'T use `.titleLabel`

>---
> * Good functions accept input and return output.
> * Bad functions set global variables and rely on other functions to work

>---
> Always run (and pass) your unit tests before you:
> * Create pull requests
> * Deploy to device
> * Send app to beta testers
> * Submit apps to App Store!!!

>---
> Remember always set `clipsToBounds` where
> you need it, this prevents you a headache!
