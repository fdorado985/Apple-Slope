# HTCHHKR - Uber Clone
This app works as a simple Uber Clone, a little bit complex, where you will be able to see `Firebase`, `didEnterRegion:` `didExitRegion:` functions, `MapKit`, `MKAnnotations`, `CustomAnnotations`, `Custom Views`, `Extensions`, `Protocols` for Alerts, `CLLocation`, and much more...

## Demo
### Request Ride
![htchhkr_request_ride](.screenshots/htchhkr_request_ride.gif)

### Start Trip
![htchhkr_start_trip](.screenshots/htchhkr_start_trip.gif)

### Get Directions
![htchhkr_get_directions](.screenshots/htchhkr_get_directions.gif)

### End Trip
![htchhkr_end_trip](.screenshots/htchhkr_end_trip.gif)
