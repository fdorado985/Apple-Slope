//
//  CenterVCDelegate.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol CenterVCDelegate {
    
    func toggleLeftPanel()
    func addLeftPanelViewController()
    func animateLeftPanel(shouldExpand: Bool)
}
