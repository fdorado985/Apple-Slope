//
//  Alertable.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/24/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol Alertable {}

extension Alertable where Self: UIViewController {
    
    func showAlert(title: String = "Error", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let btnAcceptAction = UIAlertAction(title: "Accept", style: .default)
        alert.addAction(btnAcceptAction)
        present(alert, animated: true)
    }
}
