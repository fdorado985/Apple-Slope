//
//  PickupVC.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class PickupVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var mapViewPickup: RoundedMapView!
    
    // MARK: Properties
    
    private let regionRadius: CLLocationDistance = 2000
    private var pin: MKPlacemark? = nil
    private var pickupCoordinate: CLLocationCoordinate2D?
    private var passengerKey: String?
    private var locationPlacemark: MKPlacemark!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let pickupCoordinate = self.pickupCoordinate else { return }
        locationPlacemark = MKPlacemark(coordinate: pickupCoordinate)
        dropPin(for: locationPlacemark)
        centerMapOnLocation(location: locationPlacemark.location!)
        
        guard let passengerKey = self.passengerKey else { return }
        DataService.instance.REF_TRIPS.child(passengerKey).observe(.value) { (snapshot) in
            if snapshot.exists() {
                if snapshot.childSnapshot(forPath: TRIP_IS_ACCEPTED).value as? Bool == true {
                    self.dismiss(animated: true)
                }
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    // MARK: Initialization
    
    func initData(coordinate: CLLocationCoordinate2D, passengerKey: String) {
        self.pickupCoordinate = coordinate
        self.passengerKey = passengerKey
    }
    
    // MARK: IBActions
    
    @IBAction func btnAcceptTripTapped(_ sender: RoundedShadowButton) {
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let passengerKey = self.passengerKey else { return }
        UpdateService.instance.acceptTrip(withPassenger: passengerKey, forDriver: currentUser.uid)
        presentingViewController?.shouldPresentLoadingView(status: true)
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

extension PickupVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "pickupPoint"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "destinationAnnotation")
        return annotationView
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        
        mapViewPickup.setRegion(coordinateRegion, animated: true)
    }
    
    private func dropPin(for placemark: MKPlacemark) {
        pin = placemark
        for annotation in mapViewPickup.annotations {
            mapViewPickup.removeAnnotation(annotation)
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        
        mapViewPickup.addAnnotation(annotation)
    }
}
