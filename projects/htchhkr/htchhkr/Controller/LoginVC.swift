//
//  LoginVC.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController, Alertable {
    
    // MARK: IBActions
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var txtEmail: RoundedCornerTextField!
    @IBOutlet weak var txtPassword: RoundedCornerTextField!
    @IBOutlet weak var btnAuth: RoundedShadowButton!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.bindToKeyboard()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    // MARK: IBActions
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func btnAuthTapped(_ sender: RoundedShadowButton) {
        guard let email = txtEmail.text, !email.isEmpty else { return }
        guard let password = txtPassword.text, !password.isEmpty else { return }
        btnAuth.animateButton(shouldLoad: true, message: nil)
        view.endEditing(true)
        
        Auth.auth().signIn(withEmail: email, password: password) { (authData, error) in
            if let error = error {
                if let errorCode = AuthErrorCode(rawValue: error._code) {
                    switch errorCode {
                    case .wrongPassword:
                        self.showAlert(message: ERROR_MSG_WRONG_PASSWORD)
                    default:
                        self.showAlert(message: ERROR_MSG_UNEXPECTED_ERROR)
                    }
                }
                
                Auth.auth().createUser(withEmail: email, password: password, completion: { (authData, error) in
                    if let error = error {
                        if let errorCode = AuthErrorCode(rawValue: error._code) {
                            switch errorCode {
                            case .emailAlreadyInUse:
                                self.showAlert(message: ERROR_MSG_EMAIL_ALREADY_IN_USE)
                            case .invalidEmail:
                                self.showAlert(message: ERROR_MSG_INVALID_EMAIL)
                            default:
                                self.showAlert(message: ERROR_MSG_UNEXPECTED_ERROR)
                            }
                        }
                        return
                    }
                    
                    guard let authData = authData else { return }
                    if self.segmentControl.selectedSegmentIndex == 0 {
                        let userData = ["provider" : authData.user.providerID] as [String : Any]
                        DataService.instance.createUser(uid: authData.user.uid, userData: userData)
                    } else {
                        let userData = ["provider" : authData.user.providerID, USER_IS_DRIVER : true, ACCOUNT_PICKUP_MODE_ENABLED : false, DRIVER_IS_ON_TRIP : false] as [String : Any]
                        DataService.instance.createUser(uid: authData.user.uid, userData: userData, isDriver: true)
                    }
                })
                self.dismiss(animated: true)
            }
            
            guard let authData = authData else { return }
            if self.segmentControl.selectedSegmentIndex == 0 {
                let userData = ["provider" : authData.user.providerID] as [String : Any]
                DataService.instance.createUser(uid: authData.user.uid, userData: userData)
            } else {
                let userData = ["provider" : authData.user.providerID, USER_IS_DRIVER : true, ACCOUNT_PICKUP_MODE_ENABLED : false, DRIVER_IS_ON_TRIP : false] as [String : Any]
                DataService.instance.createUser(uid: authData.user.uid, userData: userData, isDriver: true)
            }
            self.dismiss(animated: true)
        }
    }
    
    // MARK: Private
    
    @objc private func handleScreenTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}
