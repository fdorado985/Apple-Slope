//
//  ViewController.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase
import RevealingSplashView

enum AnnotationType {
    case pickup
    case destination
    case driver
}

enum ButtonAction {
    case requestRide
    case getDirectionsToPassenger
    case getDirectionsToDestination
    case startTrip
    case endTrip
}

class HomeVC: UIViewController, Alertable {

    // MARK: IBOutlet
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var txtDestination: UITextField!
    @IBOutlet weak var imgDestination: CircleView!
    @IBOutlet weak var btnCenterMap: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAction: RoundedShadowButton!
    
    // MARK: Properties
    
    var delegate: CenterVCDelegate?
    private var revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "launchScreenIcon") ?? UIImage(), iconInitialSize: CGSize(width: 100, height: 100), backgroundColor: UIColor.white)
    private var manager: CLLocationManager?
    private let regionRadius: CLLocationDistance = 1000
    private var tableView = UITableView()
    private var matchingItems = [MKMapItem]()
    private var selectedItemPlacemark: MKPlacemark? = nil
    private var route: MKRoute!
    private var currentUserId: String {
        return Auth.auth().currentUser?.uid ?? ""
    }
    private var actionForButton: ButtonAction = .requestRide
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Location
        manager = CLLocationManager()
        manager?.delegate = self
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        checkLocationAuthStatus()
        centerMapOnUserLocation()
        
        DataService.instance.REF_DRIVERS.observe(.value) { (snapshot) in
            self.loadDriverAnnotationFromFB()
            DataService.instance.passengerIsOnTrip(passengerKey: self.currentUserId, handler: { (isOnTrip, driverKey, tripKey) in
                if isOnTrip {
                    self.zoomToFitAnnotations(from: self.mapView, forActiveTripWithDriver: true, withKey: driverKey)
                }
            })
        }
        
        btnCancel.alpha = 0.0
        
        // Splash
        self.view.addSubview(revealingSplashView)
        revealingSplashView.animationType = .heartBeat
        revealingSplashView.startAnimation()
        
        // Observe the currentTrips
        UpdateService.instance.observeTrips { (trip) in
            if let trip = trip {
                let pickupCoordinate = trip[USER_PICKUP_COORDINATE] as! NSArray
                let tripKey = trip[USER_PASSENGER_KEY] as! String
                let acceptanceStatus = trip[TRIP_IS_ACCEPTED] as! Bool
                
                if !acceptanceStatus {
                    DataService.instance.driverIsAvailable(key: self.currentUserId, handler: { (isAvailable) in
                        if isAvailable {
                            let storyboard = UIStoryboard(name: MAIN_STORYBOARD, bundle: Bundle.main)
                            if let pickupVC = storyboard.instantiateViewController(withIdentifier: VC_PICKUP) as? PickupVC {
                                pickupVC.initData(coordinate: CLLocationCoordinate2D(latitude: pickupCoordinate[0] as! CLLocationDegrees, longitude: pickupCoordinate[1] as! CLLocationDegrees), passengerKey: tripKey)
                                self.present(pickupVC, animated: true)
                            }
                        }
                    })
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DataService.instance.userIsDriver(userKey: currentUserId) { (status) in
            if status {
                self.buttonsForDriver(areHidden: true)
            }
        }
        
        DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
            if isOnTrip {
                DataService.instance.REF_TRIPS.observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let objects = snapshot.children.allObjects as? [DataSnapshot] else { return }
                    for object in objects {
                        if object.childSnapshot(forPath: DRIVER_KEY).value as? String == self.currentUserId {
                            let pickupCoordinateArray = object.childSnapshot(forPath: USER_PICKUP_COORDINATE).value as! NSArray
                            let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees)
                            let pickupPlacemark = MKPlacemark(coordinate: pickupCoordinate)
                            
                            self.dropPin(for: pickupPlacemark)
                            self.searchMapKitForResultsWithPolyline(origin: nil, destination: MKMapItem(placemark: pickupPlacemark))
                            
                            self.setCustomRegion(for: .pickup, with: pickupCoordinate)
                            
                            self.actionForButton = .getDirectionsToPassenger
                            self.btnAction.setTitle(MSG_GET_DIRECTION, for: .normal)
                            
                            self.buttonsForDriver(areHidden: false)
                        }
                    }
                })
            }
        }
        
        DataService.instance.REF_TRIPS.observe(.childRemoved) { (snapshot) in
            guard let removedTripDict = snapshot.value as? [String : Any] else { return }
            if let driverKey = removedTripDict[DRIVER_KEY] as? String {
                DataService.instance.REF_DRIVERS.child(driverKey).updateChildValues([DRIVER_IS_ON_TRIP : false])
            }
            
            DataService.instance.userIsDriver(userKey: self.currentUserId, handler: { (isDriver) in
                if isDriver {
                    self.removeOverlaysAndAnnotations(forDrivers: false, forPassengers: true)
                } else {
                    self.btnCancel.fadeTo(alpha: 0.0, duration: 0.2)
                    self.btnAction.animateButton(shouldLoad: false, message: MSG_REQUEST_RIDE)
                    self.txtDestination.isUserInteractionEnabled = true
                    self.txtDestination.text = ""
                    
                    self.removeOverlaysAndAnnotations(forDrivers: false, forPassengers: true)
                    self.centerMapOnUserLocation()
                }
            })
        }
        
        connectUserAndDriverForTrip()
    }

    // MARK: IBAction
    
    @IBAction func btnActionTapped(_ sender: RoundedShadowButton) {
        buttonSelector(for: actionForButton)
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
            if isOnTrip {
                UpdateService.instance.cancelTrip(withPassenger: tripKey!, forDriver: driverKey!)
            }
        }
        
        DataService.instance.passengerIsOnTrip(passengerKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
            if isOnTrip {
                UpdateService.instance.cancelTrip(withPassenger: self.currentUserId, forDriver: driverKey)
            } else {
                self.removeOverlaysAndAnnotations(forDrivers: false, forPassengers: true)
                self.centerMapOnUserLocation()
            }
        }
        
        btnAction.isUserInteractionEnabled = true
    }
    
    @IBAction func btnMenuTapped(_ sender: UIButton) {
        delegate?.toggleLeftPanel()
        
    }
    
    @IBAction func btnCenterMapTapped(_ sender: UIButton) {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            if let objects = snapshot.children.allObjects as? [DataSnapshot] {
                for object in objects {
                    if object.key == self.currentUserId {
                        if object.hasChild(TRIP_COORDINATE) {
                            self.zoomToFitAnnotations(from: self.mapView, forActiveTripWithDriver: false, withKey: nil)
                            self.btnCenterMap.fadeTo(alpha: 0.0, duration: 0.2)
                        } else {
                            self.centerMapOnUserLocation()
                            self.btnCenterMap.fadeTo(alpha: 0.0, duration: 0.2)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Private
    
    private func checkLocationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            manager?.startUpdatingLocation()
        } else {
            manager?.requestAlwaysAuthorization()
        }
    }
    
    private func centerMapOnUserLocation() {
        let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func loadDriverAnnotationFromFB() {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let snapshotObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for object in snapshotObjects {
                if object.hasChild(USER_IS_DRIVER) {
                    if object.hasChild(COORDINATE) {
                        if object.childSnapshot(forPath: ACCOUNT_PICKUP_MODE_ENABLED).value as? Bool == true {
                            if let driverDict = object.value as? [String : Any] {
                                let coordinates = driverDict[COORDINATE] as! NSArray
                                let driverCoordinate = CLLocationCoordinate2D(latitude: coordinates[0] as! CLLocationDegrees, longitude: coordinates[1] as! CLLocationDegrees)
                                let annotation = DriverAnnotation(coordinate: driverCoordinate, withKey: object.key)
                                var driverIsVisible: Bool {
                                    return self.mapView.annotations.contains(where: { (annotation) -> Bool in
                                        if let driverAnnotation = annotation as? DriverAnnotation {
                                            if driverAnnotation.key == object.key {
                                                driverAnnotation.update(annotationPosition: driverAnnotation, withCoordinate: driverCoordinate)
                                                return true
                                            }
                                        }
                                        return false
                                    })
                                }
                                
                                if !driverIsVisible {
                                    self.mapView.addAnnotation(annotation)
                                }
                            }
                        } else {
                            for annotation in self.mapView.annotations {
                                if annotation.isKind(of: DriverAnnotation.self) {
                                    guard let annotation = annotation as? DriverAnnotation,
                                        annotation.key == object.key else { return }
                                    self.mapView.removeAnnotation(annotation)
                                }
                            }
                        }
                    }
                }
            }
        }
        revealingSplashView.heartAttack = true
    }
    
    private func connectUserAndDriverForTrip() {
        DataService.instance.passengerIsOnTrip(passengerKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
            if isOnTrip == true {
                self.removeOverlaysAndAnnotations(forDrivers: false, forPassengers: true)
                DataService.instance.REF_TRIPS.child(tripKey!).observeSingleEvent(of: .value, with: { (tripSnapshot) in
                    let tripDict = tripSnapshot.value as? Dictionary<String, AnyObject>
                    let driverId = tripDict?[DRIVER_KEY] as! String
                    
                    let pickupCoordinateArray = tripDict?[USER_PICKUP_COORDINATE] as! NSArray
                    let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees)
                    let pickupPlacemark = MKPlacemark(coordinate: pickupCoordinate)
                    let pickupMapItem = MKMapItem(placemark: pickupPlacemark)
                    
                    DataService.instance.REF_DRIVERS.child(driverId).child(COORDINATE).observeSingleEvent(of: .value, with: { (coordinateSnapshot) in
                        let coordinateSnapshot = coordinateSnapshot.value as! NSArray
                        let driverCoordinate = CLLocationCoordinate2D(latitude: coordinateSnapshot[0] as! CLLocationDegrees, longitude: coordinateSnapshot[1] as! CLLocationDegrees)
                        let driverPlacemark = MKPlacemark(coordinate: driverCoordinate)
                        let driverMapItem = MKMapItem(placemark: driverPlacemark)
                        
                        let passengerAnnotation = PassengerAnnotation(coordinate: pickupCoordinate, key: self.currentUserId)
                        self.mapView.addAnnotation(passengerAnnotation)
                        
                        self.searchMapKitForResultsWithPolyline(origin: driverMapItem, destination: pickupMapItem)
                        self.btnAction.animateButton(shouldLoad: false, message: MSG_DRIVER_COMING)
                        self.btnAction.isUserInteractionEnabled = false
                    })
                    
                    DataService.instance.REF_TRIPS.child(tripKey!).observeSingleEvent(of: .value, with: { (tripSnapshot) in
                        if tripDict?[TRIP_IN_PROGRESS] as? Bool == true {
                            self.removeOverlaysAndAnnotations(forDrivers: true, forPassengers: true)
                            let destinationcoordinateArray = tripDict?[USER_DESTINATION_COORDINATE] as! NSArray
                            let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationcoordinateArray[0] as! CLLocationDegrees, longitude: destinationcoordinateArray[1] as! CLLocationDegrees)
                            let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                            
                            self.dropPin(for: destinationPlacemark)
                            self.searchMapKitForResultsWithPolyline(origin: pickupMapItem, destination: MKMapItem(placemark: destinationPlacemark))
                            
                            self.btnAction.setTitle(MSG_ON_TRIP, for: .normal)
                        }
                    })
                })
            }
        }
    }
    
    private func buttonSelector(for action: ButtonAction) {
        switch action {
        case .requestRide:
            if txtDestination.text != "" {
                UpdateService.instance.updateTripsWithCoordinateUponRequest()
                btnAction.animateButton(shouldLoad: true, message: nil)
                btnCancel.fadeTo(alpha: 1.0, duration: 0.2)
                
                self.view.endEditing(true)
                txtDestination.isUserInteractionEnabled = false
            }
        case .getDirectionsToPassenger:
            DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
                if isOnTrip {
                    DataService.instance.REF_TRIPS.child(tripKey!).observe(.value, with: { (tripSnapshot) in
                        let tripDict = tripSnapshot.value as? [String : Any]
                        
                        let pickupCoordinateArray = tripDict?[USER_PICKUP_COORDINATE] as! NSArray
                        let pickupCoordinate = CLLocationCoordinate2D(latitude: pickupCoordinateArray[0] as! CLLocationDegrees, longitude: pickupCoordinateArray[1] as! CLLocationDegrees)
                        
                        let pickupMapItem = MKMapItem(placemark: MKPlacemark(coordinate: pickupCoordinate))
                        pickupMapItem.name = MSG_PASSENGER_PICKUP
                        pickupMapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                    })
                }
            }
        case .getDirectionsToDestination:
            DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
                if isOnTrip {
                    DataService.instance.REF_TRIPS.child(tripKey!).child(USER_DESTINATION_COORDINATE).observe(.value, with: { (snapshot) in
                        let destinationCoordinateArray = snapshot.value as! NSArray
                        let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees)
                        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
                        
                        destinationMapItem.name = MSG_PASSENGER_DESTINATION
                        destinationMapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                    })
                }
            }
        case .startTrip:
            DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
                if isOnTrip {
                    self.removeOverlaysAndAnnotations(forDrivers: false, forPassengers: false)
                    DataService.instance.REF_TRIPS.child(tripKey!).updateChildValues([TRIP_IN_PROGRESS : true])
                    DataService.instance.REF_TRIPS.child(tripKey!).child(USER_DESTINATION_COORDINATE).observeSingleEvent(of: .value, with: { (coordinateSnapshot) in
                        let destinationCoordinateArray = coordinateSnapshot.value as! NSArray
                        let destinationCoordinate = CLLocationCoordinate2D(latitude: destinationCoordinateArray[0] as! CLLocationDegrees, longitude: destinationCoordinateArray[1] as! CLLocationDegrees)
                        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate)
                        
                        self.dropPin(for: destinationPlacemark)
                        self.searchMapKitForResultsWithPolyline(origin: nil, destination: MKMapItem(placemark: destinationPlacemark))
                        self.setCustomRegion(for: .destination, with: destinationCoordinate)
                        
                        self.actionForButton = .getDirectionsToDestination
                        self.btnAction.setTitle(MSG_GET_DIRECTION, for: .normal)
                    })
                }
            }
        case .endTrip:
            DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
                if isOnTrip {
                    UpdateService.instance.cancelTrip(withPassenger: tripKey!, forDriver: driverKey!)
                    self.buttonsForDriver(areHidden: true)
                }
            }
        }
    }
    
    private func buttonsForDriver(areHidden: Bool) {
        if areHidden {
            btnAction.fadeTo(alpha: 0.0, duration: 0.2)
            btnCancel.fadeTo(alpha: 0.0, duration: 0.2)
            btnCenterMap.fadeTo(alpha: 0.0, duration: 0.2)
            btnAction.isHidden = true
            btnCancel.isHidden = true
            btnCenterMap.isHidden = true
        } else {
            btnAction.fadeTo(alpha: 1.0, duration: 0.2)
            btnCancel.fadeTo(alpha: 1.0, duration: 0.2)
            btnCenterMap.fadeTo(alpha: 1.0, duration: 0.2)
            btnAction.isHidden = false
            btnCancel.isHidden = false
            btnCenterMap.isHidden = false
        }
    }
}

// MARK: - MKMapView Delegate

extension HomeVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        UpdateService.instance.updateUserLocation(withCoordinate: userLocation.coordinate)
        UpdateService.instance.updateDriverLocation(withCoordinate: userLocation.coordinate)
        
        DataService.instance.userIsDriver(userKey: currentUserId) { (isDriver) in
            if isDriver {
                DataService.instance.driverIsOnTrip(driverKey: self.currentUserId, handler: { (isOnTrip, driverKey, tripKey) in
                    if isOnTrip {
                        self.zoomToFitAnnotations(from: self.mapView, forActiveTripWithDriver: true, withKey: driverKey)
                    } else {
                        self.centerMapOnUserLocation()
                    }
                })
            } else {
                DataService.instance.passengerIsOnTrip(passengerKey: self.currentUserId, handler: { (isOnTrip, driverKey, tripKey) in
                    if isOnTrip {
                        self.zoomToFitAnnotations(from: self.mapView, forActiveTripWithDriver: true, withKey: driverKey)
                    } else {
                        self.centerMapOnUserLocation()
                    }
                })
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? DriverAnnotation {
            let identifier = "driver"
            let view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = UIImage(named: ANNO_DRIVER)
            return view
        } else if let annotation = annotation as? PassengerAnnotation {
            let identifier = "passenger"
            let view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.image = UIImage(named: ANNO_PICKUP)
            return view
        } else if let annotation = annotation as? MKPointAnnotation {
            let identifier = REGION_DESTINATION
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            } else {
                annotationView?.annotation = annotation
            }
            
            annotationView?.image = UIImage(named: ANNO_DESTINATION)
            return annotationView
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        btnCenterMap.fadeTo(alpha: 1.0, duration: 0.2)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let lineRenderer = MKPolylineRenderer(overlay: self.route.polyline)
        lineRenderer.strokeColor = UIColor(red: 216.0/255.0, green: 71.0/255.0, blue: 30.0/255.0, alpha: 0.75)
        lineRenderer.lineWidth = 3
        
        shouldPresentLoadingView(status: false)
        
        return lineRenderer
    }
    
    private func performSearch() {
        matchingItems.removeAll()
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = txtDestination.text
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            if let error = error {
                self.showAlert(message: error.localizedDescription)
                self.shouldPresentLoadingView(status: false)
                return
            }
            
            guard let response = response, response.mapItems.count > 0 else {
                self.showAlert(message: ERROR_MSG_NO_MATCHES_FOUND)
                self.shouldPresentLoadingView(status: false)
                return
            }
            
            for mapItem in response.mapItems {
                self.matchingItems.append(mapItem)
                self.tableView.reloadData()
                self.shouldPresentLoadingView(status: false)
            }
        }
    }
    
    private func dropPin(for placemark: MKPlacemark) {
        selectedItemPlacemark = placemark
        
        // Remove current pins
        for annotation in mapView.annotations {
            if annotation.isKind(of: MKPointAnnotation.self) {
                mapView.removeAnnotation(annotation)
            }
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        mapView.addAnnotation(annotation)
    }
    
    private func searchMapKitForResultsWithPolyline(origin originMapItem: MKMapItem?, destination destinationMapItem: MKMapItem) {
        let request = MKDirections.Request()
        
        request.source = (originMapItem != nil) ? originMapItem : MKMapItem.forCurrentLocation()
        request.destination = destinationMapItem
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            if let error = error {
                self.showAlert(message: error.localizedDescription)
                self.shouldPresentLoadingView(status: false)
                return
            }
            
            guard let response = response, response.routes.count > 0 else {
                self.shouldPresentLoadingView(status: false)
                return
            }
            
            self.route = response.routes[0]
            self.mapView.addOverlay(self.route.polyline)
            
            self.zoomToFitAnnotations(from: self.mapView, forActiveTripWithDriver: false, withKey: nil)
            let delegate = AppDelegate.getAppDelegate()
            delegate.window?.rootViewController?.shouldPresentLoadingView(status: false)
        }
    }
    
    private func zoomToFitAnnotations(from mapView: MKMapView, forActiveTripWithDriver: Bool, withKey key: String?) {
        guard mapView.annotations.count > 0 else { return }
        
        var topLeftCoordinate = CLLocationCoordinate2D(latitude: -90, longitude: 180)
        var bottomRightCoordinate = CLLocationCoordinate2D(latitude: 90, longitude: -180)
        
        if forActiveTripWithDriver {
            for annotation in mapView.annotations {
                if let annotation = annotation as? DriverAnnotation {
                    if annotation.key == key {
                        topLeftCoordinate.longitude = fmin(topLeftCoordinate.longitude, annotation.coordinate.longitude)
                        topLeftCoordinate.latitude = fmax(topLeftCoordinate.latitude, annotation.coordinate.latitude)
                        bottomRightCoordinate.longitude = fmax(bottomRightCoordinate.longitude, annotation.coordinate.longitude)
                        bottomRightCoordinate.latitude = fmin(bottomRightCoordinate.latitude, annotation.coordinate.latitude)
                    }
                } else {
                    topLeftCoordinate.longitude = fmin(topLeftCoordinate.longitude, annotation.coordinate.longitude)
                    topLeftCoordinate.latitude = fmax(topLeftCoordinate.latitude, annotation.coordinate.latitude)
                    bottomRightCoordinate.longitude = fmax(bottomRightCoordinate.longitude, annotation.coordinate.longitude)
                    bottomRightCoordinate.latitude = fmin(bottomRightCoordinate.latitude, annotation.coordinate.latitude)
                }
            }
        }
        
        for annotation in mapView.annotations where !annotation.isKind(of: DriverAnnotation.self) {
            topLeftCoordinate.longitude = fmin(topLeftCoordinate.longitude, annotation.coordinate.longitude)
            topLeftCoordinate.latitude = fmax(topLeftCoordinate.latitude, annotation.coordinate.latitude)
            bottomRightCoordinate.longitude = fmax(bottomRightCoordinate.longitude, annotation.coordinate.longitude)
            bottomRightCoordinate.latitude = fmin(bottomRightCoordinate.latitude, annotation.coordinate.latitude)
        }
        
        var region = MKCoordinateRegion(
            center: CLLocationCoordinate2DMake(
                topLeftCoordinate.latitude - (topLeftCoordinate.latitude - bottomRightCoordinate.latitude) * 0.5,
                topLeftCoordinate.longitude + (bottomRightCoordinate.longitude - topLeftCoordinate.longitude) * 0.5
            ),
            span: MKCoordinateSpan(
                latitudeDelta: fabs(topLeftCoordinate.latitude - bottomRightCoordinate.latitude) * 2.0,
                longitudeDelta: fabs(bottomRightCoordinate.longitude - topLeftCoordinate.longitude) * 2.0
            )
        )
        region = mapView.regionThatFits(region)
        mapView.setRegion(region, animated: true)
    }
    
    private func removeOverlaysAndAnnotations(forDrivers driversStatus: Bool?, forPassengers passengersStatus: Bool?) {
        // Remove Annotations
        for annotation in mapView.annotations {
            if let annotation = annotation as? MKPointAnnotation {
                mapView.removeAnnotation(annotation)
            }
            
            if let passengers = passengersStatus, passengers == true {
                if let annotation = annotation as? PassengerAnnotation {
                    mapView.removeAnnotation(annotation)
                }
            }
            
            if let drivers = driversStatus, drivers == true {
                if let annotation = annotation as? DriverAnnotation {
                    mapView.removeAnnotation(annotation)
                }
            }
        }
        
        // Remove Overlays
        for overlay in mapView.overlays {
            if overlay is MKPolyline {
                mapView.removeOverlay(overlay)
            }
        }
    }
    
    func setCustomRegion(for type: AnnotationType, with coordinate: CLLocationCoordinate2D) {
        if type == .pickup {
            let pickupRegion = CLCircularRegion(center: coordinate, radius: 100, identifier: REGION_PICKUP)
            manager?.startMonitoring(for: pickupRegion)
        } else if type == .destination {
            let destinationRegion = CLCircularRegion(center: coordinate, radius: 100, identifier: REGION_DESTINATION)
            manager?.startMonitoring(for: destinationRegion)
        }
    }
}

// MARK: - CLLocationManager Delegate

extension HomeVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, passengerKey) in
            if isOnTrip {
                if region.identifier == REGION_PICKUP {
                    self.actionForButton = .startTrip
                    self.btnAction.setTitle(MSG_START_TRIP, for: .normal)
                } else if region.identifier == REGION_DESTINATION {
                    self.btnCancel.fadeTo(alpha: 0.0, duration: 0.2)
                    self.btnCancel.isHidden = true
                    self.actionForButton = .endTrip
                    self.btnAction.setTitle(MSG_END_TRIP, for: .normal)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        DataService.instance.driverIsOnTrip(driverKey: currentUserId) { (isOnTrip, driverKey, tripKey) in
            if isOnTrip {
                if region.identifier == REGION_PICKUP {
                    self.actionForButton = .getDirectionsToPassenger
                    self.btnAction.setTitle(MSG_GET_DIRECTION, for: .normal)
                } else if region.identifier == REGION_DESTINATION {
                    self.actionForButton = .getDirectionsToDestination
                    self.btnAction.setTitle(MSG_GET_DIRECTION, for: .normal)
                }
            }
        }
    }
}

// MARK: - UITextField Delegate

extension HomeVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtDestination {
            tableView.frame = CGRect(x: 20, y: view.frame.height, width: (view.frame.width - 40), height: view.frame.height - 170)
            tableView.layer.cornerRadius = 5.0
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: CELL_LOCATION)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tag = 18 // This will be used to delete it from parent
            tableView.rowHeight = 60
            view.addSubview(tableView)
            animateTableView(shouldShow: true)
            
            UIView.animate(withDuration: 0.2) {
                self.imgDestination.backgroundColor = UIColor.red
                self.imgDestination.borderColor = UIColor(red: 199.0 / 255.0, green: 0.0 / 255.0, blue: 0.0, alpha: 1.0)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtDestination {
            performSearch()
            shouldPresentLoadingView(status: true)
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtDestination {
            if txtDestination.text == "" {
                UIView.animate(withDuration: 0.2) {
                    self.imgDestination.backgroundColor = UIColor.lightGray
                    self.imgDestination.borderColor = UIColor.darkGray
                }
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        matchingItems = []
        tableView.reloadData()
        
        DataService.instance.REF_USERS.child(currentUserId).child(TRIP_COORDINATE).removeValue()
        mapView.removeOverlays(mapView.overlays)
        for annotation in mapView.annotations {
            if let annotation = annotation as? MKPointAnnotation {
                mapView.removeAnnotation(annotation)
            } else if annotation.isKind(of: PassengerAnnotation.self) {
                mapView.removeAnnotation(annotation)
            }
        }
        
        centerMapOnUserLocation()
        return true
    }
    
    func animateTableView(shouldShow: Bool) {
        if shouldShow {
            UIView.animate(withDuration: 0.2) {
                self.tableView.frame = CGRect(x: 20, y: 170, width: (self.view.frame.width - 40), height: self.view.frame.height - 170)
            }
        } else {
            UIView.animate(withDuration: 0.2, animations: {
                self.tableView.frame = CGRect(x: 20, y: self.view.frame.height, width: (self.view.frame.width - 40), height: self.view.frame.height - 170)
            }) { (finished) in
                for subview in self.view.subviews {
                    if subview.tag == 18 {
                        subview.removeFromSuperview()
                    }
                }
            }
        }
    }
}

// MARK: - UITableView Delegate|DataSource

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: CELL_LOCATION)
        let mapItem = matchingItems[indexPath.row]
        cell.textLabel?.text = mapItem.name
        cell.detailTextLabel?.text = mapItem.placemark.title // This got the address
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let passengerCoordinate = manager?.location?.coordinate else { return }
        shouldPresentLoadingView(status: true)
        let passengerAnnotation = PassengerAnnotation(coordinate: passengerCoordinate, key: currentUserId)
        mapView.addAnnotation(passengerAnnotation)
        
        txtDestination.text = tableView.cellForRow(at: indexPath)?.textLabel?.text
        let selectedMapItem = matchingItems[indexPath.row]
        
        DataService.instance.REF_USERS.child(currentUserId).updateChildValues([TRIP_COORDINATE : [selectedMapItem.placemark.coordinate.latitude, selectedMapItem.placemark.coordinate.longitude]])
        
        dropPin(for: selectedMapItem.placemark)
        searchMapKitForResultsWithPolyline(origin: nil, destination: selectedMapItem)
        
        animateTableView(shouldShow: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if txtDestination.text == "" {
            animateTableView(shouldShow: false)
        }
    }
}
