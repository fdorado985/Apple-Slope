//
//  LeftSidePanelVC.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class LeftSidePanelVC: UIViewController {

    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAccountType: UILabel!
    @IBOutlet weak var imgUser: RoundImageView!
    @IBOutlet weak var btnAuth: UIButton!
    @IBOutlet weak var lblPickupMode: UILabel!
    @IBOutlet weak var switchPickupMode: UISwitch!
    
    let appDelegate = AppDelegate.getAppDelegate()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switchPickupMode.isOn = false
        switchPickupMode.isHidden = true
        lblPickupMode.isHidden = false
        
        observePassengersAndDrivers()
        
        if let currentUser = Auth.auth().currentUser {
            lblEmail.text = currentUser.email
            lblAccountType.text = ""
            imgUser.isHidden = false
            btnAuth.setTitle(MSG_SIGN_OUT, for: .normal)
        } else {
            lblEmail.text = ""
            lblAccountType.text = ""
            imgUser.isHidden = true
            btnAuth.setTitle(MSG_SIGN_UP_SIGN_IN, for: .normal)
        }
        
    }
    
    // MARK: IBActions

    @IBAction func btnSignUpLoginTapped(_ sender: UIButton) {
        if Auth.auth().currentUser != nil {
            do {
                try Auth.auth().signOut()
                lblEmail.text = ""
                lblAccountType.text = ""
                imgUser.isHidden = true
                lblPickupMode.text = ""
                switchPickupMode.isHidden = true
                btnAuth.setTitle(MSG_SIGN_UP_SIGN_IN, for: .normal)
            } catch let error {
                debugPrint(error.localizedDescription)
            }
        } else {
            let storyboard = UIStoryboard(name: MAIN_STORYBOARD, bundle: Bundle.main)
            guard let loginVC = storyboard.instantiateViewController(withIdentifier: VC_LOGIN) as? LoginVC else { return }
            present(loginVC, animated: true)
        }
    }
    
    @IBAction func switchPickUpModeValueChanged(_ sender: UISwitch) {
        guard let currentUser = Auth.auth().currentUser else { return }
        if sender.isOn {
            lblPickupMode.text = MSG_PICKUP_MODE_ENABLED
            appDelegate.MenuContainerVC.toggleLeftPanel()
            DataService.instance.REF_DRIVERS.child(currentUser.uid).updateChildValues([ACCOUNT_PICKUP_MODE_ENABLED : true])
        } else {
            lblPickupMode.text = MSG_PICKUP_MODE_DISABLED
            appDelegate.MenuContainerVC.toggleLeftPanel()
            DataService.instance.REF_DRIVERS.child(currentUser.uid).updateChildValues([ACCOUNT_PICKUP_MODE_ENABLED : false])
        }
    }
    // MARK: Private
    
    private func observePassengersAndDrivers() {
        guard let currentUser = Auth.auth().currentUser else { return }
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let allObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for object in allObjects {
                if object.key == currentUser.uid {
                    self.lblAccountType.text = ACCOUNT_TYPE_PASSENGER
                    self.lblPickupMode.isHidden = false
                }
            }
        }
        
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let allObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for object in allObjects {
                if object.key == currentUser.uid {
                    self.lblAccountType.text = ACCOUNT_TYPE_DRIVER
                    
                    let switchStatus = object.childSnapshot(forPath: ACCOUNT_PICKUP_MODE_ENABLED).value as? Bool ?? false
                    self.switchPickupMode.isHidden = false
                    self.lblPickupMode.isHidden = false
                    self.switchPickupMode.isOn = switchStatus
                    self.switchPickupMode.isEnabled = true
                }
            }
        }
    }
}
