//
//  UIView+Ext.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIView {
    
    func fadeTo(alpha: CGFloat, duration: Double) {
        UIView.animate(withDuration: duration) {
            self.alpha = alpha
        }
    }
    
    func bindToKeyboard() {
        let keyboardChangeFrame = UIWindow.keyboardWillChangeFrameNotification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: keyboardChangeFrame, object: nil)
    }
    
    @objc private func keyboardWillChange(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        guard let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt else { return }
        guard let curveFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
        guard let targetFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let deltaY = targetFrame.origin.y - curveFrame.origin.y
        
        UIView.animateKeyframes(withDuration: duration, delay: 0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
    }
}
