//
//  RoundImageView.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RoundImageView: UIImageView {
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: Private
    
    private func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }

}
