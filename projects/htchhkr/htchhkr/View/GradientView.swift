//
//  GradientView.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    // MARK: Properties
    
    private let gradient = CAGradientLayer()
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupGradientView()
    }
    
    // MARK: Private
    
    private func setupGradientView() {
        gradient.frame = self.bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.init(white: 1.0, alpha: 0.0).cgColor]
        gradient.startPoint = CGPoint.zero
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.locations = [0.8, 1.0]
        self.layer.addSublayer(gradient)
    }

}
