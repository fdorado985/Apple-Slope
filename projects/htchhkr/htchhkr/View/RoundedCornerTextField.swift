//
//  RoundedCornerTextField.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RoundedCornerTextField: UITextField {
    
    // MARK: Properties
    
    private var textRectOffset: CGFloat = 20

    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: Private
    
    private func setupView() {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0 + textRectOffset, y: 0 + textRectOffset / 2, width: self.frame.width - textRectOffset, height: self.frame.height + textRectOffset)
        // return self.bounds.inset(by: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0 + textRectOffset, y: 0 + textRectOffset / 2, width: self.frame.width - textRectOffset, height: self.frame.height + textRectOffset)
        // return self.bounds.inset(by: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        // return CGRect(x: 0 + textRectOffset, y: 0 + textRectOffset / 2, width: self.frame.width - textRectOffset, height: self.frame.height + textRectOffset)
        return self.bounds.inset(by: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20))
    }

}
