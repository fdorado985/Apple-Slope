//
//  DriverAnnotation.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import MapKit

class DriverAnnotation: NSObject, MKAnnotation {
    
    // MARK: Properties
    
    dynamic private(set) var coordinate: CLLocationCoordinate2D // Part of the MKAnnotation Protocol
    private(set) var key: String
    
    // MARK: Initialization
    
    init(coordinate: CLLocationCoordinate2D, withKey key: String) {
        self.coordinate = coordinate
        self.key = key
        super.init()
    }
    
    // MARK: Public
    
    func update(annotationPosition annotation: DriverAnnotation, withCoordinate coordinate: CLLocationCoordinate2D) {
        var location = self.coordinate
        location.latitude = coordinate.latitude
        location.longitude = coordinate.longitude
        UIView.animate(withDuration: 0.2) {
            self.coordinate = location
        }
    }
    
}
