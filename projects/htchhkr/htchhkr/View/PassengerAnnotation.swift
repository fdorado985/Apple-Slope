//
//  PassengerAnnotation.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/24/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import MapKit

class PassengerAnnotation: NSObject, MKAnnotation {
    
    // MARK: MKAnnotation Properties
    
    dynamic var coordinate: CLLocationCoordinate2D
    
    // MARK: Properties
    var key: String
    
    // MARK: Initialization
    
    init(coordinate: CLLocationCoordinate2D, key: String) {
        self.coordinate = coordinate
        self.key = key
        super.init()
    }
}
