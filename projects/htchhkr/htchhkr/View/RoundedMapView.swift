//
//  RoundedMapView.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit

class RoundedMapView: MKMapView {
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: Private
    
    private func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 10.0
    }

}
