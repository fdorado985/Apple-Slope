//
//  DataService.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/16/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()

class DataService {
    
    // MARK: Singleton
    
    static let instance = DataService()
    
    // MARK: Properties
    
    private(set) var REF_BASE = DB_BASE
    private(set) var REF_USERS = DB_BASE.child("USERS")
    private(set) var REF_DRIVERS = DB_BASE.child("DRIVERS")
    private(set) var REF_TRIPS = DB_BASE.child("TRIPS")
    
    // MARK: Public
    
    func createUser(uid: String, userData: [String : Any], isDriver: Bool = false) {
        if isDriver {
            REF_DRIVERS.child(uid).updateChildValues(userData)
        } else {
            REF_USERS.child(uid).updateChildValues(userData)
        }
    }
    
    func driverIsAvailable(key: String, handler: @escaping (_ status: Bool) -> Void) {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            if let objects = snapshot.children.allObjects as? [DataSnapshot] {
                for object in objects {
                    if object.key == key {
                        if object.childSnapshot(forPath: ACCOUNT_PICKUP_MODE_ENABLED).value as? Bool == true {
                            if object.childSnapshot(forPath: DRIVER_IS_ON_TRIP).value as? Bool == true {
                                handler(false)
                            } else {
                                handler(true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func driverIsOnTrip(driverKey: String, handler: @escaping (_ status: Bool, _ driverKey: String?, _ tripKey: String?) -> Void) {
        DataService.instance.REF_DRIVERS.child(driverKey).child(DRIVER_IS_ON_TRIP).observe(.value) { (snapshot) in
            guard let driverTripStatusSnapshot = snapshot.value as? Bool, driverTripStatusSnapshot == true else { return }
            if driverTripStatusSnapshot {
                DataService.instance.REF_TRIPS.observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let objects = snapshot.children.allObjects as? [DataSnapshot] else { return }
                    for object in objects {
                        if object.childSnapshot(forPath: DRIVER_KEY).value as? String == driverKey {
                            handler(true, driverKey, object.key)
                        } else {
                            return
                        }
                    }
                })
            } else {
                handler(false, nil, nil)
            }
        }
    }
    
    func passengerIsOnTrip(passengerKey: String, handler: @escaping (_ status: Bool, _ driverKey: String?, _ tripKey: String?) -> Void) {
        DataService.instance.REF_TRIPS.observe(.value) { (snapshot) in
            guard let objects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for object in objects {
                if object.key == passengerKey {
                    if object.childSnapshot(forPath: TRIP_IS_ACCEPTED).value as? Bool == true {
                        let driverKey = object.childSnapshot(forPath: DRIVER_KEY).value as? String
                        handler(true, driverKey, object.key)
                    } else {
                        handler(false, nil, nil)
                    }
                }
            }
        }
    }
    
    func userIsDriver(userKey: String, handler: @escaping (_ status: Bool) -> Void) {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let objects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            for object in objects {
                if object.key == userKey {
                    handler(true)
                } else {
                    handler(false)
                }
            }
        }
    }
}
