//
//  UpdateService.swift
//  htchhkr
//
//  Created by Juan Francisco Dorado Torres on 9/22/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class UpdateService {
    
    // MARK: Singleton
    
    static var instance = UpdateService()
    
    // MARK: Public
    
    func updateUserLocation(withCoordinate coordinate: CLLocationCoordinate2D) {
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let snapshotObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            guard let currentUser = Auth.auth().currentUser else { return }
            
            for object in snapshotObjects {
                if object.key == currentUser.uid {
                    DataService.instance
                        .REF_USERS
                        .child(object.key)
                        .updateChildValues([COORDINATE : [coordinate.latitude, coordinate.longitude]])
                }
            }
        }
    }
    
    func updateDriverLocation(withCoordinate coordinate: CLLocationCoordinate2D) {
        DataService.instance.REF_DRIVERS.observeSingleEvent(of: .value) { (snapshot) in
            guard let snapshotObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            guard let currentUser = Auth.auth().currentUser else { return }
            
            for object in snapshotObjects {
                if object.key == currentUser.uid {
                    if object.childSnapshot(forPath: ACCOUNT_PICKUP_MODE_ENABLED).value as? Bool == true {
                        DataService.instance
                            .REF_DRIVERS
                            .child(object.key)
                            .updateChildValues([COORDINATE : [coordinate.latitude, coordinate.longitude]])
                    }
                }
            }
        }
    }
    
    func updateTripsWithCoordinateUponRequest() {
        guard let currentUser = Auth.auth().currentUser else { return }
        DataService.instance.REF_USERS.observeSingleEvent(of: .value) { (snapshot) in
            if let objects = snapshot.children.allObjects as? [DataSnapshot] {
                for object in objects {
                    if object.key == currentUser.uid {
                        if !object.hasChild(USER_IS_DRIVER) {
                            if let userDict = object.value as? [String : Any] {
                                let pickupArray = userDict[COORDINATE] as! NSArray
                                let destinationArray = userDict[TRIP_COORDINATE] as! NSArray
                                
                                let childValues = [
                                    USER_PICKUP_COORDINATE : [pickupArray[0], pickupArray[1]],
                                    USER_DESTINATION_COORDINATE : [destinationArray[0], destinationArray[1]],
                                    USER_PASSENGER_KEY : object.key,
                                    TRIP_IS_ACCEPTED : false
                                    ] as [String : Any]
                                
                                DataService.instance.REF_TRIPS.child(object.key).updateChildValues(childValues)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func observeTrips(handler: @escaping (_ coordinate: [String : Any]?) -> Void) {
        DataService.instance.REF_TRIPS.observe(.value) { (snapshot) in
            if let objects = snapshot.children.allObjects as? [DataSnapshot] {
                for object in objects {
                    if object.hasChild(USER_PASSENGER_KEY) && object.hasChild(TRIP_IS_ACCEPTED) {
                        if let tripDict = object.value as? [String : Any] {
                            handler(tripDict)
                        }
                    }
                }
            }
        }
    }
    
    func acceptTrip(withPassenger passengerKey: String, forDriver driverKey: String) {
        let tripValues = [DRIVER_KEY : driverKey, TRIP_IS_ACCEPTED : true] as [String : Any]
        let driverValues = [DRIVER_IS_ON_TRIP : true] as [String : Any]
        DataService.instance.REF_TRIPS.child(passengerKey).updateChildValues(tripValues)
        DataService.instance.REF_DRIVERS.child(driverKey).updateChildValues(driverValues)
    }
    
    func cancelTrip(withPassenger passengerKey: String, forDriver driverKey: String?) {
        DataService.instance.REF_TRIPS.child(passengerKey).removeValue()
        DataService.instance.REF_USERS.child(passengerKey).child(TRIP_COORDINATE).removeValue()
        guard let driverKey = driverKey else { return }
        DataService.instance.REF_DRIVERS.child(driverKey).updateChildValues([DRIVER_IS_ON_TRIP : false])
    }
}
