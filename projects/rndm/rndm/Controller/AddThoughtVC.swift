//
//  AddThoughtVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class AddThoughtVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var segmentCategory: UISegmentedControl!
    @IBOutlet private weak var txtUsername: UITextField!
    @IBOutlet private weak var txtThoughts: UITextView!
    @IBOutlet private weak var btnPost: UIButton!
    
    // MARK: Properties
    
    private var selectedCategory = ThoughtCategory.funny.rawValue
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnPostTapped(_ sender: UIButton) {
        guard let username = txtUsername.text, !username.isEmpty else { return }
        guard let content = txtThoughts.text, !content.isEmpty else { return }
        guard let currentUser = Auth.auth().currentUser else { return }
        
        let data: [String : Any] = [
            CATEGORY : selectedCategory,
            NUM_COMMENTS : 0,
            NUM_LIKES : 0,
            CONTENT : content,
            TIMESTAMP : FieldValue.serverTimestamp(),
            USERNAME : username,
            USER_ID : currentUser.uid
        ]
        
        Firestore.firestore().collection(THOUGHTS_REF).addDocument(data: data) { (error) in
            if let error = error {
                debugPrint("Error adding document: \(error.localizedDescription)")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func segmentCategoryValueChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            selectedCategory = ThoughtCategory.funny.rawValue
        case 1:
            selectedCategory = ThoughtCategory.serious.rawValue
        case 2:
            selectedCategory = ThoughtCategory.crazy.rawValue
        default:
            selectedCategory = ThoughtCategory.funny.rawValue
        }
    }
    
    // MARK: Private
    
    private func setupView() {
        btnPost.layer.cornerRadius = 4
        txtThoughts.layer.cornerRadius = 4
        txtThoughts.text = "My random thoughts..."
        txtThoughts.textColor = UIColor.lightGray
        
        guard let displayName = Auth.auth().currentUser?.displayName else { return }
        txtUsername.text = displayName
        txtUsername.isEnabled = false
    }
}

// MARK: - UITextView Delegate

extension AddThoughtVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.darkGray
    }
}
