//
//  UpdateCommentVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class UpdateCommentVC: UIViewController {
    
    // MARK: IBOutlet

    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var btnUpdate: UIButton!
    
    // MARK: Variables
    
    var commentData: (comment: Comment, thought: Thought)?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnUpdate.layer.cornerRadius = 10
        txtContent.layer.cornerRadius = 10
        
        guard let commentData = self.commentData else { return }
        txtContent.text = commentData.comment.content
    }
    
    // MARK: IBActions
    
    @IBAction func btnUpdateTapped(_ sender: UIButton) {
        guard let commentData = self.commentData else { return }
        Firestore.firestore().collection(THOUGHTS_REF).document(commentData.thought.documentId).collection(COMMENTS_REF).document(commentData.comment.documentId).updateData([COMMENT : txtContent.text]) { (error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                return
            }
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    

}
