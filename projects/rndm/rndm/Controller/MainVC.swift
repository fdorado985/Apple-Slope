//
//  MainVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

enum ThoughtCategory: String {
    case funny = "funny"
    case serious = "serious"
    case crazy = "crazy"
    case popular = "popular"
}

class MainVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var tableThoughts: UITableView!
    @IBOutlet private weak var segmentCategory: UISegmentedControl!
    
    // MARK: Properties
    
    private var thoughts = [Thought]()
    private var thoughtsCollectionRef: CollectionReference!
    private var thoughtsListener: ListenerRegistration!
    private var selectedCategory = ThoughtCategory.funny.rawValue
    private var handle: AuthStateDidChangeListenerHandle?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        thoughtsCollectionRef = Firestore.firestore().collection(THOUGHTS_REF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if let _ = user {
                self.setListener()
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                self.present(loginVC, animated: true)
            }
        })
        setListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if thoughtsListener != nil {
            thoughtsListener.remove()
        }
    }
    
    // MARK: IBActions
    
    @IBAction func segmentCategoryValueChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            selectedCategory = ThoughtCategory.funny.rawValue
        case 1:
            selectedCategory = ThoughtCategory.serious.rawValue
        case 2:
            selectedCategory = ThoughtCategory.crazy.rawValue
        case 3:
            selectedCategory = ThoughtCategory.popular.rawValue
        default:
            selectedCategory = ThoughtCategory.funny.rawValue
        }
        
        thoughtsListener.remove()
        setListener()
    }
    
    @IBAction func btnLogoutTapped(_ sender: UIBarButtonItem) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let error {
            debugPrint("Cannot signout: \(error.localizedDescription)")
        }
    }
    
    // MARK: Private
    
    private func setListener() {
        if selectedCategory == ThoughtCategory.popular.rawValue {
            thoughtsListener = thoughtsCollectionRef
                .order(by: TIMESTAMP, descending: true)
                .addSnapshotListener { (snapshot, error) in
                    if let error = error {
                        debugPrint("getDocuments Error: \(error.localizedDescription)")
                        return
                    }
                    
                    self.thoughts = Thought.parseData(snapshot: snapshot)
                    self.tableThoughts.reloadData()
            }
        } else {
            thoughtsListener = thoughtsCollectionRef
                .whereField(CATEGORY, isEqualTo: selectedCategory)
                .order(by: TIMESTAMP, descending: true)
                .addSnapshotListener { (snapshot, error) in
                    if let error = error {
                        debugPrint("getDocuments Error: \(error.localizedDescription)")
                        return
                    }
                    
                    self.thoughts = Thought.parseData(snapshot: snapshot)
                    self.tableThoughts.reloadData()
            }
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentsVC",
            let vc = segue.destination as? CommentsVC,
            let thought = sender as? Thought {
            vc.thought = thought
        }
    }
}

// MARK: UITableView Delegate|Datasource

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return thoughts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ThoughtCell", for: indexPath) as? ThoughtCell else { return UITableViewCell() }
        let thought = thoughts[indexPath.row]
        cell.configureCell(thought: thought, delegate: self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thought = thoughts[indexPath.row]
        performSegue(withIdentifier: "CommentsVC", sender: thought)
    }
}

// MARK: - Thought Delegate

extension MainVC: ThoughtDelegate {
    
    func thoughtOptionTapped(thought: Thought) {
        let alert = UIAlertController(title: "Delete", message: "Do you want to delete your thought?", preferredStyle: .actionSheet)
        let btnDeleteAction = UIAlertAction(title: "Delete", style: .default) { (action) in
            
            self.delete(collection: Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).collection(COMMENTS_REF), completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    alert.dismiss(animated: true)
                    return
                }
                
                Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId).delete(completion: { (error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                        return
                    }
                    
                    alert.dismiss(animated: true)
                })
            })
            
            
        }
        let btnCancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(btnDeleteAction)
        alert.addAction(btnCancelAction)
        present(alert, animated: true)
    }
}

// MARK: - Firestore Delete Collections

extension MainVC {
    
    func delete(collection: CollectionReference, batchSize: Int = 100, completion: @escaping (Error?) -> ()) {
        // Limit query to avoid out-of-memory errors on large collections.
        // When deleting a collection guaranteed to fit in memory, batching can be avoided entirely
        collection.limit(to: batchSize).getDocuments { (docset, error) in
            // An error ocurred
            guard let docset = docset else {
                completion(error)
                return
            }
            
            // There is nothing to delete
            guard docset.count > 0 else {
                completion(nil)
                return
            }
            
            let batch = collection.firestore.batch()
            docset.documents.forEach { batch.deleteDocument($0.reference) }
            
            batch.commit(completion: { (batchError) in
                if let batchError = batchError {
                    // Stop the deletion process and handle error. Some elements
                    // may have been deleted
                    completion(batchError)
                } else {
                    self.delete(collection: collection, batchSize: batchSize, completion: completion)
                }
            })
        }
    }
}
