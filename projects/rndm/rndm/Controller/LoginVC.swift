//
//  LoginVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.layer.cornerRadius = 10
        btnSignup.layer.cornerRadius = 10
    }
    
    // MARK: IBActions
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        guard let email = txtEmail.text, !email.isEmpty,
            let password = txtPassword.text, !password.isEmpty else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            if let error = error {
                debugPrint("Could not SignIn: \(error.localizedDescription)")
                return
            }
            
            DispatchQueue.main.async {
                self.dismiss(animated: true)
            }
        }
        
    }
    
    @IBAction func btnCreateUserTapped(_ sender: UIButton) {
        
    }
}
