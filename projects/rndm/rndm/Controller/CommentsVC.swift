//
//  CommentsVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/10/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class CommentsVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var tableComments: UITableView!
    @IBOutlet private weak var txtComment: UITextField!
    @IBOutlet private weak var viewAddCommentContainer: UIView!
    
    // MARK: Properties
    
    var thought: Thought?
    private var comments = [Comment]()
    private var thoughtRef: DocumentReference!
    private let firestore = Firestore.firestore()
    private var username: String?
    private var commentsListener: ListenerRegistration!
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let thought = self.thought else { return }
        thoughtRef = firestore.collection(THOUGHTS_REF).document(thought.documentId)
        
        if let displayName = Auth.auth().currentUser?.displayName {
            username = displayName
        }
        
        self.view.bindToKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let thought = self.thought else { return }
        commentsListener = firestore
            .collection(THOUGHTS_REF)
            .document(thought.documentId)
            .collection(COMMENTS_REF)
            .order(by: TIMESTAMP, descending: false)
            .addSnapshotListener({ (snapshot, error) in
            if let error = error {
                debugPrint("Comments Error: \(error.localizedDescription)")
                return
            }
            
            self.comments = Comment.parseData(snapshot: snapshot)
            self.tableComments.reloadData()
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        commentsListener.remove()
    }
    
    // MARK: IBActions
    
    @IBAction func btnAddCommentTapped(_ sender: UIButton) {
        guard let comment = txtComment.text, !comment.isEmpty else { return }
        guard let thought = self.thought else { return }
        guard let username = self.username else { return }
        guard let currentUser = Auth.auth().currentUser else { return }
        firestore.runTransaction({ (transaction, errorPointer) -> Any? in
            let thoughtDocument: DocumentSnapshot
            
            do {
                try thoughtDocument = transaction.getDocument(Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId))
            } catch let error {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            guard let oldNumComments = thoughtDocument.data()?[NUM_COMMENTS] as? Int else { return nil }
            transaction.updateData([NUM_COMMENTS : oldNumComments + 1], forDocument: self.thoughtRef)
            
            let newCommentRef = self.firestore.collection(THOUGHTS_REF).document(thought.documentId).collection(COMMENTS_REF).document()
            transaction.setData([
                COMMENT : comment,
                TIMESTAMP : FieldValue.serverTimestamp(),
                USERNAME : username,
                USER_ID : currentUser.uid
                ], forDocument: newCommentRef)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                self.txtComment.text = ""
                self.txtComment.resignFirstResponder()
            }
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "UpdateCommentVC", let vc = segue.destination as? UpdateCommentVC, let commentData = sender as? (comment: Comment, thought: Thought) else { return }
        vc.commentData = commentData
    }
    
}

extension CommentsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentCell else { return UITableViewCell() }
        let comment = comments[indexPath.row]
        cell.configureCell(comment: comment, delegate: self)
        return cell
    }
}

extension CommentsVC: CommentDelegate {
    
    func commentOptionTapped(comment: Comment) {
        guard let thought = self.thought else { return }
        let alert = UIAlertController(title: "Edit Comment", message: "You can delete or edit", preferredStyle: .actionSheet)
        
        let btnDeleteAction = UIAlertAction(title: "Delete", style: .default) { (action) in
            /*// Method To Just Delete Something in a Collection
             self.firestore.collection(THOUGHTS_REF)
                .document(thought.documentId)
                .collection(COMMENTS_REF)
                .document(comment.documentId)
                .delete(completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    return
                }
                
                alert.dismiss(animated: true)
            })*/
            
            self.firestore.runTransaction({ (transaction, errorPointer) -> Any? in
                let thoughtDocument: DocumentSnapshot
                
                do {
                    let document = Firestore.firestore().collection(THOUGHTS_REF).document(thought.documentId)
                    try thoughtDocument = transaction.getDocument(document)
                } catch let error {
                    debugPrint("Fetch error: \(error.localizedDescription)")
                    return nil
                }
                
                guard let oldNumComments = thoughtDocument.data()?[NUM_COMMENTS] as? Int else { return nil }
                transaction.updateData([NUM_COMMENTS : oldNumComments - 1], forDocument: self.thoughtRef)
                
                let commentREF = self.firestore.collection(THOUGHTS_REF)
                    .document(thought.documentId)
                    .collection(COMMENTS_REF)
                    .document(comment.documentId)
                
                transaction.deleteDocument(commentREF)
                
                return nil
            }) { (object, error) in
                if let error = error {
                    debugPrint("Transaction failed: \(error.localizedDescription)")
                } else {
                    alert.dismiss(animated: true)
                }
            }
        }
        let btnEditAction = UIAlertAction(title: "Edit", style: .default) { (action) in
            self.performSegue(withIdentifier: "UpdateCommentVC", sender: (comment, thought))
            alert.dismiss(animated: true)
        }
        let btnCancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(btnDeleteAction)
        alert.addAction(btnEditAction)
        alert.addAction(btnCancelAction)
        
        present(alert, animated: true)
    }
}
