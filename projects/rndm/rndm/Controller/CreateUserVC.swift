//
//  CreateUserVC.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class CreateUserVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var btnCreateUser: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCreateUser.layer.cornerRadius = 10
        btnCancel.layer.cornerRadius = 10
    }
    
    // MARK: IBActions
    
    @IBAction func btnCreateUserTapped(_ sender: UIButton) {
        guard let email = txtEmail.text, !email.isEmpty else { return }
        guard let password = txtPassword.text, !password.isEmpty else { return }
        guard let username = txtUsername.text, !username.isEmpty else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            if let error = error {
                debugPrint("Error: \(error.localizedDescription)")
                return
            }
            
            guard let user = authDataResult?.user else { return }
            let changeRequest = user.createProfileChangeRequest()
            changeRequest.displayName = username
            changeRequest.commitChanges(completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    return
                }
            })
            
            Firestore.firestore()
                .collection(USERS_REF)
                .document(user.uid)
                .setData([USERNAME : username, DATE_CREATED : FieldValue.serverTimestamp()], completion: { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    return
                }
                self.dismiss(animated: true)
            })
        }
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}
