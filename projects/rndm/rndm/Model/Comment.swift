//
//  Comment.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/10/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Firebase

struct Comment {
    
    // MARK: Properties
    
    private(set) var documentId: String
    private(set) var userId: String
    private(set) var username: String
    private(set) var timestamp: Date
    private(set) var content: String
    
    // MARK: Init
    
    init(documentId: String, userId: String, username: String, timestamp: Date, content: String) {
        self.documentId = documentId
        self.userId = userId
        self.username = username
        self.timestamp = timestamp
        self.content = content
    }
    
    static func parseData(snapshot: QuerySnapshot?) -> [Comment] {
        var comments = [Comment]()
        
        guard let snapshot = snapshot else { return comments }
        for document in snapshot.documents {
            let data = document.data()
            let documentId = document.documentID
            let userId = data[USER_ID] as? String ?? ""
            let username = data[USERNAME] as? String ?? "Anonymous"
            let timestamp = data[TIMESTAMP] as? Date ?? Date()
            let comment = data[COMMENT] as? String ?? ""
            
            let newCommnet = Comment(documentId: documentId, userId: userId, username: username, timestamp: timestamp, content: comment)
            comments.append(newCommnet)
        }
        return comments
    }
    
}
