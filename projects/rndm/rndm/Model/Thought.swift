//
//  Thought.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Firebase

struct Thought {
    
    // MARK: Properties
    
    private(set) var username: String
    private(set) var timestamp: Date
    private(set) var content: String
    private(set) var likes: Int
    private(set) var comments: Int
    private(set) var documentId: String
    private(set) var userId: String
    
    // MARK: Init
    
    init(documentId: String, userId: String, username: String, timestamp: Date, content: String, likes: Int, comments: Int) {
        self.documentId = documentId
        self.userId = userId
        self.username = username
        self.timestamp = timestamp
        self.content = content
        self.likes = likes
        self.comments = comments
    }
    
    static func parseData(snapshot: QuerySnapshot?) -> [Thought] {
        var thoughts = [Thought]()
        
        guard let snapshot = snapshot else { return thoughts }
        for document in snapshot.documents {
            let data = document.data()
            let id = document.documentID
            let username = data[USERNAME] as? String ?? "Anonymous"
            let timestamp = data[TIMESTAMP] as? Date ?? Date()
            let content = data[CONTENT] as? String ?? ""
            let likes = data[NUM_LIKES] as? Int ?? 0
            let comments = data[NUM_COMMENTS] as? Int ?? 0
            let userId = data[USER_ID] as? String ?? ""
            
            let newThought = Thought(documentId: id, userId: userId, username: username, timestamp: timestamp, content: content, likes: likes, comments: comments)
            thoughts.append(newThought)
        }
        return thoughts
    }
    
}
