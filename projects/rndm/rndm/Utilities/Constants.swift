//
//  Constants.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

let THOUGHTS_REF = "thoughts"
let USERS_REF = "users"
let COMMENTS_REF = "comments"

let CATEGORY = "category"
let NUM_COMMENTS = "comments"
let NUM_LIKES = "likes"
let CONTENT = "content"
let TIMESTAMP = "timestamp"
let USERNAME = "username"
let DATE_CREATED = "dateCreated"
let COMMENT = "comment"
let USER_ID = "userId"
