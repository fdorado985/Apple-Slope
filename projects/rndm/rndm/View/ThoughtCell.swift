//
//  ThoughtCell.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

protocol ThoughtDelegate {
    func thoughtOptionTapped(thought: Thought)
}

class ThoughtCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblUsername: UILabel!
    @IBOutlet private weak var lblTimestamp: UILabel!
    @IBOutlet private weak var lblContent: UILabel!
    @IBOutlet private weak var lblLikes: UILabel!
    @IBOutlet private weak var imgLikes: UIImageView!
    @IBOutlet private weak var lblComments: UILabel!
    @IBOutlet private weak var imgComments: UIImageView!
    @IBOutlet private weak var imgOptionMenu: UIImageView!
    
    // MARK: Properties
    
    private var thought: Thought?
    private var delegate: ThoughtDelegate?
    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm"
        return formatter
    }
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
        imgLikes.addGestureRecognizer(tap)
        imgLikes.isUserInteractionEnabled = true
    }
    
    // MARK: Private
    
    @objc private func likeTapped() {
        guard let thought = self.thought else { return }
        // Method-01
        Firestore.firestore()
            .collection(THOUGHTS_REF)
            .document(thought.documentId)
            .setData([NUM_LIKES : thought.likes + 1], merge: true)
        
        // Method-02
//        Firestore.firestore()
//            .document("\(THOUGHTS_REF)/\(thought.documentId)")
//            .updateData([NUM_LIKES : thought.likes + 1])
    }
    
    // MARK: Public
    
    func configureCell(thought: Thought, delegate: ThoughtDelegate) {
        guard let currentUser = Auth.auth().currentUser else { return }
        self.thought = thought
        self.delegate = delegate
        imgOptionMenu.isHidden = true
        lblUsername.text = thought.username
        lblContent.text = thought.content
        lblLikes.text = "\(thought.likes)"
        lblComments.text = "\(thought.comments)"
        
        let timestamp = dateFormatter.string(from: thought.timestamp)
        lblTimestamp.text = timestamp
        
        if thought.userId == currentUser.uid {
            imgOptionMenu.isHidden = false
            imgOptionMenu.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(menuOptionTapped))
            imgOptionMenu.addGestureRecognizer(tap)
        }
    }
    
    // MARK: Private
    
    @objc private func menuOptionTapped() {
        guard let thought = self.thought else { return }
        delegate?.thoughtOptionTapped(thought: thought)
    }
}
