//
//  CommentCell.swift
//  rndm
//
//  Created by Juan Francisco Dorado Torres on 9/10/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

protocol CommentDelegate {
    func commentOptionTapped(comment: Comment)
}

class CommentCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblUsername: UILabel!
    @IBOutlet private weak var lblTimestamp: UILabel!
    @IBOutlet private weak var lblContent: UILabel!
    @IBOutlet private weak var imgOptionMenu: UIImageView!
    
    // MARK: Properties
    
    private var comment: Comment?
    private var delegate: CommentDelegate?
    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, hh:mm"
        return formatter
    }
    
    // MARK: Public
    
    func configureCell(comment: Comment, delegate: CommentDelegate) {
        self.comment = comment
        self.delegate = delegate
        
        lblUsername.text = comment.username
        lblContent.text = comment.content
        imgOptionMenu.isHidden = true
        
        let timestamp = dateFormatter.string(from: comment.timestamp)
        lblTimestamp.text = timestamp
        
        guard let currentUser = Auth.auth().currentUser else { return }
        
        if comment.userId == currentUser.uid {
            imgOptionMenu.isHidden = false
            let tap = UITapGestureRecognizer(target: self, action: #selector(menuOptionTapped))
            imgOptionMenu.addGestureRecognizer(tap)
            imgOptionMenu.isUserInteractionEnabled = true
        }
    }
    
    // MARK: Private
    
    @objc private func menuOptionTapped() {
        guard let comment = self.comment else { return }
        delegate?.commentOptionTapped(comment: comment)
    }
}
