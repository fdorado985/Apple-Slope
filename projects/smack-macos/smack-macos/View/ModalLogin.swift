//
//  ModalLogin.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ModalLogin: NSView {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var view: NSView!
    @IBOutlet private weak var txtUsername: NSTextField!
    @IBOutlet private weak var txtPassword: NSSecureTextField!
    @IBOutlet private weak var btnLogin: NSButton!
    @IBOutlet private weak var btnCreateAccount: NSButton!
    @IBOutlet private weak var stackLogin: NSStackView!
    @IBOutlet private weak var progressSpinner: NSProgressIndicator!
    
    // MARK: View cycle
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        Bundle.main.loadNibNamed("ModalLogin", owner: self, topLevelObjects: nil)
        self.addSubview(self.view)
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnLoginClicked(_ sender: NSButton) {
        let email = txtUsername.stringValue
        let password = txtPassword.stringValue
        guard !email.isEmpty, !password.isEmpty else { return }
        progressSpinner.isHidden = false
        progressSpinner.startAnimation(nil)
        stackLogin.alphaValue = 0.4
        btnLogin.isEnabled = false
        AuthService.instance.loginUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.findUserByEmail(completion: { (success) in
                    if success {
                        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                    }
                })
            }
            self.progressSpinner.isHidden = true
            self.progressSpinner.stopAnimation(nil)
            self.stackLogin.alphaValue = 1.0
            self.btnLogin.isEnabled = true
        }
    }
    
    @IBAction func btnCreateAccountClicked(_ sender: NSButton) {
        let closeImmediatelyDict: [String : Bool] = [USER_INFO_REMOVE_IMMEDIATELY : true]
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil, userInfo: closeImmediatelyDict)
        
        let createAccountDict: [String : ModalType] = [USER_INFO_MODAL : .createAccount]
        NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: createAccountDict)
    }
    
    @IBAction func btnCloseClicked(_ sender: NSButton) {
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
    }
    @IBAction func txtPasswordEnterAction(_ sender: NSSecureTextField) {
        btnLogin.performClick(nil)
    }
    
    // MARK: Functions
    
    private func setupView() {
        self.view.wantsLayer = true
        self.view.frame = NSRect(x: 0, y: 0, width: 475, height: 300)
        view.layer?.backgroundColor = CGColor.white
        view.layer?.cornerRadius = 7
        
        btnLogin.layer?.backgroundColor = CHAT_GREEN.cgColor
        btnLogin.layer?.cornerRadius = 7
        btnLogin.styleButtonText(btnLogin, buttonName: "Login", fontColor: .white, alignment: .center, font: AVENIR_REGULAR, size: 14.0)
        
        btnCreateAccount.styleButtonText(btnCreateAccount, buttonName: "Create Account", fontColor: CHAT_GREEN, alignment: .center, font: AVENIR_REGULAR, size: 12.0)
    }
    
}
