//
//  AvatarPickerVC.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

enum AnimalType {
    case dark
    case light
}

class AvatarPickerVC: NSViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var segmentControl: NSSegmentedControl!
    @IBOutlet private weak var collectionAvatars: NSCollectionView!
    
    // MARK: Properties
    
    private var animalType: AnimalType = .dark
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionAvatars.delegate = self
        collectionAvatars.dataSource = self
        segmentControl.wantsLayer = true
        segmentControl.cell?.controlTint = NSControlTint.clearControlTint
        segmentControl.selectedSegment = 0
    }
    
    // MARK: IBActions
    
    @IBAction func segmentChanged(_ sender: NSSegmentedControl) {
        if segmentControl.selectedSegment == 0 {
            animalType = .dark
        } else {
            animalType = .light
        }
        
        collectionAvatars.reloadData()
    }
}

// MARK: - NSCollectionView Delegate|Datasource

extension AvatarPickerVC: NSCollectionViewDelegate, NSCollectionViewDataSource, NSCollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return 28
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let identifier = NSUserInterfaceItemIdentifier(rawValue: "AnimalCell")
        let cell = collectionView.makeItem(withIdentifier: identifier, for: indexPath)
        guard let animalCell = cell as? AnimalCell else { fatalError("\(#function): AnimalCell was not found") }
        animalCell.configureCell(index: indexPath.item, type: animalType)
        return animalCell
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        if let selectedIndexPath = collectionView.selectionIndexPaths.first {
            if animalType == .dark {
                UserDataService.instance.avatarName = "dark\(selectedIndexPath.item)"
            } else {
                UserDataService.instance.avatarName = "light\(selectedIndexPath.item)"
            }
        }
        
        view.window?.cancelOperation(nil)
    }
    
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        return NSSize(width: 85.0, height: 85.0)
    }
}
