//
//  AnimalCell.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class AnimalCell: NSCollectionViewItem {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgAnimal: NSImageView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        setupView()
    }
    
    // MARK: Functions
    
    private func setupView() {
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.lightGray.cgColor
        view.layer?.cornerRadius = 10
        view.layer?.borderWidth = 2
        view.layer?.borderColor = NSColor.darkGray.cgColor
    }
    
    func configureCell(index: Int, type: AnimalType) {
        switch type {
        case .dark:
            let imageName = "dark\(index)"
            imgAnimal.image = NSImage(named: imageName)
            view.layer?.backgroundColor = NSColor.lightGray.cgColor
        case .light:
            let imageName = "light\(index)"
            imgAnimal.image = NSImage(named: imageName)
            view.layer?.backgroundColor = NSColor.gray.cgColor
            
        }
    }
    
}
