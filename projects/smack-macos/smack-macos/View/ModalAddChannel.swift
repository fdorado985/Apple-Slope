//
//  ModalAddChannel.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ModalAddChannel: NSView {

    // MARK: IBOutlets
    
    @IBOutlet private weak var view: NSView!
    @IBOutlet private weak var btnCreateChannel: NSButton!
    @IBOutlet private weak var txtName: NSTextField!
    @IBOutlet private weak var txtDescription: NSTextField!
    @IBOutlet private weak var progressSpinner: NSProgressIndicator!
    @IBOutlet private weak var stackAddChannel: NSStackView!
    
    // MARK: View cycle
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        Bundle.main.loadNibNamed("ModalAddChannel", owner: self, topLevelObjects: nil)
        self.addSubview(self.view)
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnCreateChannelClicked(_ sender: NSButton) {
        let name = txtName.stringValue
        let description = txtDescription.stringValue
        guard !name.isEmpty, !description.isEmpty else { return }
        startSpinner()
        SocketService.instance.addChannel(name: name, description: description) { (success) in
            self.stopSpinner()
            if success {
                NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
            }
        }
        
    }
    
    @IBAction func btnCloseClicked(_ sender: NSButton) {
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
    }
    
    // MARK: Functions
    
    private func setupView() {
        self.view.wantsLayer = true
        self.view.frame = NSRect(x: 0, y: 0, width: 475, height: 300)
        view.layer?.backgroundColor = CGColor.white
        view.layer?.cornerRadius = 7
        
        btnCreateChannel.layer?.backgroundColor = CHAT_GREEN.cgColor
        btnCreateChannel.layer?.cornerRadius = 7
        btnCreateChannel.styleButtonText(btnCreateChannel, buttonName: "Create Channel", fontColor: .white, alignment: .center, font: AVENIR_REGULAR, size: 14.0)
    }
    
    private func startSpinner() {
        progressSpinner.isHidden = false
        progressSpinner.startAnimation(nil)
        btnCreateChannel.isEnabled = false
        stackAddChannel.alphaValue = 0.4
    }
    
    private func stopSpinner() {
        progressSpinner.isHidden = true
        progressSpinner.stopAnimation(nil)
        btnCreateChannel.isEnabled = true
        stackAddChannel.alphaValue = 1.0
    }
}
