//
//  ModalCreateAccount.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ModalCreateAccount: NSView {

    // MARK: IBOutlets
    
    @IBOutlet private weak var view: NSView!
    @IBOutlet private weak var txtName: NSTextField!
    @IBOutlet private weak var txtEmail: NSTextField!
    @IBOutlet private weak var txtPassword: NSSecureTextField!
    @IBOutlet private weak var btnCreateAccount: NSButton!
    @IBOutlet private weak var btnChooseImage: NSButton!
    @IBOutlet private weak var imgProfile: NSImageView!
    @IBOutlet private weak var progressSpinner: NSProgressIndicator!
    @IBOutlet private weak var stackCreateAccount: NSStackView!
    @IBOutlet private weak var colorWell: NSColorWell!
    
    // MARK: Properties
    
    private var avatarName = "profileDefault"
    private var avatarColor = "[0.5, 0.5, 0.5, 1]"
    private let popover = NSPopover()
    
    // MARK: View cycle
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        Bundle.main.loadNibNamed("ModalCreateAccount", owner: self, topLevelObjects: nil)
        self.addSubview(self.view)
        popover.delegate = self
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnCreateAccountClicked(_ sender: NSButton) {
        let email = txtEmail.stringValue
        let password = txtPassword.stringValue
        let name = txtName.stringValue
        
        guard !email.isEmpty, !password.isEmpty, !name.isEmpty else { return }
        startSpinner()
        AuthService.instance.registerUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.loginUser(email: email, password: password, completion: { (success) in
                    if success {
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, completion: { (success) in
                            if success {
                                NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                            }
                            self.stopSpinner()
                        })
                    } else {
                        self.stopSpinner()
                    }
                })
            } else {
                self.stopSpinner()
            }
        }
    }
    
    @IBAction func btnChooseImageClicked(_ sender: NSButton) {
        popover.contentViewController = AvatarPickerVC(nibName: "AvatarPickerVC", bundle: nil)
        popover.show(relativeTo: btnChooseImage.bounds, of: btnChooseImage, preferredEdge: .minX)
        popover.behavior = .transient
    }
    
    @IBAction func btnCloseClicked(_ sender: NSButton) {
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
    }
    
    @IBAction func colorPicked(_ sender: NSColorWell) {
        imgProfile.layer?.backgroundColor = sender.color.cgColor
        let color = sender.color.cgColor
        guard let colorArray = color.components?.description else { return }
        avatarColor = colorArray
    }
    
    // MARK: Functions
    
    private func setupView() {
        self.view.wantsLayer = true
        self.view.frame = NSRect(x: 0, y: 0, width: 475, height: 300)
        view.layer?.backgroundColor = CGColor.white
        view.layer?.cornerRadius = 7
        
        imgProfile.layer?.cornerRadius = 10
        imgProfile.layer?.borderColor = NSColor.gray.cgColor
        imgProfile.layer?.borderWidth = 3
        
        btnCreateAccount.layer?.backgroundColor = CHAT_GREEN.cgColor
        btnCreateAccount.layer?.cornerRadius = 7
        btnCreateAccount.styleButtonText(btnCreateAccount, buttonName: "Create Account", fontColor: .white, alignment: .center, font: AVENIR_REGULAR, size: 14.0)
        
        btnChooseImage.layer?.backgroundColor = CHAT_GREEN.cgColor
        btnChooseImage.layer?.cornerRadius = 7
        btnChooseImage.styleButtonText(btnChooseImage, buttonName: "Choose Image", fontColor: .white, alignment: .center, font: AVENIR_REGULAR, size: 14.0)
    }
    
    private func startSpinner() {
        progressSpinner.isHidden = false
        progressSpinner.startAnimation(nil)
        btnCreateAccount.isEnabled = false
        btnChooseImage.isEnabled = false
        stackCreateAccount.alphaValue = 0.4
    }
    
    private func stopSpinner() {
        progressSpinner.isHidden = true
        progressSpinner.stopAnimation(nil)
        btnCreateAccount.isEnabled = true
        btnChooseImage.isEnabled = true
        stackCreateAccount.alphaValue = 1.0
    }
    
}

// MARK: - NSPopover Delegate

extension ModalCreateAccount: NSPopoverDelegate {
    
    func popoverDidClose(_ notification: Notification) {
        if UserDataService.instance.avatarName != "" {
            imgProfile.image = NSImage(named: UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
        }
    }
}
