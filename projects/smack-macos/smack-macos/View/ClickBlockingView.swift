//
//  ClickBlockingView.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ClickBlockingView: NSView {

    // MARK: View cycle
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
    }
    
    // MARK: Mouse Events
    
    override func mouseDown(with event: NSEvent) { }
    
    override func mouseUp(with event: NSEvent) { }
    
    override func mouseDragged(with event: NSEvent) { }
    
    override func mouseMoved(with event: NSEvent) { }
    
}
