//
//  ChannelCell.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ChannelCell: NSTableCellView {
    
    @IBOutlet weak var lblName: NSTextField!
    
    // MARK: View cycle

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
    }
    
    // MARK: Functions
    
    func configureCell(channel: Channel, selectedChannel: Int, row: Int) {
        lblName.stringValue = "#\(channel.name)"
        lblName.font = NSFont(name: AVENIR_REGULAR, size: 13.0)
        
        for id in MessageService.instance.unreadChannels {
            if id == channel.id {
                lblName.font = NSFont(name: AVENIR_BOLD, size: 13.0)
            }
        }
        
        wantsLayer = true
        if row == selectedChannel {
            layer?.backgroundColor = CHAT_GREEN.cgColor
            lblName.textColor = NSColor.white
        } else {
            layer?.backgroundColor = CGColor.clear
            lblName.textColor = NSColor.controlColor
        }
    }
    
}
