//
//  ChatCell.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/10/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ChatCell: NSTableCellView {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgProfile: NSImageView!
    @IBOutlet weak var lblUsername: NSTextField!
    @IBOutlet weak var lblTimestamp: NSTextField!
    @IBOutlet weak var lblMessage: NSTextField!
    
    // MARK: View cycle

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        imgProfile.wantsLayer = true
        imgProfile.layer?.cornerRadius = 6
        //imgProfile.layer?.borderColor = NSColor.gray.cgColor
        //imgProfile.layer?.borderWidth = 2
    }
    
    // MARK: Functions
    
    func configureCell(message: Message) {
        lblMessage.stringValue = message.message
        lblUsername.stringValue = message.username
        imgProfile.image = NSImage(named: message.avatar)
        imgProfile.layer?.backgroundColor = UserDataService.instance.returnCGColor(components: message.avatarColor)
        
        // Convert date
        var isoDate = message.timestamp
        let end = isoDate.index(isoDate.endIndex, offsetBy: -5)
        isoDate = String(isoDate[..<end])
        
        let isoFormatter = ISO8601DateFormatter()
        let chatDate = isoFormatter.date(from: isoDate.appending("Z"))
        
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "MMM d, h:mm a"
        
        if let finalDate = chatDate {
            let finalDate = newFormatter.string(from: finalDate)
            lblTimestamp.stringValue = finalDate
        }
    }
}
