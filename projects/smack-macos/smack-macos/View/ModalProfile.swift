//
//  ModalProfile.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ModalProfile: NSView {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var view: NSView!
    @IBOutlet weak var btnLogout: NSButton!
    @IBOutlet weak var lblName: NSTextField!
    @IBOutlet weak var lblEmail: NSTextField!
    @IBOutlet weak var imgProfile: NSImageView!
    
    // MARK: View cycle
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        Bundle.main.loadNibNamed("ModalProfile", owner: self, topLevelObjects: nil)
        self.addSubview(self.view)
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        setupView()
    }
    
    // MARK: IBActions
    
    @IBAction func btnLogoutClicked(_ sender: NSButton) {
        UserDataService.instance.logoutUser()
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        
    }
    
    @IBAction func btnCloseClicked(_ sender: NSButton) {
        NotificationCenter.default.post(name: NOTIF_CLOSE_MODAL, object: nil)
    }
    
    // MARK: Functions
    
    private func setupView() {
        self.view.wantsLayer = true
        self.view.frame = NSRect(x: 0, y: 0, width: 475, height: 300)
        view.layer?.backgroundColor = CGColor.white
        view.layer?.cornerRadius = 7
        
        imgProfile.layer?.cornerRadius = 10
        imgProfile.layer?.borderColor = NSColor.gray.cgColor
        imgProfile.layer?.borderWidth = 3
        imgProfile.image = NSImage(named: UserDataService.instance.avatarName)
        imgProfile.layer?.backgroundColor = UserDataService.instance.returnCGColor(components: UserDataService.instance.avatarColor)
        
        lblName.stringValue = UserDataService.instance.name
        lblEmail.stringValue = UserDataService.instance.email

        btnLogout.layer?.backgroundColor = CHAT_GREEN.cgColor
        btnLogout.layer?.cornerRadius = 7
        btnLogout.styleButtonText(btnLogout, buttonName: "Logout", fontColor: .white, alignment: .center, font: AVENIR_REGULAR, size: 14.0)
    }
}
