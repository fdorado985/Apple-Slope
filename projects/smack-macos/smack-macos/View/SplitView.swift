//
//  SplitView.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/10/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class SplitView: NSSplitView {

    // MARK: View cycle
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
    }
    
    override var dividerThickness: CGFloat {
        get { return 0.0 }
    }
}
