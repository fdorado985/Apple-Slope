//
//  NSButton+Ext.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

extension NSButton {
    
    func styleButtonText(_ button: NSButton, buttonName: String, fontColor: NSColor, alignment: NSTextAlignment, font: String, size: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        
        let buttonAttributes = [NSAttributedString.Key.foregroundColor : fontColor,
                                NSAttributedString.Key.paragraphStyle : paragraphStyle,
                                NSAttributedString.Key.font : NSFont(name: font, size: size)!]
        
        button.attributedTitle = NSAttributedString(string: buttonName, attributes: buttonAttributes)
    }
}
