//
//  Constants.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

// MARK: URL Constants

let BASE_URL = "https://smacky-chat-api.herokuapp.com/v1/"
let URL_REGISTER = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"
let URL_USER_ADD = "\(BASE_URL)user/add"
let URL_USER_BY_EMAIL = "\(BASE_URL)user/byEmail/"
let URL_GET_CHANNELS = "\(BASE_URL)channel/"
let URL_GET_MESSAGES = "\(BASE_URL)message/byChannel/"

// MARK: Headers

let HEADERS = ["Content-Type" : "application/json; charset=utf-8"]
let BEARER_HEADER = ["Authorization" : "Bearer \(AuthService.instance.authToken)", "Content-Type" : "application/json; charset=utf-8"]

// MARK: CompletionHandlers

typealias CompletionHandler = (_ success: Bool) -> ()

// MARK: Colors

let CHAT_PURPLE = NSColor(calibratedRed: 0.30, green: 0.22, blue: 0.29, alpha: 1.0)
let CHAT_GREEN = NSColor(calibratedRed: 0.22, green: 0.66, blue: 0.68, alpha: 1.0)

// MARK: Fonts

let AVENIR_REGULAR = "AvenirNext-Regular"
let AVENIR_BOLD = "AvenirNext-Bold"

// MARK: Notifications

let USER_INFO_MODAL = "modalUserInfo"
let NOTIF_PRESENT_MODAL = Notification.Name("presentModal")
let NOTIF_CLOSE_MODAL = Notification.Name("closeModal")
let NOTIF_USER_DATA_DID_CHANGE = Notification.Name("notifUserDataChanged")
let NOTIF_CHANNELS_LOADED = Notification.Name("notifChannelsLoaded")
let USER_INFO_REMOVE_IMMEDIATELY = "modalRemoveImmediately"

// MARK: UserDefaults

let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL_KEY = "userEmail"
