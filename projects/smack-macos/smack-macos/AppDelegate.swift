//
//  AppDelegate.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        SocketService.instance.closeConnection()
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
        SocketService.instance.establishConnection()
    }
    
    func applicationWillHide(_ notification: Notification) {
        UserDataService.instance.isMinimizing = true
    }
    
    func applicationDidUnhide(_ notification: Notification) {
        UserDataService.instance.isMinimizing = false
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }


}

