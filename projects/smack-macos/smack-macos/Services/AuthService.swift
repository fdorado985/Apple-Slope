//
//  AuthService.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    
    // MARK: Singleton
    
    static let instance = AuthService()
    
    // MARK: Properties
    
    private let defaults = UserDefaults.standard
    
    var isLoggedIn: Bool {
        get { return defaults.bool(forKey: LOGGED_IN_KEY) }
        set { defaults.set(newValue, forKey: LOGGED_IN_KEY) }
    }
    
    var authToken: String {
        get { return defaults.value(forKey: TOKEN_KEY) as! String }
        set { defaults.set(newValue, forKey: TOKEN_KEY) }
    }
    
    var userEmail: String {
        get { return defaults.value(forKey: USER_EMAIL_KEY) as! String }
        set { defaults.set(newValue, forKey: USER_EMAIL_KEY) }
    }
    
    // MARK: Functions
    
    func registerUser(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = ["email" : lowerCaseEmail, "password" : password]
        
        Alamofire.request(URL_REGISTER, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADERS).responseString { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
            
            completion(true)
        }
    }
    
    func loginUser(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = ["email" : lowerCaseEmail, "password" : password]
        
        Alamofire.request(URL_LOGIN, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADERS).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
            
            guard let data = response.data else { return }
            do {
                let json = try JSON(data: data)
                self.authToken = json["token"].stringValue
                self.userEmail = json["user"].stringValue
                self.isLoggedIn = true
                completion(true)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
        }
    }
    
    func createUser(name: String, email: String, avatarName: String, avatarColor: String, completion: @escaping CompletionHandler) {
        let lowerCaseEmail = email.lowercased()
        let body: [String : Any] = ["name" : name, "email" : lowerCaseEmail, "avatarName" : avatarName, "avatarColor" : avatarColor]
        
        Alamofire.request(URL_USER_ADD, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
            
            guard let data = response.data else { return }
            do {
                let json = try JSON(data: data)
                let userId = json["_id"].stringValue
                let avatarColor = json["avatarColor"].stringValue
                let avatarName = json["avatarName"].stringValue
                let email = json["email"].stringValue
                let username = json["name"].stringValue
                
                UserDataService.instance.id = userId
                UserDataService.instance.name = username
                UserDataService.instance.email = email
                UserDataService.instance.avatarName = avatarName
                UserDataService.instance.avatarColor = avatarColor
                
                completion(true)
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
        }
    }
    
    func findUserByEmail(completion: @escaping CompletionHandler) {
        Alamofire.request("\(URL_USER_BY_EMAIL)\(userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
            
            guard let data = response.data else { return }
            self.setUserInfo(data: data)
            completion(true)
        }
    }
    
    func setUserInfo(data: Data) {
        do {
            let json = try JSON(data: data)
            UserDataService.instance.id = json["_id"].stringValue
            UserDataService.instance.name = json["name"].stringValue
            UserDataService.instance.email = json["email"].stringValue
            UserDataService.instance.avatarName = json["avatarName"].stringValue
            UserDataService.instance.avatarColor = json["avatarColor"].stringValue
        } catch let error {
            debugPrint("\(#function): \(error.localizedDescription)")
            return
        }
        
    }
}
