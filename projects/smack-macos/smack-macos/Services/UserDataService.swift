//
//  UserDataService.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/8/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class UserDataService {
    
    // MARK: Singleton
    
    static let instance = UserDataService()
    
    // MARK: Properties
    
    private var _id = ""
    private var _avatarColor = ""
    private var _avatarName = ""
    private var _email = ""
    private var _name = ""
    private var _isMinimizing = false
    
    // MARK: Data Encapsulation
    
    var id: String {
        get { return _id }
        set { _id = newValue }
    }
    
    var avatarColor: String {
        get { return _avatarColor }
        set { _avatarColor = newValue }
    }
    
    var avatarName: String {
        get { return _avatarName }
        set { _avatarName = newValue }
    }
    
    var email: String {
        get { return _email }
        set { _email = newValue }
    }
    
    var name: String {
        get { return _name }
        set { _name = newValue }
    }
    
    var isMinimizing: Bool {
        get { return _isMinimizing }
        set { _isMinimizing = newValue }
    }
    
    func logoutUser() {
        _id = ""
        _avatarColor = ""
        _avatarName = ""
        _email = ""
        _name = ""
        AuthService.instance.authToken = ""
        AuthService.instance.userEmail = ""
        AuthService.instance.isLoggedIn = false
        MessageService.instance.clearChannels()
        MessageService.instance.clearMessages()
    }
    
    func returnCGColor(components: String) -> CGColor {
        let scanner = Scanner(string: components)
        let skipped = CharacterSet(charactersIn: "[], ")
        let comma = CharacterSet(charactersIn: ",")
        scanner.charactersToBeSkipped = skipped
        
        var r, g, b, a: NSString?
        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)
        
        let defaultColor = CGColor(red: 0.69, green: 0.85, blue: 0.99, alpha: 1.0)
        guard let rUnwrapped = r else { return defaultColor }
        guard let gUnwrapped = g else { return defaultColor }
        guard let bUnwrapped = b else { return defaultColor }
        guard let aUnwrapped = a else { return defaultColor }
        
        let rFloat = CGFloat(rUnwrapped.doubleValue)
        let gFloat = CGFloat(gUnwrapped.doubleValue)
        let bFloat = CGFloat(bUnwrapped.doubleValue)
        let aFloat = CGFloat(aUnwrapped.doubleValue)
        
        let newCGColor = CGColor(red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
        return newCGColor
    }
}
