//
//  MessageService.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    
    // MARK: Singleton
    
    static let instance = MessageService()
    
    // MARK: Properties
    
    var channels = [Channel]()
    var messages = [Message]()
    var unreadChannels = [String]()
    
    // MARK: Functions
    
    func findAllChannel(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data else { return }
            do {
                let json = try JSON(data: data)
                if let jsonArray = json.array {
                    self.setChannelInfo(allJSONs: jsonArray)
                    NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
                    completion(true)
                } else { completion(false) }
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
        }
    }
    
    func findAllMessages(for channelId: String, completion: @escaping CompletionHandler) {
        Alamofire.request("\(URL_GET_MESSAGES)\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            self.clearMessages()
            if response.result.error != nil {
                completion(false)
                return
            }
            
            guard let data = response.data else { return }
            do {
                let json = try JSON(data: data)
                if let jsonArray = json.array {
                    self.setMessageInfo(allJSONs: jsonArray)
                    completion(true)
                } else { completion(false) }
            } catch let error {
                debugPrint("\(#function): \(error.localizedDescription)")
                completion(false)
                return
            }
        }
    }
    
    func clearMessages() {
        messages.removeAll()
    }
    
    func clearChannels() {
        channels.removeAll()
    }
    
    private func setChannelInfo(allJSONs: [JSON]) {
        for json in allJSONs {
            let id = json["_id"].stringValue
            let name = json["name"].stringValue
            let description = json["description"].stringValue
            let channel = Channel(name: name, description: description, id: id)
            channels.append(channel)
        }
    }
    
    private func setMessageInfo(allJSONs: [JSON]) {
        for json in allJSONs {
            let id = json["_id"].stringValue
            let messageBody = json["messageBody"].stringValue
            let userId = json["userId"].stringValue
            let channelId = json["channelId"].stringValue
            let username = json["userName"].stringValue
            let avatar = json["userAvatar"].stringValue
            let avatarColor = json["userAvatarColor"].stringValue
            let timestamp = json["timeStamp"].stringValue
            let message = Message(id: id, message: messageBody, username: username, channelId: channelId, avatar: avatar, avatarColor: avatarColor, userId: userId, timestamp: timestamp)
            messages.append(message)
        }
    }
}
