//
//  ChannelVC.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ChannelVC: NSViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblUsername: NSTextField!
    @IBOutlet weak var btnAddChannel: NSButton!
    @IBOutlet weak var tableChannels: NSTableView!
    
    // MARK: Properties
    
    private var selectedChannelIndex = 0
    private var selectedChannel: Channel?
    private var chatVC: ChatVC?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        tableChannels.delegate = self
        tableChannels.dataSource = self
        setupView()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if UserDataService.instance.isMinimizing { return }
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        if UserDataService.instance.isMinimizing { return }
        chatVC = self.view.window?.contentViewController?.children[0].children[1] as? ChatVC
        SocketService.instance.getChannel { (success) in
            if success {
                self.tableChannels.reloadData()
                if MessageService.instance.channels.count > 0 {
                    self.tableChannels.selectRowIndexes(IndexSet(integer: self.selectedChannelIndex), byExtendingSelection: false)
                }
            }
        }
        
        SocketService.instance.getChatMessage { (newMessage) in
            if newMessage.channelId != self.selectedChannel?.id && AuthService.instance.isLoggedIn {
                MessageService.instance.unreadChannels.append(newMessage.channelId)
                self.tableChannels.reloadData()
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnAddClicked(_ sender: NSButton) {
        if AuthService.instance.isLoggedIn {
            let loginDict: [String : ModalType] = [USER_INFO_MODAL : ModalType.addChannel]
            NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: loginDict)
        } else {
            let loginDict: [String : ModalType] = [USER_INFO_MODAL : ModalType.login]
            NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: loginDict)
        }
    }
    
    // MARK: Functions
    
    private func setupView() {
        view.wantsLayer = true
        view.layer?.backgroundColor = CHAT_PURPLE.cgColor
        btnAddChannel.styleButtonText(btnAddChannel, buttonName: "Add +", fontColor: .controlColor, alignment: .center, font: AVENIR_REGULAR, size: 13.0)
    }
    
    @objc private func userDataDidChange(_ notification: Notification) {
        if AuthService.instance.isLoggedIn {
            getChannels()
            lblUsername.stringValue = UserDataService.instance.name
        } else {
            tableChannels.reloadData()
            lblUsername.stringValue = ""
        }
    }
    
    private func getChannels() {
        MessageService.instance.findAllChannel { (success) in
            if success {
                self.tableChannels.reloadData()
                if MessageService.instance.channels.count > 0 {
                    self.tableChannels.selectRowIndexes(IndexSet(integer: self.selectedChannelIndex), byExtendingSelection: false)
                }
            }
        }
    }
}

// MARK: NSTableView Delegate | DataSource

extension ChannelVC: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return MessageService.instance.channels.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let identifier = NSUserInterfaceItemIdentifier(rawValue: "ChannelCell")
        let channel = MessageService.instance.channels[row]
        guard let cell = tableView.makeView(withIdentifier: identifier, owner: nil) as? ChannelCell else { fatalError("\(#function): ChannelCell was not found") }
        cell.configureCell(channel: channel, selectedChannel: selectedChannelIndex, row: row)
        return cell
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 30.0
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        selectedChannelIndex = tableChannels.selectedRow
        let channel = MessageService.instance.channels[selectedChannelIndex]
        selectedChannel = channel
        
        if MessageService.instance.unreadChannels.count > 0 {
            MessageService.instance.unreadChannels = MessageService.instance.unreadChannels.filter { $0 != channel.id }
        }
        
        chatVC?.update(with: channel)
        tableChannels.reloadData()
    }
}
