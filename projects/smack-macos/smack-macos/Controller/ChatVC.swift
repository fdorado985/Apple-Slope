//
//  ChatVC.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class ChatVC: NSViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblTitle: NSTextField!
    @IBOutlet weak var lblDescription: NSTextField!
    @IBOutlet weak var tableChats: NSTableView!
    @IBOutlet weak var lblUsersTyping: NSTextField!
    @IBOutlet weak var viewMessageOutline: NSView!
    @IBOutlet weak var txtMessage: NSTextField!
    @IBOutlet weak var btnSend: NSButton!

    // MARK: Properties
    
    var channel: Channel?
    var isTyping = false
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        tableChats.delegate = self
        tableChats.dataSource = self
        txtMessage.delegate = self
        setupView()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if UserDataService.instance.isMinimizing { return }
        setupView()
        
        SocketService.instance.getChatMessage { (newMessage) in
            if newMessage.channelId == self.channel?.id && AuthService.instance.isLoggedIn {
                MessageService.instance.messages.append(newMessage)
                self.tableChats.reloadData()
                self.tableChats.scrollRowToVisible(MessageService.instance.messages.count - 1)
            }
        }
        
        SocketService.instance.getTypingUsers { (typingUsers) in
            guard let channelId = self.channel?.id else { return }
            var names = ""
            var numberOfTypers = 0
            
            for (typingUser, channel) in typingUsers {
                if typingUser != UserDataService.instance.name && channel == channelId {
                    if names == "" {
                        names = typingUser
                    } else {
                        names = "\(names), \(typingUser)"
                    }
                    numberOfTypers += 1
                }
            }
            
            if numberOfTypers > 0 && AuthService.instance.isLoggedIn {
                var verb = "is"
                if numberOfTypers > 1 {
                    verb = "are"
                }
                
                self.lblUsersTyping.stringValue = "\(names) \(verb) typing a message..."
            } else {
                self.lblUsersTyping.stringValue = ""
            }
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnSendClicked(_ sender: NSButton) {
        if AuthService.instance.isLoggedIn {
            let message = txtMessage.stringValue
            guard let channelId = channel?.id, !message.isEmpty else { return }
            
            SocketService.instance.addMessage(messageBody: message, userId: UserDataService.instance.id, channelId: channelId) { (success) in
                if success {
                    self.txtMessage.stringValue = ""
                    self.txtMessage.resignFirstResponder()
                    SocketService.instance.socket.emit("stopType", UserDataService.instance.name, channelId)
                }
            }
        } else {
            let loginDict: [String : ModalType] = [USER_INFO_MODAL : ModalType.login]
            NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: loginDict)
        }
    }
    
    @IBAction func txtMessageEnterPressed(_ sender: NSTextField) {
        btnSend.performClick(nil)
    }
    
    // MARK: Functions
    
    private func setupView() {
        view.wantsLayer = true
        view.layer?.backgroundColor = CGColor.white
        
        viewMessageOutline.wantsLayer = true
        viewMessageOutline.layer?.backgroundColor = CGColor.white
        viewMessageOutline.layer?.borderColor = NSColor.darkGray.cgColor
        viewMessageOutline.layer?.borderWidth = 1
        viewMessageOutline.layer?.cornerRadius = 5
        
        btnSend.styleButtonText(btnSend, buttonName: "Send", fontColor: .darkGray, alignment: .center, font: AVENIR_REGULAR, size: 13.0)
    }
    
    @objc private func userDataDidChange(_ notification: Notification) {
        if AuthService.instance.isLoggedIn {
            if MessageService.instance.channels.count == 0 {
                lblTitle.stringValue = "Create a channel and get chatting!"
            }
        } else {
            lblTitle.stringValue = "Please Log In"
            lblDescription.stringValue = ""
            lblUsersTyping.stringValue = ""
            tableChats.reloadData()
        }
    }
    
    func update(with channel: Channel) {
        lblUsersTyping.stringValue = ""
        self.channel = channel
        let channelName = channel.name
        let channelDescription = channel.description
        
        lblDescription.stringValue = channelDescription
        lblTitle.stringValue = "#\(channelName)"
        getChats()
    }
    
    func getChats() {
        guard let channelId = channel?.id else { return }
        MessageService.instance.findAllMessages(for: channelId) { (success) in
            if success {
                self.tableChats.reloadData()
                self.tableChats.scrollRowToVisible(MessageService.instance.messages.count - 1)
            }
        }
    }
}

// MARK: NSTableView Delegate|Datasource

extension ChatVC: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return MessageService.instance.messages.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let identifier = NSUserInterfaceItemIdentifier(rawValue: "ChatCell")
        let message = MessageService.instance.messages[row]
        guard let cell = tableView.makeView(withIdentifier: identifier, owner: nil) as? ChatCell else { fatalError("\(#function): ChatCell was not found") }
        cell.configureCell(message: message)
        return cell
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 100.0
    }
}

// MARK: - NSTextField Delegate

extension ChatVC: NSTextFieldDelegate {
    
    func controlTextDidChange(_ obj: Notification) {
        guard let channelId = channel?.id else { return }
        if txtMessage.stringValue == "" {
            isTyping = false
            SocketService.instance.socket.emit("stopType", UserDataService.instance.name, channelId)
        } else {
            if !isTyping {
                SocketService.instance.socket.emit("startType", UserDataService.instance.name, channelId)
            }
            isTyping = true
        }
    }
}
