//
//  MainWC.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class MainWC: NSWindowController {

    // MARK: View cycle
    
    override func windowDidLoad() {
        super.windowDidLoad()
        setupView()
    }
    
    // MARK: Functions
    
    private func setupView() {
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
        window?.minSize = NSMakeSize(950, 600)
        window?.delegate = self
    }

}

extension MainWC: NSWindowDelegate {
    
    func windowWillMiniaturize(_ notification: Notification) {
        UserDataService.instance.isMinimizing = true
    }
    
    func windowDidDeminiaturize(_ notification: Notification) {
        UserDataService.instance.isMinimizing = false
    }
}
