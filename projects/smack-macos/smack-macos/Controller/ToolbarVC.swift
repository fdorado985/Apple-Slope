//
//  ToolbarVC.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

enum ModalType {
    case login
    case createAccount
    case profile
    case addChannel
}

class ToolbarVC: NSViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgLogin: NSImageView!
    @IBOutlet weak var lblLogin: NSTextField!
    @IBOutlet weak var stackLogin: NSStackView!
    
    // MARK: Properties
    
    private var modalBGView: ClickBlockingView!
    private var modalView: NSView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.setFrameSize(NSSize(width: 950, height: 600))
        NotificationCenter.default.addObserver(self, selector: #selector(ToolbarVC.presentModal(_:)), name: NOTIF_PRESENT_MODAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ToolbarVC.closeModalNotif(_:)), name: NOTIF_CLOSE_MODAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ToolbarVC.userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        setupView()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if UserDataService.instance.isMinimizing { return }
        
        setupView()
        if AuthService.instance.isLoggedIn {
            AuthService.instance.findUserByEmail { (success) in
                if success {
                    NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                }
            }
        }
    }
    
    // MARK: Functions
    
    private func setupView() {
        view.wantsLayer = true
        view.layer?.backgroundColor = CHAT_GREEN.cgColor
        
        stackLogin.gestureRecognizers.removeAll()
        let profilePage = NSClickGestureRecognizer(target: self, action: #selector(ToolbarVC.openProfilePage(_:)))
        stackLogin.addGestureRecognizer(profilePage)
    }
    
    @objc private func openProfilePage(_ recognizer: NSClickGestureRecognizer) {
        if AuthService.instance.isLoggedIn {
            let profileDict: [String : ModalType] = [USER_INFO_MODAL : ModalType.profile]
            NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: profileDict)
        } else {
            let loginDict: [String : ModalType] = [USER_INFO_MODAL : ModalType.login]
            NotificationCenter.default.post(name: NOTIF_PRESENT_MODAL, object: nil, userInfo: loginDict)
        }
    }
    
    @objc private func closeModalClick(_ recognizer: NSClickGestureRecognizer) {
        closeModal()
    }
    
    @objc private func presentModal(_ notification: Notification) {
        let modalWidth: CGFloat
        let modalHeight: CGFloat
        
        if modalBGView == nil {
            modalBGView = ClickBlockingView()
            modalBGView.wantsLayer = true
            modalBGView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(modalBGView, positioned: .above, relativeTo: stackLogin)
            
            // Programatically constraints
            let topConstraint = NSLayoutConstraint(
                item: modalBGView,
                attribute: .top,
                relatedBy: .equal,
                toItem: view,
                attribute: .top,
                multiplier: 1,
                constant: 50
            )
            let leftConstraint = NSLayoutConstraint(
                item: modalBGView,
                attribute: .left,
                relatedBy: .equal,
                toItem: view,
                attribute: .left,
                multiplier: 1,
                constant: 0
            )
            let rightConstraint = NSLayoutConstraint(
                item: modalBGView,
                attribute: .right,
                relatedBy: .equal,
                toItem: view,
                attribute: .right,
                multiplier: 1,
                constant: 0
            )
            let bottomConstraint = NSLayoutConstraint(
                item: modalBGView,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: view,
                attribute: .bottom,
                multiplier: 1,
                constant: 0
            )
            
            view.addConstraints([topConstraint, leftConstraint, rightConstraint, bottomConstraint])
            modalBGView.layer?.backgroundColor = CGColor.black
            modalBGView.alphaValue = 0.0
            
            let closeBackgroundClick = NSClickGestureRecognizer(target: self, action: #selector(ToolbarVC.closeModalClick(_:)))
            modalBGView.addGestureRecognizer(closeBackgroundClick)
        }
        
        // Instantiate XIB
        guard let userInfo = notification.userInfo, let modalType = userInfo[USER_INFO_MODAL] as? ModalType else { return }
        switch modalType {
        case .login:
            modalView = ModalLogin()
            modalWidth = 475
            modalHeight = 300
        case .createAccount:
            modalView = ModalCreateAccount()
            modalWidth = 475
            modalHeight = 300
        case .profile:
            modalView = ModalProfile()
            modalWidth = 475
            modalHeight = 300
        case .addChannel:
            modalView = ModalAddChannel()
            modalWidth = 475
            modalHeight = 300
        }
        
        modalView.wantsLayer = true
        modalView.translatesAutoresizingMaskIntoConstraints = false
        modalView.alphaValue = 0
        view.addSubview(modalView, positioned: .above, relativeTo: modalBGView)
        
        let horizontalConstraint = modalView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let verticalConstraint = modalView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        let widthConstraint = modalView.widthAnchor.constraint(equalToConstant: modalWidth)
        let heightConstraint = modalView.heightAnchor.constraint(equalToConstant: modalHeight)
        
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        NSAnimationContext.runAnimationGroup { (context) in
            context.duration = 0.5
            modalBGView.animator().alphaValue = 0.6
            modalView.animator().alphaValue = 1.0
            self.view.layoutSubtreeIfNeeded()
        }
    }
    
    private func closeModal(_ removeImmediately: Bool = false) {
        if removeImmediately {
            self.modalView.removeFromSuperview()
        } else {
            NSAnimationContext.runAnimationGroup({ (context) in
                context.duration = 0.5
                if modalBGView != nil { modalBGView.animator().alphaValue = 0.0 }
                if modalView != nil { modalView.animator().alphaValue = 0.0 }
                self.view.layoutSubtreeIfNeeded()
            }) {
                if self.modalBGView != nil {
                    self.modalBGView.removeFromSuperview()
                    self.modalBGView = nil
                }
                
                self.modalView.removeFromSuperview()
            }
        }
    }
    
    @objc private func closeModalNotif(_ notification: Notification) {
        if let removeImmediately = notification.userInfo?[USER_INFO_REMOVE_IMMEDIATELY] as? Bool {
            closeModal(removeImmediately)
        } else {
            closeModal()
        }
    }
    
    @objc private func userDataDidChange(_ notification: Notification) {
        if AuthService.instance.isLoggedIn {
            lblLogin.stringValue = UserDataService.instance.name
            imgLogin.wantsLayer = true
            imgLogin.layer?.cornerRadius = 5
            imgLogin.layer?.borderColor = NSColor.white.cgColor
            imgLogin.layer?.borderWidth = 1
            imgLogin.image = NSImage(named: UserDataService.instance.avatarName)
            imgLogin.layer?.backgroundColor = UserDataService.instance.returnCGColor(components: UserDataService.instance.avatarColor)
        } else {
            lblLogin.stringValue = "Login"
            imgLogin.wantsLayer = true
            imgLogin.layer?.borderColor = CGColor.clear
            imgLogin.layer?.borderWidth = 0
            imgLogin.image = NSImage(named: "profileDefault")
        }
    }
}

