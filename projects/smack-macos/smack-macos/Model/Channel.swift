//
//  Channel.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Channel {
    
    private(set) var name: String
    private(set) var description: String
    private(set) var id: String
}
