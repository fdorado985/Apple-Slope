//
//  Message.swift
//  smack-macos
//
//  Created by Juan Francisco Dorado Torres on 10/9/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Message {
    
    // MARK: Properties
    
    private(set) var id: String
    private(set) var message: String
    private(set) var username: String
    private(set) var channelId: String
    private(set) var avatar: String
    private(set) var avatarColor: String
    private(set) var userId: String
    private(set) var timestamp: String
}
