#  Smack

This is a little clone of the popular app **Slack** you are able to...
* Create Users
* Login with an existing account
* Create Channels
* Send Messages
* See for a user typing
* See unread channels
* All in real time

What do we use special here?
* Socket IO (Swift Client)

## Delete UserPreferences
* Go to `~/Library/Preferences/`
* Look for your `<BundleId>.plist` and delete it
* On your project directory type `killall cfprefsd` and press enter
* If you repeat the command before you will get `No matching processes belonging to you were found`, that means that it is already kill it.

## Demo
![profile_demo](.screenshots/smack_profile_demo.png)
![chat_demo](.screenshots/smack_chat_demo.png)
![add_channel_demo](.screenshots/smack_add_channel_demo.png)
