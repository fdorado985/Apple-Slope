//
//  ViewController.swift
//  hustle-mode
//
//  Created by Juan Francisco Dorado Torres on 26/07/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var imgDarkBlueBG: UIImageView!
  @IBOutlet weak var btnPower: UIButton!
  @IBOutlet weak var viewCloudHolder: UIView!
  @IBOutlet weak var imgRocket: UIImageView!
  @IBOutlet weak var lblHustle: UILabel!
  @IBOutlet weak var lblOn: UILabel!

  // MARK: - Properties

  var player: AVAudioPlayer!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()

    guard let path = Bundle.main.path(forResource: "hustle-on", ofType: "wav") else { return }
    let url = URL(fileURLWithPath: path)
    do {
      player = try AVAudioPlayer(contentsOf: url)
      player.prepareToPlay()
    } catch let error as NSError {
      print(error.description)
    }
  }

  // MARK: - IBActions

  @IBAction func btnPowerPressed(_ sender: UIButton) {
    viewCloudHolder.isHidden = false
    imgDarkBlueBG.isHidden = true
    btnPower.isHidden = true

    player.play()

    UIView.animate(withDuration: 2.3, animations: {
      self.imgRocket.frame = CGRect(x: 0, y: 140, width: 375, height: 402)
    }) { (finished) in
      self.lblHustle.isHidden = false
      self.lblOn.isHidden = false
    }
  }
}


