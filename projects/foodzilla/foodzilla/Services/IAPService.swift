//
//  IAPService.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import StoreKit

protocol IAPServiceDelegate {
    func iAPProductLoaded()
}

/*
 NOTE: StoreKit is for SKProductsRequestDelegate, your class must conform NSObject
 because of we are using SKReceiptRefreshRequest NSObject is not needed... because SKReceiptRefreshRequest already inheritance from NSObject
 */
class IAPService: SKReceiptRefreshRequest {
    
    // MARK: Singleton
    
    static let shared = IAPService()
    
    // MARK: Delegate
    
    var iAPDelegate: IAPServiceDelegate?
    
    // MARK: Properties
    
    private var productsIds = Set<String>()
    private var productRequest = SKProductsRequest()
    private var products = [SKProduct]()
    
    private var expirationDate: Date? {
        set { UserDefaults.standard.set(newValue, forKey: "expiration_date_key") }
        get { return UserDefaults.standard.value(forKey: "expiration_date_key") as? Date }
    }
    private var nonConsumablePurchaseWasMade: Bool {
        set { UserDefaults.standard.set(newValue, forKey: "nonConsumablePurchaseWasMade") }
        get { return UserDefaults.standard.bool(forKey: "nonConsumablePurchaseWasMade") }
    }
    
    // MARK: Public
    
    func loadProduts() {
        productIdToStringSet()
        requestProducts(for: productsIds)
    }
    
    func attemptPurchase(for productIndex: Product) {
        let product = products[productIndex.rawValue]
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    // MARK: Private
    
    private func productIdToStringSet() {
        let ids = [IAP_HIDE_ADS_ID, IAP_MEAL_ID, IAP_MEALTIME_MONTHLY_SUB]
        for id in ids {
            productsIds.insert(id)
        }
    }
    
    private func requestProducts(for ids: Set<String>) {
        productRequest.cancel()
        productRequest = SKProductsRequest(productIdentifiers: ids)
        productRequest.delegate = self
        productRequest.start()
    }
    
    private func sendNotification(for status: PurchaseStatus, with identifier: String?, boolean bool: Bool?) {
        switch status {
        case .purchased:
            NotificationCenter.default.post(name: IAPServicePurchaseNotification, object: identifier)
        case .restored:
            NotificationCenter.default.post(name: IAPServiceRestoreNotification, object: nil)
        case .failed:
            NotificationCenter.default.post(name: IAPServiceFailureNotification, object: nil)
        case .suscribed:
            NotificationCenter.default.post(name: IAPServiceSubscribedNotification, object: bool)
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        switch transaction.payment.productIdentifier {
        case IAP_MEAL_ID:
            sendNotification(for: .suscribed, with: transaction.payment.productIdentifier, boolean: nil)
            break
        case IAP_HIDE_ADS_ID:
            nonConsumablePurchaseWasMade = true
        case IAP_MEALTIME_MONTHLY_SUB:
            sendNotification(for: .suscribed, with: nil, boolean: true)
            nonConsumablePurchaseWasMade = true
        default:
            break
        }
    }
    
    private func uploadReceipt(completion: @escaping (Bool) -> Void) {
        guard let receiptUrl = Bundle.main.appStoreReceiptURL, let receipt = try? Data(contentsOf: receiptUrl).base64EncodedString() else {
            completion(false)
            return
        }
        
        let body = [
            "receipt-data" : receipt,
            "password" : "<Generated_Password>"
        ]
        
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        let url = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(false)
            } else if let responseData = data {
                let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! [String : Any]
                let newExpirationDate = self.expirationDate(from: json)
                self.expirationDate = newExpirationDate
                debugPrint("NEW EXPIRATION DATE: \(newExpirationDate ?? Date())")
                completion(true)
            }
        }
        task.resume()
    }
    
    private func expirationDate(from response: [String : Any]) -> Date? {
        if let receiptInfo: NSArray = response["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! [String : Any]
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            let expirationDate: Date = formatter.date(from: lastReceipt["expires_date"] as! String)!
            return expirationDate
        }
        return nil
    }
    
    private func isSubscriptionActive(_ completion: @escaping (Bool) -> Void) {
        reloadExpirationDate()
        let nowDate = Date()
        guard let expirationDate = expirationDate else { return }
        if nowDate.isLess(than: expirationDate) {
            completion(true)
        } else {
            completion(false)
        }
    }
    
    private func reloadExpirationDate() {
        expirationDate = UserDefaults.standard.value(forKey: "expiration_date_key") as? Date
    }
    
    // MARK: SKReceiptRefresh Request
    
    func requestDidFinish(_ request: SKRequest) {
        uploadReceipt { (success) in
            if success {
                self.isSubscriptionActive({ (active) in
                    if active {
                        self.nonConsumablePurchaseWasMade = true
                        self.sendNotification(for: .suscribed, with: nil, boolean: true)
                    } else {
                        self.nonConsumablePurchaseWasMade = false
                        self.sendNotification(for: .suscribed, with: nil, boolean: false)
                    }
                })
            } else {
                self.nonConsumablePurchaseWasMade = false
                self.sendNotification(for: .suscribed, with: nil, boolean: false)
            }
        }
    }
    
}

// MARK: - SKProductsRequest Delegate

extension IAPService: SKProductsRequestDelegate {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        if products.count == 0 {
            requestProducts(for: productsIds)
        } else {
            iAPDelegate?.iAPProductLoaded()
        }
    }
}

// MARK: - SKPaymentTransaction Observer

extension IAPService: SKPaymentTransactionObserver {
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                complete(transaction: transaction)
                SKPaymentQueue.default().finishTransaction(transaction)
                debugPrint("Purchased prodcut")
            case .restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                debugPrint("Purchases Restored")
                break
            case .failed:
                sendNotification(for: .failed, with: nil, boolean: nil)
                SKPaymentQueue.default().finishTransaction(transaction)
            case .deferred:
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .purchasing:
                debugPrint("Purchasing...")
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        sendNotification(for: .restored, with: nil, boolean: nil)
        nonConsumablePurchaseWasMade = true
    }
}
