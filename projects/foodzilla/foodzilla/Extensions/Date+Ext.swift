//
//  Date+Ext.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

extension Date {
    
    func isLess(than date: Date) -> Bool {
        if self.timeIntervalSince(date) < date.timeIntervalSinceNow {
            return true
        }
        
        return false
    }
}
