//
//  Item.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

struct Item {
    
    // MARK: Properties
    
    private(set) var image: UIImage
    private(set) var name: String
    private(set) var price: Double
    
    // MARK: Initialization
    
    init(image: UIImage, name: String, price: Double) {
        self.image = image
        self.name = name
        self.price = price
    }
}
