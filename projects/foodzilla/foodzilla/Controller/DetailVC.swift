//
//  DetailVC.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewUglyAd: UIView!
    @IBOutlet weak var btnBuyItem: UIButton!
    @IBOutlet weak var btnRemoveAds: UIButton!
    
    // MARK: Properties
    
    var item: Item?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        
        NotificationCenter.default.addObserver(self, selector: #selector(handlePurchase(_:)), name: IAPServicePurchaseNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleFailure(_:)), name: IAPServiceFailureNotification, object: nil)
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleAds()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: IBActions

    @IBAction func btnBuyItemTapped(_ sender: UIButton) {
        sender.isEnabled = false
        IAPService.shared.attemptPurchase(for: .meal)
    }
    
    @IBAction func btnRemoveAdsTapped(_ sender: UIButton) {
        IAPService.shared.attemptPurchase(for: .hideAds)
    }
    
    // MARK: Private
    
    private func setupView() {
        guard let item = item else { return }
        imgItem.image = item.image
        lblName.text = item.name
        lblPrice.text = "$\(item.price)"
        btnBuyItem.setTitle("Buy this item for $\(item.price)", for: .normal)
    }
    
    private func handleAds(isHidden: Bool = UserDefaults.standard.bool(forKey: "nonConsumablePurchaseWasMade")) {
        viewUglyAd.isHidden = isHidden
        btnRemoveAds.isHidden = isHidden
    }
    
    @objc private func handlePurchase(_ notification: Notification) {
        guard let productID = notification.object as? String else { return }
        switch productID {
        case IAP_MEAL_ID:
            btnBuyItem.isEnabled = true
            debugPrint("Meal Successfully Purchased")
        case IAP_HIDE_ADS_ID:
            handleAds(isHidden: true)
            debugPrint("Hide Ads Successfully Purchased")
        default:
            break
        }
    }
    
    @objc private func handleFailure(_ notification: Notification) {
        btnBuyItem.isEnabled = true
        debugPrint("Purchase has failed")
    }
}
