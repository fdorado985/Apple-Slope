//
//  StoreFrontVC.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class StoreFrontVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var collectionProducts: UICollectionView!
    @IBOutlet weak var lblSubscriptionStatus: UILabel!
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        IAPService.shared.iAPDelegate = self
        IAPService.shared.loadProduts()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showRestoreAlert), name: IAPServiceRestoreNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionStatusChanged(_:)), name: IAPServiceSubscribedNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: IBActions
    
    @IBAction func btnRestoreTapped(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Restore Purchases", message: "Do you want to restore any in-app purchases you've previously purchased?", preferredStyle: .actionSheet)
        let btnRestoreAction = UIAlertAction(title: "Restore", style: .default) { (action) in
            IAPService.shared.restorePurchases()
        }
        let btnCancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(btnRestoreAction)
        alert.addAction(btnCancelAction)
        present(alert, animated: true)
    }
    
    @IBAction func btnSubscribeTapped(_ sender: UIBarButtonItem) {
        IAPService.shared.attemptPurchase(for: .monthlySub)
    }
    
    // MARK: Functions
    
    @objc private func showRestoreAlert() {
        let alert = UIAlertController(title: "Restore Success!", message: "Your purchases were successfully restored.", preferredStyle: .alert)
        let btnAcceptAction = UIAlertAction(title: "Accept", style: .default)
        alert.addAction(btnAcceptAction)
        present(alert, animated: true)
    }
    
    @objc private func subscriptionStatusChanged(_ notification: Notification) {
        guard let status = notification.object as? Bool else { return }
        DispatchQueue.main.async {
            if status {
                self.view.backgroundColor = UIColor.darkGray
                self.collectionProducts.backgroundColor = UIColor.darkGray
                self.lblSubscriptionStatus.text = "SUBSCRIPTION ACTIVE"
                self.lblSubscriptionStatus.textColor = #colorLiteral(red: 0.9098039216, green: 0.6941176471, blue: 0.2745098039, alpha: 1)
            } else {
                self.view.backgroundColor = UIColor.white
                self.collectionProducts.backgroundColor = UIColor.white
                self.lblSubscriptionStatus.text = "SUBSCRIPTION EXPIRED"
                self.lblSubscriptionStatus.textColor = #colorLiteral(red: 0.8274509804, green: 0.3647058824, blue: 0.3137254902, alpha: 1)
            }
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailVC",
            let item = sender as? Item,
            let vc = segue.destination as? DetailVC {
            vc.item = item
        }
    }
}

extension StoreFrontVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return foodItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as? ItemCell else { fatalError("ItemCell was not found") }
        let item = foodItems[indexPath.item]
        cell.configureCell(item: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = foodItems[indexPath.item]
        performSegue(withIdentifier: "DetailVC", sender: item)
    }
}

extension StoreFrontVC: IAPServiceDelegate {
    
    func iAPProductLoaded() {
        print("iAP Products Loaded")
    }
    
    
}

