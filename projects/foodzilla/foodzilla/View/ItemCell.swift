//
//  ItemCell.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    // MARK: Functions
    
    func configureCell(item: Item) {
        imgItem.image = item.image
        lblName.text = item.name
        lblPrice.text = "$\(item.price)"
    }
}
