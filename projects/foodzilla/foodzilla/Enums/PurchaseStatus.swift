//
//  PurchaseStatus.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum PurchaseStatus {
    
    case purchased
    case restored
    case failed
    case suscribed
}
