//
//  Product.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum Product: Int {
    
    case hideAds = 0
    case meal = 1
    case monthlySub = 2
}
