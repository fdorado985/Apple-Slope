//
//  Constants.swift
//  foodzilla
//
//  Created by Juan Francisco Dorado Torres on 8/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

// MARK: - IAP Identifiers

let IAP_MEAL_ID = "com.iapcourse.meal"
let IAP_HIDE_ADS_ID = "com.iapcourse.hideads"
let IAP_MEALTIME_MONTHLY_SUB = "com.iapcourse.mealtimemonthly"

// MARK: - Notification Identifiers

let IAPServicePurchaseNotification = Notification.Name("IAPServicePurchaseNotification")
let IAPServiceRestoreNotification = Notification.Name("IAPServiceRestoreNotification")
let IAPServiceFailureNotification = Notification.Name("IAPServiceFailureNotification")
let IAPServiceSubscribedNotification = Notification.Name("IAPServiceSubscribedNotification")
