#  Foodzilla

This app is a simple restaurant app... where you'll see how the `Consumable` and `Non-Consumables` purchases are on it.

## Demo
![foodzilla_demo](screenshots/foodzilla_demo.gif)

## Steps To Configure Consumables
**NOTE: YOU WILL NEED AN APPLE PAID DEVELOPER ACCOUNT**

## Create New App on iTunes Connect
* Go to [iTunesConnect](itunesconnect.apple.com)
* Go to My Apps and press the plus button to add a new app.
* Then Fill the following form...
  * Select iOS
  * Name should be the same as your project
  * Select your primary language
  * Your bundle ID must be of your new app. (You maybe will need to Register your new Bundle ID on your portal before this.)
  * SKU (This could be also your Bundle ID)
  * And optional if you want to limit user access.

![new_app_itunes](screenshots/new_app_itunes.png)

* Then CREATE

## Add In-App Purchases
* Once created your new app go to `Features`
* Tap on `In-App Purchases` and press the `plus` button to select the new in-app purchase that you want
* You will see 4 of them...
  * Consumable
  * Non-Consumable
  * Auto-Renewable Subscription
  * Non-Renewing Subscription
* You will need to fill the form...
  * Reference Name: Just a name to you to reference your in-app purchase
  * Product ID: This is gonna be your identifier for your purchase example `com.iapcourse.meal`.
  * Pricing : You choose one from the table
  * Localization: In case you want to add more languages for your In-App Purchase
  * Display Name: Is gonna be the name that you will see after buy your In-App.
  * Description: A simple description of your In-App
  * The App Store Promotion : Although it is optional is good for the users to know about the consumables on your app.
* save your In-App

## Create Sandbox User
Your sandbox user is gonna be your user to test, this avoid to spend money.

* Go to `Users and Roles`
* Tap on Sandbox Users and press on plus button.
* Fill the information for your test account.
