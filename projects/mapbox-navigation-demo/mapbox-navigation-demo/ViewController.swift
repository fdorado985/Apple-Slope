//
//  ViewController.swift
//  mapbox-navigation-demo
//
//  Created by Juan Francisco Dorado Torres on 9/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections

class ViewController: UIViewController {
    
    // MARK: Properties
    
    private var mapView: NavigationMapView!
    private var btnNavigate: UIButton!
    private var directionsRoute: Route?
    private var disneylandCoordinate = CLLocationCoordinate2D(latitude: 33.8121, longitude: -117.9190)

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create and configure the MapView
        mapView = NavigationMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(mapView)
        
        // Show user Location
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        // Track the user
        mapView.setUserTrackingMode(.follow, animated: true)
        
        // Add Navigation Custom Button
        addButton()
    }
    
    // MARK: Private
    
    private func addButton() {
        btnNavigate = UIButton(frame: CGRect(x: (view.frame.width / 2) - 100, y: view.frame.height - 75, width: 200, height: 50))
        btnNavigate.backgroundColor = UIColor.white
        btnNavigate.setTitle("✨ NAVIGATE ✨", for: .normal)
        btnNavigate.setTitleColor(UIColor(red: 59/255, green: 178/255, blue: 208/255, alpha: 1), for: .normal)
        btnNavigate.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        btnNavigate.layer.cornerRadius = 25
        btnNavigate.layer.shadowOffset = CGSize(width: 0, height: 10)
        btnNavigate.layer.shadowColor = UIColor.darkGray.cgColor
        btnNavigate.layer.shadowRadius = 5
        btnNavigate.layer.shadowOpacity = 0.3
        btnNavigate.addTarget(self, action: #selector(btnNavigateTapped(_:)), for: .touchUpInside)
        view.addSubview(btnNavigate)
    }
    
    @objc private func btnNavigateTapped(_ sender: UIButton) {
        guard let userLocation = mapView.userLocation else { return }
        mapView.setUserTrackingMode(.none, animated: true)
        
        let annotation = MGLPointAnnotation()
        annotation.coordinate = disneylandCoordinate
        annotation.title = "Start Navigation"
        mapView.addAnnotation(annotation)
        
        calculateRoute(from: (userLocation.coordinate), to: disneylandCoordinate) { (route, error) in
            guard let route = route else {
                if let error = error {
                    debugPrint("ERROR CALCULATING ROUTE: \(error.localizedDescription)")
                    return
                }
                
                debugPrint("UNEXPECTED ERROR")
                return
            }
        }
    }
    
    private func calculateRoute(from originCoordinate: CLLocationCoordinate2D, to destinationCoordinate: CLLocationCoordinate2D, completion: @escaping (Route?, Error?) -> Void) {
        let origin = Waypoint(coordinate: originCoordinate, coordinateAccuracy: -1, name: "Start")
        let destination = Waypoint(coordinate: destinationCoordinate, coordinateAccuracy: -1, name: "Finish")
        
        let options = NavigationRouteOptions(waypoints: [origin, destination], profileIdentifier: MBDirectionsProfileIdentifier.automobileAvoidingTraffic)
        
        _ = Directions.shared.calculate(options, completionHandler: { (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            
            // draw the line
            self.drawRoute(route: self.directionsRoute)
            
            // Zoom the coordination
            let coordinateBounds = MGLCoordinateBounds(sw: destinationCoordinate, ne: originCoordinate)
            let insets = UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50)
            let routeCam = self.mapView.cameraThatFitsCoordinateBounds(coordinateBounds, edgePadding: insets)
            self.mapView.setCamera(routeCam, animated: true)
        })
    }
    
    private func drawRoute(route: Route?) {
        guard let route = route, let routeCoordinates = route.coordinates, routeCoordinates.count > 0 else { return }
        let polyline = MGLPolylineFeature(coordinates: routeCoordinates, count: route.coordinateCount)
        
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: UIColor(red: 59/255, green:178/255, blue:208/255, alpha:1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 4)
            
            mapView.style?.addSource(source)
            mapView.style?.addLayer(lineStyle)
        }
    }
}

extension ViewController: MGLMapViewDelegate {
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        guard let route = self.directionsRoute else { return }
        let navigationVC = NavigationViewController(for: route)
        present(navigationVC, animated: true)
    }
}

