# Splitter

This app use the `Master-Detail View Controller` to work with Split Views, using Dummy Data.

## Demo
![splitter_vertical_demo](screenshots/splitter_vertical_demo.gif)

![splitter_horizontal_demo](screenshots/splitter_horizontal_demo.gif) 
