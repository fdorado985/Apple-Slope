//
//  DetailViewController.swift
//  splitter
//
//  Created by Juan Francisco Dorado Torres on 19/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ImagePresentationVC: UIViewController {

  @IBOutlet weak var imgItem: UIImageView!

  var image: UIImage?

  override func viewDidLoad() {
    super.viewDidLoad()
    imgItem.image = image
  }
}

