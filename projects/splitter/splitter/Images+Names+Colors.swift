//
//  Images+Names+Colors.swift
//  splitter
//
//  Created by Juan Francisco Dorado Torres on 19/03/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

// MARK: - Images

let camera = UIImage(named: "1")!
let city = UIImage(named: "2")!
let buffalo = UIImage(named: "3")!
let bouqet = UIImage(named: "4")!
let stand = UIImage(named: "5")!
let urban = UIImage(named: "6")!

let imgArray = [camera, city, buffalo, bouqet, stand, urban]

// MARK: - Names

let namesArray = ["CAMERA", "CITY", "ANIMALS", "FLOWERS", "STANDS", "URBAN"]

// MARK: - Colors

let colorsArray = [UIColor.blue, UIColor.brown, UIColor.purple, UIColor.orange, UIColor.green, UIColor.magenta]

