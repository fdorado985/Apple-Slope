//
//  ViewController.swift
//  daily-dose
//
//  Created by Juan Francisco Dorado Torres on 5/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HomeVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var bannerView: GADBannerView!
  @IBOutlet weak var btnRemoveAds: UIButton!
  @IBOutlet weak var constraintAdHolderHeight: NSLayoutConstraint!

  // MARK: - Properties

  var articles = [Article]()
  
  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    setupAds()
    articles = DataService.instance.getArticles()
  }

  // MARK: - IBActions

  @IBAction func restorePurchasesPressed(_ sender: UIButton) {
    // Make the purchase
    /*PurchaseManager.instance.restorePurchases { (success) in
     hideAdBanner()
     }*/

    if self.constraintAdHolderHeight.constant != 0 {
      hideAdBanner()
    }
  }

  @IBAction func removeAdsPressed(_ sender: UIButton) {
    // Restore the purchase
    /*PurchaseManager.instance.purchaseRemoveAds { (success) in
     hideAdBanner()
     }*/

    if self.constraintAdHolderHeight.constant != 0 {
      hideAdBanner()
    }
  }

  func setupAds() {
    if UserDefaults.standard.bool(forKey: PurchaseManager.instance.IAP_REMOVE_ADS) {
      constraintAdHolderHeight.constant = 0
    } else {
      bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
      bannerView.rootViewController = self
      bannerView.load(GADRequest())
    }
  }

  func hideAdBanner() {
    UIView.animate(withDuration: 1) {
      self.constraintAdHolderHeight.constant = 0

      self.view.setNeedsLayout()
      self.view.setNeedsUpdateConstraints()
      self.view.layoutIfNeeded()
    }
  }
}

// MARK: - UITableView

extension HomeVC: UITableViewDelegate, UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return articles.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as? ArticleCell else { return UITableViewCell() }
    let article = articles[indexPath.row]
    cell.configureCell(article)
    return cell
  }
}

