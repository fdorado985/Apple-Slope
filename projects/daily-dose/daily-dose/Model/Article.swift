//
//  Article.swift
//  daily-dose
//
//  Created by Juan Francisco Dorado Torres on 5/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Article {

  public private(set) var title: String
  public private(set) var isPopular: Bool
  public private(set) var isSubscription: Bool

  init(title: String, isPopular: Bool = false, isSubscription: Bool = false) {
    self.title = title
    self.isPopular = isPopular
    self.isSubscription = isSubscription
  }
}
