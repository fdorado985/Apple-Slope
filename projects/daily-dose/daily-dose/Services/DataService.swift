//
//  DataService.swift
//  daily-dose
//
//  Created by Juan Francisco Dorado Torres on 5/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class DataService {

  static let instance = DataService()

  func getArticles() -> [Article] {
    let item1 = Article(title: "Creatives Listen Up", isPopular: true)
    let item2 = Article(title: "Never Leave Home Without It", isSubscription: true)
    let item3 = Article(title: "Another Cool Article")
    let item4 = Article(title: "The best course")

    return [item1, item2, item3, item4]
  }
}
