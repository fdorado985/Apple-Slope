//
//  PurchaseManager.swift
//  daily-dose
//
//  Created by Juan Francisco Dorado Torres on 5/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

typealias handler = (_ success: Bool) -> ()

import Foundation
import StoreKit

class PurchaseManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {

  static let instance = PurchaseManager()

  let IAP_REMOVE_ADS = "com.dorado.daily-dose.remove.ads"

  var productsRequest: SKProductsRequest!
  var products = [SKProduct]()
  var transactionComplete: handler?

  func fetchProducts() {
    guard let productIds = NSSet(object: IAP_REMOVE_ADS) as? Set<String> else { return }
    productsRequest = SKProductsRequest(productIdentifiers: productIds)
    productsRequest.delegate = self
    productsRequest.start()
  }

  func purchaseRemoveAds(_ handler: @escaping handler) {
    if SKPaymentQueue.canMakePayments() && !products.isEmpty {
      transactionComplete = handler
      let removeAdsProduct = products[0]
      let payment = SKPayment(product: removeAdsProduct)
      SKPaymentQueue.default().add(self)
      SKPaymentQueue.default().add(payment)
    } else {
      handler(false)
    }
  }

  func restorePurchases(_ handler: @escaping handler) {
    if SKPaymentQueue.canMakePayments() {
      transactionComplete = handler
      SKPaymentQueue.default().add(self)
      SKPaymentQueue.default().restoreCompletedTransactions()
    } else {
      handler(false)
    }
  }

  func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
    if !response.products.isEmpty {
      products = response.products
    }
  }

  func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchased:
        SKPaymentQueue.default().finishTransaction(transaction)
        if transaction.payment.productIdentifier == IAP_REMOVE_ADS {
          UserDefaults.standard.set(true, forKey: IAP_REMOVE_ADS)
          transactionComplete?(true)
        }
      case .failed:
        SKPaymentQueue.default().finishTransaction(transaction)
        transactionComplete?(false)
      case .restored:
        SKPaymentQueue.default().finishTransaction(transaction)
        if transaction.payment.productIdentifier == IAP_REMOVE_ADS {
          UserDefaults.standard.set(true, forKey: IAP_REMOVE_ADS)
        }
        transactionComplete?(true)
      default: break
      }
    }
  }
}
