//
//  ArticleCell.swift
//  daily-dose
//
//  Created by Juan Francisco Dorado Torres on 5/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {

  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblSubscriptionOnly: UILabel!
  @IBOutlet weak var imgPopular: UIImageView!

  func configureCell(_ article: Article) {
    lblTitle.text = article.title
    lblSubscriptionOnly.isHidden = !article.isSubscription
    imgPopular.isHidden = !article.isPopular
  }
}
