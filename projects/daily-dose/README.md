#  Daily Dose

With this app we will learn about how to use In-App Purchases, and add some Google Ads to work with.

## Requirments
- Enroll to Apple Developers ($99 DLLS)
- iTunes Connect

## Steps
### Register The App
- Log In to [Apple Developer](www.developer.apple.com)
- Go to `Certificates, Identifiers & Profiles`
- Go to `App ID's` and create a new `App ID`
- Type the Name on the `App ID Description`
- IMPORTANT: In the `App ID Suffix` type your `Bundle ID`, you will find this on your project
- In the `App Services` part `In-App Services` is already selected by default
- Click to `Continue` then click on `Register`

### Manage The App for In-App Purchases
- Log in to [iTunes Connect](www.itunesconnect.apple.com)
- Fill the form of  `Agreements, Tax, and Banking` in case you haven't done before. *(IMPORTANT)*
- Go to My Apps *(Remember you must be a pay developer)*
- Click on the plus `+` button and add new App
- Fill the form, the platform for this, Name, Language, Bundle ID (This is which you have created on the part before) and click `Create`

### Create User Test for InApp-Purchases
- On [iTunes Connect](www.itunesconnect.apple.com)
- Click on `Users and Roles`
- Click on `Sandbox Users`
- Tap on the little plus `+` button and create a new one

### Turn On In-App Purchases
*IMPORTANT:* Remember to turn on `In-App Purchases` on your app project
- Go to project
- Tap `Capabilites`
- Turn on `In-App Purchases`

### Create In-App Purchases
- On [iTunes Connect](www.itunesconnect.apple.com)
- Go to My Apps
- Select your app
- Go to Features and tap on Plus `+` button
- Select the type of `In-App Purchase` that you will create

## Demo
![iPhone_Demo](screenshots/dailydose_demo.gif)
