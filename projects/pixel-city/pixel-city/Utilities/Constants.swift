//
//  Constants.swift
//  pixel-city
//
//  Created by Juan Francisco Dorado Torres on 22/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

let API_KEY = "3ca9d0a9eee464aaa20ee3ef8947b8e0"

func flickrURL(forApiKey key: String, withAnnotation annotation: DroppablePin, andNumberOfImages number: Int) -> String {
  return "https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=\(key)&lat=\(annotation.coordinate.latitude)lon=\(annotation.coordinate.longitude)&radius=1&radius_units=km&per_page=\(number)&format=json&nojsoncallback=1"
}
