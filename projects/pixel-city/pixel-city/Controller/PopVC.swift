//
//  PopVC.swift
//  pixel-city
//
//  Created by Juan Francisco Dorado Torres on 23/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PopVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var popImageView: UIImageView!

  // MARK: - Properties

  var passedImage: UIImage!

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    popImageView.image = passedImage
    addDoubleTap()
  }

  // MARK: - Functions

  func initData(forImage image: UIImage) {
    self.passedImage = image
  }

  @objc private func screenWasDoubleTapped() {
    self.dismiss(animated: true, completion: nil)
  }
}

extension PopVC: UIGestureRecognizerDelegate {

  func addDoubleTap() {
    let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTapped))
    doubleTap.numberOfTapsRequired = 2
    doubleTap.delegate = self
    view.addGestureRecognizer(doubleTap)
  }
}
