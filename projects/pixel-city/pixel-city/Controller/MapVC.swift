//
//  ViewController.swift
//  pixel-city
//
//  Created by Juan Francisco Dorado Torres on 20/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import AlamofireImage

enum AnimateDirection {
  case up
  case down
}

class MapVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var pullUpView: UIView!

  // MARK: - Properties

  var locationManager = CLLocationManager()
  let authorizationStatus = CLLocationManager.authorizationStatus()
  let regionRadius: Double = 1000
  var screenSize = UIScreen.main.bounds
  var spinner: UIActivityIndicatorView?
  var progressLbl: UILabel?
  var collectionView: UICollectionView?
  var flowLayout = UICollectionViewFlowLayout()
  var imagesURL = [String]()
  var images = [UIImage]()

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
    mapView.delegate = self
    locationManager.delegate = self
    configureLocationServices()
    addDoubleTap()

    collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
    guard let collectionView = collectionView else { return }
    collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: "PhotoCell")
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    registerForPreviewing(with: self, sourceView: collectionView)

    pullUpView.addSubview(collectionView)
  }

  // MARK: - IBActions

  @IBAction func btCenterMapPressed(_ sender: UIButton) {
    if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
      centerMapOnUserLocation()
    }
  }

  // MARK: - Functions

  func addSpinner() {
    spinner = UIActivityIndicatorView()
    guard let spinner = spinner else { return }
    spinner.center = CGPoint(x: (screenSize.width / 2) - (spinner.frame.width / 2), y: 150)
    spinner.activityIndicatorViewStyle = .whiteLarge
    spinner.color = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    spinner.startAnimating()
    guard let collectionView = collectionView else { return }
    collectionView.addSubview(spinner)
  }

  func removeSpinner() {
    if spinner != nil {
      spinner?.removeFromSuperview()
    }
  }

  func addProgressLbl() {
    progressLbl = UILabel()
    guard let progressLbl = progressLbl else { return }
    progressLbl.frame = CGRect(x: (screenSize.width / 2) - 100, y: 175, width: 200, height: 40)
    progressLbl.font = UIFont(name: "Avenir Next", size: 14)
    progressLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    progressLbl.textAlignment = .center
    guard let collectionView = collectionView else { return }
    collectionView.addSubview(progressLbl)
  }

  func removeProgressLbl() {
    if progressLbl != nil {
      progressLbl?.removeFromSuperview()
    }
  }

  func retreiveURLS(for annotation: DroppablePin, handler: @escaping (_ status: Bool) -> ()) {
    Alamofire.request(flickrURL(forApiKey: API_KEY, withAnnotation: annotation, andNumberOfImages: 40)).responseJSON { (response) in
      guard let json = response.result.value as? [String : Any],
        let photosDict = json["photos"] as? [String : Any],
        let photosArray = photosDict["photo"] as? [[String : Any]] else { return }

      for photo in photosArray {
        guard let farm = photo["farm"],
          let server = photo["server"],
          let id = photo["id"],
          let secret = photo["secret"] else { return }
        let postURL = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_h_d.jpg"
        self.imagesURL.append(postURL)
      }
      handler(true)
    }
  }

  func retreiveImages(handler: @escaping(_ status: Bool) -> ()) {
    for url in imagesURL {
      Alamofire.request(url).responseImage(completionHandler: { (response) in
        guard let image = response.result.value else { return }
        self.images.append(image)
        self.progressLbl?.text = "\(self.images.count)/40 IMAGES DOWNLOADED"
        if self.images.count == self.imagesURL.count {
          handler(true)
        }
      })
    }
  }

  func cancelAllSessions() {
    Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
      sessionDataTask.forEach({ $0.cancel() })
      downloadData.forEach({ $0.cancel() })
    }
  }
}

// MARK: - MKMapViewDelegate

extension MapVC: MKMapViewDelegate {

  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard !(annotation is MKUserLocation) else { return nil }
    let pinAnnotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "droppablePin")
    pinAnnotation.pinTintColor = #colorLiteral(red: 0.9771530032, green: 0.7062081099, blue: 0.1748393774, alpha: 1)
    pinAnnotation.animatesDrop = true
    return pinAnnotation
  }

  func centerMapOnUserLocation() {
    guard let coordinate = locationManager.location?.coordinate else { return }
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)
  }

  @objc func dropPin(_ sender: UITapGestureRecognizer) {
    removePin()
    removeSpinner()
    removeProgressLbl()
    cancelAllSessions()

    imagesURL = []
    images = []
    collectionView?.reloadData()

    animateView(.up)
    addSwipe()
    addSpinner()
    addProgressLbl()

    let touchPoint = sender.location(in: mapView)
    let touchCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
    let annotation = DroppablePin(touchCoordinate, identifier: "droppablePin")
    mapView.addAnnotation(annotation)
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(touchCoordinate, regionRadius * 2.0, regionRadius * 2.0)
    mapView.setRegion(coordinateRegion, animated: true)

    retreiveURLS(for: annotation) { (finish) in
      if finish {
        self.retreiveImages(handler: { (finish) in
          self.removeSpinner()
          self.removeProgressLbl()
          self.collectionView?.reloadData()
        })
      }
    }
  }

  func removePin() {
    for annotation in mapView.annotations {
      mapView.removeAnnotation(annotation)
    }
  }

  func animateView(_ animateDirection: AnimateDirection) {
    if animateDirection == .up {
      pullUpViewHeightConstraint.constant = 300
    } else if animateDirection == .down {
      cancelAllSessions()
      pullUpViewHeightConstraint.constant = 0
    }

    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }

  @objc func swipeDown() {
    animateView(.down)
  }
}

// MARK: - CLLocationManagerDelegate

extension MapVC: CLLocationManagerDelegate {

  func configureLocationServices() {
    guard authorizationStatus == .notDetermined else { return }
    locationManager.requestAlwaysAuthorization()
  }

  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    centerMapOnUserLocation()
  }
}

// MARK: - UIGestureRecognizerDelegate

extension MapVC: UIGestureRecognizerDelegate {

  func addDoubleTap() {
    let doubleTap = UITapGestureRecognizer(target: self, action: #selector(dropPin(_:)))
    doubleTap.numberOfTapsRequired = 2
    doubleTap.delegate = self
    mapView.addGestureRecognizer(doubleTap)
  }

  func addSwipe() {
    let swipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeDown))
    swipe.direction = .down
    pullUpView.addGestureRecognizer(swipe)
  }
}

// MARK: - UICollectionViewDelegate

extension MapVC: UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopVC else { return }
    popVC.initData(forImage: images[indexPath.row])
    present(popVC, animated: true, completion: nil)
  }
}

// MARK: - UICollectionViewDataSource

extension MapVC: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as? PhotoCell else { return UICollectionViewCell() }
    let imageView = UIImageView(image: images[indexPath.row])
    cell.addSubview(imageView)
    return cell
  }
}

// MARK: - UIViewControllerPreviewingDelegate

extension MapVC: UIViewControllerPreviewingDelegate {

  func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
    guard let indexPath = collectionView?.indexPathForItem(at: location),
      let cell = collectionView?.cellForItem(at: indexPath),
      let popVC = storyboard?.instantiateViewController(withIdentifier: "PopVC") as? PopVC else { return nil }
    popVC.initData(forImage: images[indexPath.row])
    previewingContext.sourceRect = cell.contentView.frame
    return popVC
  }

  func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    show(viewControllerToCommit, sender: self)
  }
}
