//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Juan Francisco Dorado Torres on 21/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {

  override init(frame: CGRect) {
    super.init(frame: frame)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
