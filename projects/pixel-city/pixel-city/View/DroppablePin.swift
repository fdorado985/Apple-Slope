//
//  DroppablePin.swift
//  pixel-city
//
//  Created by Juan Francisco Dorado Torres on 20/09/17.
//  Copyright © 2017 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import MapKit

class DroppablePin: NSObject, MKAnnotation {

  dynamic var coordinate: CLLocationCoordinate2D
  var identifier: String

  init(_ coordinate: CLLocationCoordinate2D, identifier: String) {
    self.coordinate = coordinate
    self.identifier = identifier
    super.init()
  }
}
