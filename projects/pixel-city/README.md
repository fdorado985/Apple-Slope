# Pixel-City

On this app, we will be using the `MapKit` from Apple, this app is using `Alamofire` to use a service to bring the photos from a specific location that you will get at the time you pick on the map.

## Demo
![Demo-01](screenshots/pixel-city-demo-01.gif)

![Demo-02](screenshots/pixel-city-demo-02.gif)
