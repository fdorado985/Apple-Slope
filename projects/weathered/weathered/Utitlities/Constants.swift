//
//  Constants.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/6/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

// MARK: URLS

let API_HOMEPAGE = "https://openweathermap.org/"
let API_KEY = "&appid=4061f00e8f2017bcdd8392f747f0cb4d"
let API_UNITS_METRIC = "&units=metric"
let API_URL_CURRENT_WEATHER = "https://api.openweathermap.org/data/2.5/weather?lat=\(LAT)&lon=\(LONG)\(API_KEY)\(API_UNITS_METRIC)"
let API_URL_FORECAST = "https://api.openweathermap.org/data/2.5/forecast/daily?lat=\(LAT)&lon=\(LONG)&cnt=8\(API_KEY)\(API_UNITS_METRIC)"

// MARK: Handlers

typealias DownloadComplete = (_ success: Bool) -> Void

// MARK: Notifications

let NOTIF_DOWNLOAD_COMPLETE = NSNotification.Name("dataDownloaded")

// MARK: Location

let LAT = Location.instance.latitude
let LONG = Location.instance.longitude
