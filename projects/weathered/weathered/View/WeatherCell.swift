//
//  WeatherCell.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/5/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class WeatherCell: NSCollectionViewItem {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var lblDay: NSTextField!
    @IBOutlet private weak var lblLowTemp: NSTextField!
    @IBOutlet private weak var lblHighTemp: NSTextField!
    @IBOutlet private weak var imgWeather: NSImageView!
    
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = CGColor(red: 0.69, green: 0.85, blue: 0.99, alpha: 0.5)
        view.layer?.cornerRadius = 5
    }
    
    // MARK: Functions
    
    func configureCell(forecast: Forecast) {
        if let weather = forecast.weather.first {
            imgWeather.image = NSImage(named: weather.main)
        }
        
        lblDay.stringValue = forecast.date
        lblLowTemp.stringValue = "\(forecast.temperature.min)°"
        lblHighTemp.stringValue = "\(forecast.temperature.max)°"
    }
}
