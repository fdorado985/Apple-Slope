//
//  WeatherVC.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/5/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa

class WeatherVC: NSViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblDate: NSTextField!
    @IBOutlet weak var lblTemp: NSTextField!
    @IBOutlet weak var lblLocation: NSTextField!
    @IBOutlet weak var imgWeather: NSImageView!
    @IBOutlet weak var lblWeatherCondition: NSTextField!
    @IBOutlet weak var collectionView: NSCollectionView!
    @IBOutlet weak var btnOpenWeatherMap: NSButton!
    @IBOutlet weak var btnQuit: NSButton!
    
    // MARK: View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        NotificationCenter.default.addObserver(self, selector: #selector(dataDownloaded(_:)), name: NOTIF_DOWNLOAD_COMPLETE, object: nil)
        view.layer?.backgroundColor = CGColor(red: 0.29, green: 0.72, blue: 0.98, alpha: 1.0)
        updateUI()
        btnQuit.styleButtonText(button: btnQuit, buttonName: "Quit", fontColor: .darkGray, alignment: .center, font: "Avenir Next", size: 11)
        btnOpenWeatherMap.styleButtonText(button: btnOpenWeatherMap, buttonName: "Powered by OpenWeatherMap", fontColor: .darkGray, alignment: .center, font: "Avenir Next", size: 11)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnOpenWeatherMapClicked(_ sender: Any) {
        guard let url = URL(string: API_HOMEPAGE) else { return }
        NSWorkspace.shared.open(url)
    }
    
    @IBAction func btnQuitClicked(_ sender: Any) {
        NSApplication.shared.terminate(nil)
    }
    
    // MARK: Functions
    
    private func updateUI() {
        guard let baseWeather = WeatherService.shared.baseWeather else { return }
        lblDate.stringValue = "\(baseWeather.date)"
        lblTemp.stringValue = "\(baseWeather.temperature.temperature)°"
        lblLocation.stringValue = baseWeather.city
        
        guard let weatherCondition = baseWeather.weather.first?.main else { return }
        lblWeatherCondition.stringValue = weatherCondition
        imgWeather.image = NSImage(named: weatherCondition)
        
        collectionView.reloadData()
    }
    
    @objc private func dataDownloaded(_ notification: Notification) {
        updateUI()
    }
}

// MARK: CollectionView Delegate|Datasource|FlowLayout

extension WeatherVC: NSCollectionViewDelegate, NSCollectionViewDataSource, NSCollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return WeatherService.shared.baseForecast?.daysList.count ?? 0
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let userInterfaceItemIdentifier = NSUserInterfaceItemIdentifier.init("WeatherCell")
        guard let forecastCell = collectionView.makeItem(withIdentifier: userInterfaceItemIdentifier, for: indexPath) as? WeatherCell else { fatalError("\(#function): WeatherCell not found") }
        
        if let forecast = WeatherService.shared.baseForecast?.daysList[indexPath.item] {
            forecastCell.configureCell(forecast: forecast)
        }
        
        return forecastCell
    }
    
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        return NSSize(width: 125, height: 125)
    }
}

