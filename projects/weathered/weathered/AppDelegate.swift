//
//  AppDelegate.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/5/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Cocoa
import CoreLocation

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    // MARK: Properties
    
    private let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation!
    
    // MARK: App Delegate Cycle

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        statusItem.button?.title = "--°"
        statusItem.button?.action = #selector(AppDelegate.displayPopUp(_:))
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 1000
        locationManager.startUpdatingLocation()
        
        let updateWeatherData = Timer.scheduledTimer(timeInterval: 60 * 15, target: self, selector: #selector(downloadWeatherData), userInfo: nil, repeats: true)
        updateWeatherData.tolerance = 60
        downloadWeatherData()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    // MARK: Functions
    
    @objc private func displayPopUp(_ sender: Any?) {
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateController(withIdentifier: "WeatherVC") as? NSViewController else { return }
        
        // Create the popover to show
        let popoverView = NSPopover()
        popoverView.contentViewController = vc
        popoverView.behavior = .transient
        
        guard let statusItemBtn = statusItem.button else { return }
        popoverView.show(relativeTo: statusItemBtn.bounds, of: statusItemBtn, preferredEdge: .maxY)
    }
    
    @objc private func downloadWeatherData() {
        WeatherService.shared.downloadCurrentWeather { (success) in
            if success, let baseWeather = WeatherService.shared.baseWeather {
                DispatchQueue.main.async {
                    self.statusItem.button?.title = "\(baseWeather.temperature.temperature)°"
                }
            }
            
            WeatherService.shared.download16DayWeatherForecast { (success) in
                NotificationCenter.default.post(name: NOTIF_DOWNLOAD_COMPLETE, object: nil)
                self.locationManager.stopUpdatingLocation()
            }
        }
    }
}

extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[locations.count - 1]
        Location.instance.latitude = currentLocation.coordinate.latitude
        Location.instance.longitude = currentLocation.coordinate.longitude
        downloadWeatherData()
    }
}
