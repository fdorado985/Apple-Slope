//
//  BaseWeather.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/6/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct BaseWeather: Codable {
    
    // Properties
    
    private(set) var city: String
    private let _date: Double
    private(set) var weather: [Weather]
    private(set) var temperature: Temperature
    
    var date: String {
        let myDate = Date(timeIntervalSince1970: _date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: myDate)
        return "Today, \(currentDate)"
    }
    
    enum CodingKeys: String, CodingKey {
        case city = "name"
        case _date = "dt"
        case weather
        case temperature = "main"
    }
}
