//
//  Temperature.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/6/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Temperature: Codable {
    
    // MARK: Properties
    
    private(set) var temperature: Double
    private(set) var pressure: Double
    private(set) var humidity: Double
    private(set) var temp_min: Double
    private(set) var temp_max: Double
    
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case pressure
        case humidity
        case temp_min
        case temp_max
    }
}
