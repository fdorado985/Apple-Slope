//
//  Forecast.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Forecast: Codable {
    
    // MARK: Properties
    
    private let _date: Double
    private(set) var weather: [Weather]
    private(set) var temperature: ForecastTemperature
    
    var date: String {
        let myDate = Date(timeIntervalSince1970: _date)
        return myDate.dayOfTheWeek()
    }
    
    enum CodingKeys: String, CodingKey {
        case _date = "dt"
        case weather
        case temperature = "temp"
    }
}
