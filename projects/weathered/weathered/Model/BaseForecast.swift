//
//  BaseForecast.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct BaseForecast: Codable {
    
    // MARK: Properties
    
    private let list: [Forecast]
    
    var daysList: [Forecast] {
        var temp = list
        if temp.count > 0 {
          temp.remove(at: 0)
        }
        return temp
    }
    
    enum CodingKeys: String, CodingKey {
        case list
    }
}
