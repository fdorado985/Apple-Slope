//
//  Location.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Location {
    
    // MARK: Singleton
    
    static let instance = Location()
    
    // MARK: Properties
    
    private(set) var _latitude: Double = 0.0
    private(set) var _longitude: Double = 0.0
    
    var latitude: Double {
        get { return _latitude }
        set { _latitude = newValue }
    }
    
    var longitude: Double {
        get { return _longitude }
        set { _longitude = newValue }
    }
}
