//
//  ForecastTemperature.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct ForecastTemperature: Codable {
    
    // MARK: Properties
    
    private(set) var day: Double
    private(set) var min: Double
    private(set) var max: Double
    private(set) var night: Double
    private(set) var evening: Double
    private(set) var morning: Double
    
    enum CodingKeys: String, CodingKey {
        case day
        case min
        case max
        case night
        case evening = "eve"
        case morning = "morn"
    }
}
