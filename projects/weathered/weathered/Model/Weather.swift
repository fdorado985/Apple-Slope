//
//  Weather.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/6/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Weather: Codable {
    
    // MARK: Properties
    
    private(set) var description: String
    private(set) var icon: String
    private(set) var id: Int
    private(set) var main: String

    enum CodingKeys: String, CodingKey {
        case description
        case icon
        case id
        case main
    }
}
