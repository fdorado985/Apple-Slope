//
//  WeatherService.swift
//  weathered
//
//  Created by Juan Francisco Dorado Torres on 10/6/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class WeatherService {
    
    // MARK: Singleton
    
    static let shared = WeatherService()
    
    // MARK: Properties
    
    private(set) var baseWeather: BaseWeather?
    private(set) var baseForecast: BaseForecast?
    
    // MARK: Functions
    
    func downloadCurrentWeather(handler: @escaping DownloadComplete) {
        guard let url = URL(string: API_URL_CURRENT_WEATHER) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                debugPrint("\(#function) ERROR: \(error.localizedDescription)")
                handler(false)
                return
            }
            
            guard let data = data else {
                handler(false)
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                self.baseWeather = try jsonDecoder.decode(BaseWeather.self, from: data)
                handler(true)
            } catch let err {
                debugPrint("\(#function) ERROR: \(err.localizedDescription)")
                handler(false)
                return
            }
        }.resume()
    }
    
    func download16DayWeatherForecast(handler: @escaping DownloadComplete) {
        guard let url = URL(string: API_URL_FORECAST) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                debugPrint("\(#function) ERROR: \(error.localizedDescription)")
                handler(false)
                return
            }
            
            guard let data = data else {
                handler(false)
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                self.baseForecast = try jsonDecoder.decode(BaseForecast.self, from: data)
                handler(true)
            } catch let err {
                debugPrint("\(#function) ERROR: \(err.localizedDescription)")
                handler(false)
                return
            }
            }.resume()
    }
}
