//
//  ViewController.swift
//  extensions-party
//
//  Created by Juan Francisco Dorado Torres on 4/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var btnColorize: UIButton!
  @IBOutlet weak var btnWiggle: UIButton!
  @IBOutlet weak var btnDim: UIButton!

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  @IBAction func btnColorizeWasPressed(_ sender: Any) {
    btnColorize.colorize()
  }

  @IBAction func btnWiggleWasPressed(_ sender: Any) {
    btnWiggle.wiggle()
  }

  @IBAction func btnDimWasPressed(_ sender: Any) {
    btnDim.dim()
  }

}

