//
//  Helpers.swift
//  extensions-party
//
//  Created by Juan Francisco Dorado Torres on 4/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

func generateRandomNumbers(quantity: Int) -> [CGFloat] {
  var randomNumberArray = [CGFloat]()
  for _ in 1...quantity {
    let randomNumber = CGFloat(arc4random_uniform(255))
    randomNumberArray.append(randomNumber)
  }
  return randomNumberArray
}
