# Extensions Party

This app is an easy example to see how could be an approach to be using `Extensions` to new cool functions, in this case in a button, it let us play with new behaviors that we create for our buttons.

## Demo
![Demo](screenshots/extensions-party-demo.gif)
