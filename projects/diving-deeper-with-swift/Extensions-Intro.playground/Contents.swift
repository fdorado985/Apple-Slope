import UIKit

// Syntax (remember it needs an existing class)
class DummyClass {}

extension DummyClass {
  // Functionality
}

// Creating an Extension 
extension Int {
  func square() -> Int {
    return self * self
  }
}

var value = 9
value.square()

// Creating an Extension for String class
extension String {
  func reverse() -> String {
    var characterArray = [Character]()
    for character in self.characters {
      characterArray.insert(character, at: 0)
    }
    return String(characterArray)
  }
}

var name = "Marty McFly"
name.reverse()

// Creating an Extension for Double class
extension Double {
  mutating func calculateArea() {
    let pi = 3.1416
    self = pi * (self * self)
  }
}

class Circle {
  public private(set) var radius: Double

  init(radius: Double) {
    self.radius = radius
  }
}

var circle = Circle(radius: 3.3)
print(circle.radius)

circle.radius.calculateArea()
print(circle.radius)
