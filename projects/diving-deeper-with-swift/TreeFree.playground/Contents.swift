import Foundation

class Node {

  var value: String
  var children: [Node] = []
  weak var parent: Node?

  init(value: String) {
    self.value = value
  }

  func addChild(child: Node) {
    children.append(child)
    child.parent = self
  }
}

extension Node: CustomStringConvertible {

  var description: String {
    var text = "\(value)"
    if !children.isEmpty {
      text += " {" + children.map { $0.description }.joined(separator: ", ") + "} "
    }
    return text
  }
}

extension Node {

  func search(value: String) -> Node? {
    if value == self.value {
      return self
    }

    for child in children {
      if let found = child.search(value: value) {
        return found
      }
    }

    return nil
  }
}

let devices = Node(value: "Devices")

let appleDevices = Node(value: "Apple")
let androidDevices = Node(value: "Android")

devices.addChild(child: appleDevices)
devices.addChild(child: androidDevices)

let iPhoneDevices = Node(value: "iPhone")
let macbookDevices = Node(value: "MacBook")

appleDevices.addChild(child: iPhoneDevices)
appleDevices.addChild(child: macbookDevices)

androidDevices.addChild(child: Node(value: "Galaxy"))
androidDevices.addChild(child: Node(value: "Xperia"))

iPhoneDevices.addChild(child: Node(value: "iPhone X"))
iPhoneDevices.addChild(child: Node(value: "iPhone 8"))
iPhoneDevices.addChild(child: Node(value: "iPhone 4s"))

print(devices)

let iPhoneSearch = devices.search(value: "iPhone")
let galaxySearch = devices.search(value: "Galaxy")
