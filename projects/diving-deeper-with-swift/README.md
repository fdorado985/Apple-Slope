# Diving Deeper With Swift

You will see some interesting topics that will help you on your development.
There is gonna be playgrounds with simple examples of each.

## Topics
- Enumerations
- Extensions
  - [Extensions-Party](extensions-party/): Simple cute app using extensions to add new behaviors to buttons.
- Protocols|Delegates
  - [Color-Magic](color-magic/): Simple app that pass value to a previous VC using `delegates`.
  - [Eluxon](Eluxon/): Changing behaivor of an state with `mutating` functions.
- Recursive Functions
- Data Structures
  - Stack
  - Heap
  - Tree
