//: Playground - noun: a place where people can play

import Foundation

struct MinHeap {

  var items = [Int]()

  private func getLeftChildIndex(_ parentIndex: Int) -> Int {
    return 2 * parentIndex + 1
  }

  private func getRightChildIndex(_ parentIndex: Int) -> Int {
    return 2 * parentIndex + 2
  }

  private func getParentIndex(_ childIndex: Int) -> Int {
    return (childIndex - 1) / 2
  }

  private func hasLeftChild(_ index: Int) -> Bool {
    return getLeftChildIndex(index) < items.count
  }

  private func hasRightChild(_ index: Int) -> Bool {
    return getRightChildIndex(index) < items.count
  }

  private func hasParent(_ index: Int) -> Bool {
    return getParentIndex(index) >= 0
  }

  private func leftChild(_ index: Int) -> Int {
    return items[getLeftChildIndex(index)]
  }

  private func rightChild(_ index: Int) -> Int {
    return items[getRightChildIndex(index)]
  }

  private func parent(_ index: Int) -> Int {
    return items[getParentIndex(index)]
  }

  // Heap Operations

  private mutating func swap(indexOne: Int, indexTwo: Int) {
    let placeholder = items[indexOne]
    items[indexOne] = items[indexTwo]
    items[indexTwo] = placeholder
  }

  public func peek() -> Int {
    if !items.isEmpty {
      return items[0]
    } else {
      fatalError()
    }
  }

  public mutating func poll() -> Int {
    if !items.isEmpty {
      let item = items[0]
      items[0] = items[items.count - 1]
      heapifyDown()
      items.removeLast()
      return item
    } else {
      fatalError()
    }
  }

  public mutating func add(_ item: Int) {
    items.append(item)
    heapifyUp()
  }

  private mutating func heapifyUp() {
    var index = items.count - 1
    while hasParent(index) && parent(index) > items[index] {
      swap(indexOne: getParentIndex(index), indexTwo: index)
      index = getParentIndex(index)
    }
  }

  private mutating func heapifyDown() {
    var index = 0
    while hasLeftChild(index) {
      var smallerChildIndex = getLeftChildIndex(index)
      if hasRightChild(index) && rightChild(index) < leftChild(index) {
        smallerChildIndex = getRightChildIndex(index)
      }

      if items[index] < items[smallerChildIndex] {
        break
      } else {
        swap(indexOne: index, indexTwo: smallerChildIndex)
      }
      index = smallerChildIndex
    }
  }
}

var myHeap = MinHeap()

myHeap.add(2)
myHeap.add(3)
myHeap.add(8)
myHeap.add(4)
myHeap.add(7)
myHeap.add(13)
myHeap.add(18)

dump(myHeap.items)

myHeap.poll()

dump(myHeap.items)

myHeap.poll()

dump(myHeap.items)

myHeap.poll()

dump(myHeap.items)
