import UIKit

// Basic Syntax and instance ===========
// Syntax
enum NameOfEnum {
  case caseOne
  case caseTwo
  case caseThree
}

// Syntax (cases in one line)
enum AnotherEnum {
  case caseOne, caseTwo, caseThree
}

// Instanciate
let enumeration: NameOfEnum = .caseTwo
// ======================================


// EXAMPLE (using associate values on Enums)
enum Barcode {
  case upc(Int, Int, Int, Int)
  case qrCode(String)
}


var productBarcode = Barcode.upc(8, 85909, 51226, 3)
var productBarcode2 = Barcode.qrCode("www.qrcodeme.com")

func printBarcode(product: Barcode) {
  switch product {
  case let .upc(numberSystem, manufacturer, product, check):
    print("UPC: \(numberSystem) \(manufacturer) \(product) \(check)")
  case let .qrCode(productCode):
    print("QR: \(productCode)")
  }
}

printBarcode(product: productBarcode)
printBarcode(product: productBarcode2)



// EXAMPLE (using explicit value)
enum JediMaster: String {
  case yoda = "Yoda"
  case maceWindu = "Mace Windu"
  case quiGonJinn = "Qui-Gon Jinn"
  case obiWanKenobi = "Obi-Wan Kenobi"
  case lukeSkywalker = "Luke Skywalker"
}

print(JediMaster.yoda.rawValue)


// EXAMPLE
enum SwitchStatus {
  case on
  case off
}

var switchStatus: SwitchStatus = .off

func flipSwith(status: SwitchStatus) -> SwitchStatus {
  return status == .off ? .on : .off
}

flipSwith(status: switchStatus)

switchStatus = .on
flipSwith(status: switchStatus)
