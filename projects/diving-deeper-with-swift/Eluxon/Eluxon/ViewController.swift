//
//  ViewController.swift
//  Eluxon
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var lblOnOff: UILabel!
  @IBOutlet weak var btnToggle: UIButton!

  // MARK: - Properties

  var switchStatus: SwitchStatus = .off

  // MARK: - Initialization

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: - IBActions

  @IBAction func btnToggleTapped(_ sender: UIButton) {
    switchStatus.toggle()
    if switchStatus == .off {
      btnToggle.setImage(UIImage(named: "offBtn")!, for: .normal)
      view.backgroundColor = UIColor.darkGray
      lblOnOff.text = "🌚 OFF 🌚"
      lblOnOff.textColor = UIColor.white
    } else {
      btnToggle.setImage(UIImage(named: "onBtn")!, for: .normal)
      view.backgroundColor = UIColor.white
      lblOnOff.text = "🌝 ON 🌝"
      lblOnOff.textColor = UIColor.darkGray
    }
  }
}

