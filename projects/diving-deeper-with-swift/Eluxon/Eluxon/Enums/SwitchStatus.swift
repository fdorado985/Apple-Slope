//
//  SwitchStatus.swift
//  Eluxon
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

enum SwitchStatus: Togglable {

  case on, off

  mutating func toggle() {
    switch self {
    case .on:
      self = .off
    case .off:
      self = .on
    }
  }
}
