//
//  Togglable.swift
//  Eluxon
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

protocol Togglable {
  mutating func toggle()
}
