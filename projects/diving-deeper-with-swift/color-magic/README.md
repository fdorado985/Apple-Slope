#  Color Magic

Here we are be using `protocols` and `delegates` to control a choosen color of a list to change the Background color on the main view and the label with its respective color.

## Demo
![Color-Magic-Demo](screenshots/color_magic_demo.gif)
