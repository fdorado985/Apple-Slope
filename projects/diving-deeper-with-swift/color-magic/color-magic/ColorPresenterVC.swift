//
//  ColorPresenterVC.swift
//  color-magic
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ColorPresenterVC: UIViewController {

  @IBOutlet weak var lblColorName: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ColorPickerVC" {
      guard let colorPickerVC = segue.destination as? ColorPickerVC else { return }
      colorPickerVC.delegate = self
    }
  }
}

extension ColorPresenterVC: ColorTransferDelegate {

  func userDidChoose(_ color: UIColor, withName name: String) {
    lblColorName.text = name
    view.backgroundColor = color
  }
}

