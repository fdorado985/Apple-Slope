//
//  ColorPickerVC.swift
//  color-magic
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ColorPickerVC: UIViewController {

  var delegate: ColorTransferDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  @IBAction func btnColorTapped(_ sender: UIButton) {
    guard let delegate = delegate, let colorName = sender.titleLabel?.text, let backgroundColor = sender.backgroundColor else { return }
    delegate.userDidChoose(backgroundColor, withName: colorName)
    navigationController?.popViewController(animated: true)
  }
}
