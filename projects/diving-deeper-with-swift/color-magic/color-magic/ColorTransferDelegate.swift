//
//  ColorTransferDelegate.swift
//  color-magic
//
//  Created by Juan Francisco Dorado Torres on 4/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol ColorTransferDelegate {
  func userDidChoose(_ color: UIColor, withName name: String)
}
