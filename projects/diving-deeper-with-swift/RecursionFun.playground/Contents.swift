import UIKit

// Using Loops
func printNumbers(max: Int) {
  for x in 0...max {
    print(x)
  }
}

printNumbers(max: 3)

// Using Recursion
func printNumbersRec(max: Int) {
  if max > 0 {
    printNumbersRec(max: max - 1)
  }

  print(max)
}

printNumbersRec(max: 4)


// Factorials of non negative integer
// 1! = 1
// 2! = 1 * 2 = 2
// 3! = 1 * 2 * 3 = 6

func getLoopFactorial(of num: Int) -> Int {
  var factorial = 0
  for x in 1...num {
    if factorial != 0 {
      factorial = factorial * x
    } else {
      factorial = 1
    }
  }
  return factorial
}

func getRecursiveFactorial(of num: Int) -> Int {
  if num == 1 {
    return 1
  } else {
    return num * getRecursiveFactorial(of: num - 1)
  }
}

getLoopFactorial(of: 7)
getRecursiveFactorial(of: 7)
