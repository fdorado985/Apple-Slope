//
//  AuthService.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Firebase

class AuthService {
    
    // MARK: Singleton
    
    static let instance = AuthService()
    
    // MARK: Public
    
    func registerUser(email: String, password: String, completion: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { (authData, error) in
            guard let authData = authData else {
                completion(false, error)
                return
            }
            
            let userData: [String : Any] = ["provider" : authData.user.providerID as Any, "email" : authData.user.email as Any]
            DataService.instance.createDBUser(uid: authData.user.uid, userData: userData)
            completion(true, nil)
        }
    }
    
    func loginUser(email: String, password: String, completion: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (authData, error) in
            if let error = error {
                completion(false, error)
                return
            }
            completion(true, nil)
        }
    }
}
