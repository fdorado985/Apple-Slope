//
//  DataService.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Firebase

class DataService {
    
    // MARK: Singleton
    
    static let instance = DataService()
    
    // MARK: Properties
    
    private let REF_BASE = DB_BASE
    private let REF_USERS = DB_BASE.child("users")
    private(set) var REF_GROUPS = DB_BASE.child("groups")
    private let REF_FEED = DB_BASE.child("feed")
    
    // MARK: Functions
    
    func createDBUser(uid: String, userData: [String: Any]) {
        REF_USERS.child(uid).setValue(userData)
    }
    
    func uploadPost(message: String, uid: String, groupKey: String?, completion: @escaping (_ status: Bool) -> ()) {
        if let groupKey = groupKey {
            REF_GROUPS.child(groupKey).child("messages").childByAutoId().updateChildValues(["content" : message, "senderId" : uid])
            completion(true)
        } else {
            REF_FEED.childByAutoId().updateChildValues(["content" : message, "senderId" : uid])
            completion(true)
        }
    }
    
    func getAllFeedMessages(completion: @escaping (_ message: [Message]) -> ()) {
        var messageArray = [Message]()
        REF_FEED.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for snapshot in snapshotObjects {
                if let content = snapshot.childSnapshot(forPath: "content").value as? String,
                    let senderId = snapshot.childSnapshot(forPath: "senderId").value as? String {
                    let message = Message(content: content, senderId: senderId)
                    messageArray.append(message)
                }
            }
            completion(messageArray)
        }
    }
    
    func getAllMessages(of group: Group, completion: @escaping (_ messages: [Message]) -> ()) {
        var messages = [Message]()
        REF_GROUPS.child(group.key).child("messages").observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for snapshot in snapshotObjects {
                if let content = snapshot.childSnapshot(forPath: "content").value as? String,
                    let senderId = snapshot.childSnapshot(forPath: "senderId").value as? String {
                    let message = Message(content: content, senderId: senderId)
                    messages.append(message)
                }
            }
            completion(messages)
        }
    }
    
    func getUsername(uid: String, completion: @escaping (_ username: String) -> ()) {
        REF_USERS.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for snapshot in snapshotObjects {
                guard let email = snapshot.childSnapshot(forPath: "email").value as? String else { continue }
                if snapshot.key == uid {
                    completion(email)
                }
            }
        }
    }
    
    func getEmail(searchQuery query: String, completion: @escaping (_ emails: [String]) -> ()) {
        var emails = [String]()
        REF_USERS.observe(.value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            guard let currentUser = Auth.auth().currentUser else { return }
            for snapshot in snapshotObjects {
                guard let email = snapshot.childSnapshot(forPath: "email").value as? String else { continue }
                if email.contains(query) && email != currentUser.email {
                    emails.append(email)
                }
            }
            completion(emails)
        }
    }
    
    func getIDs(for usernames: [String], completion: @escaping (_ uids: [String]) -> ()) {
        REF_USERS.observeSingleEvent(of: .value) { (dataSnapshot) in
            var ids = [String]()
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for snapshot in snapshotObjects {
                guard let email = snapshot.childSnapshot(forPath: "email").value as? String else { continue }
                if usernames.contains(email) {
                    ids.append(snapshot.key)
                }
            }
            completion(ids)
        }
    }
    
    func createGroup(title: String, description: String, userIds: [String], completion: @escaping (_ success: Bool) -> ()) {
        REF_GROUPS.childByAutoId().updateChildValues(["title" : title, "description" : description, "members" : userIds])
        completion(true)
    }
    
    func getAllGroups(completion: @escaping (_ groups: [Group]) -> ()) {
        var groups = [Group]()
        REF_GROUPS.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            guard let currentUser = Auth.auth().currentUser else { return }
            for snapshot in snapshotObjects {
                guard let members = snapshot.childSnapshot(forPath: "members").value as? [String] else { continue }
                if members.contains(currentUser.uid) {
                    guard let title = snapshot.childSnapshot(forPath: "title").value as? String else { return }
                    guard let description = snapshot.childSnapshot(forPath: "description").value as? String else { return }
                    let group = Group(key: snapshot.key, title: title, description: description, members: members)
                    groups.append(group)
                }
            }
            completion(groups)
        }
    }
    
    func getEmails(for group: Group, completion: @escaping (_ emails: [String]) -> ()) {
        var emails = [String]()
        REF_USERS.observeSingleEvent(of: .value) { (dataSnapshot) in
            guard let snapshotObjects = dataSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for snapshot in snapshotObjects {
                if group.members.contains(snapshot.key) {
                    guard let email = snapshot.childSnapshot(forPath: "email").value as? String else { return }
                    emails.append(email)
                }
            }
            completion(emails)
        }
    }
}
