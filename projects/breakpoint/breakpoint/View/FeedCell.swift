//
//  FeedCell.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    // MARK: Functions
    
    func configureCell(profileImage: UIImage, email: String, content: String) {
        imgProfile.image = profileImage
        lblEmail.text = email
        lblContent.text = content
    }
}
