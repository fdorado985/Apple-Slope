//
//  InsetTextField.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class InsetTextField: UITextField {
    
    // MARK: Properties
    
    private var padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    
    // MARK: View cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    // MARK: Private
    
    private func setupView() {
        let placeholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
        self.attributedPlaceholder = placeholder
    }
    
    // MARK: UITextField Rect
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}
