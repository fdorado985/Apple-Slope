//
//  GroupCell.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/4/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblMembers: UILabel!
    
    // MARK: Public
    
    func configureCell(title: String, description: String, members: Int) {
        lblTitle.text = title
        lblDescription.text = description
        lblMembers.text = "Members: \(members)"
    }
    
}
