//
//  UserCell.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/2/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imgCheckmark: UIImageView!
    
    // MARK: Properties
    
    private var isShowing = false
    var email: String {
        return lblEmail.text ?? ""
    }
    
    // MARK: Functions
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            if !isShowing {
                imgCheckmark.isHidden = false
                isShowing = true
            } else {
                imgCheckmark.isHidden = true
                isShowing = false
            }
        }
    }
    
    // MARK: Public
    
    func configureCell(profileImg: UIImage, email: String, isSelected: Bool) {
        self.imgProfile.image = profileImg
        self.lblEmail.text = email
        self.imgCheckmark.isHidden = !isSelected
    }

}
