//
//  GroupFeedCell.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/4/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class GroupFeedCell: UITableViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak private var imgProfile: UIImageView!
    @IBOutlet weak private var lblEmail: UILabel!
    @IBOutlet weak private var lblContent: UILabel!
    
    // MARK: Public
    
    func configureCell(imgProfile: UIImage, email: String, content: String) {
        self.imgProfile.image = imgProfile
        self.lblEmail.text = email
        self.lblContent.text = content
    }
}
