//
//  GroupFeedVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/7/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class GroupFeedVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var lblGroupTitle: UILabel!
    @IBOutlet weak var lblMembers: UILabel!
    @IBOutlet weak var tableFeed: UITableView!
    @IBOutlet weak var viewSendContent: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    // MARK: Properties
    
    var group: Group?
    private var messages = [Message]()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSendContent.bindToKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let group = self.group else { return }
        lblGroupTitle.text = group.title
        DataService.instance.getEmails(for: group) { (emails) in
            self.lblMembers.text = emails.joined(separator: ", ")
        }
        
        DataService.instance.REF_GROUPS.observe(.value) { (snapshot) in
            DataService.instance.getAllMessages(of: group, completion: { (messages) in
                self.messages = messages
                self.tableFeed.reloadData()
                
                if self.messages.count > 0 {
                    self.tableFeed.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .none, animated: true)
                }
            })
        }
    }

    // MARK: IBActions
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        dismissDetail()
    }
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        guard let message = txtMessage.text, !message.isEmpty else { return }
        guard let currentUser = Auth.auth().currentUser else { return }
        guard let group = self.group else { return }
        txtMessage.isEnabled = false
        btnSend.isEnabled = false
        DataService.instance.uploadPost(message: message, uid: currentUser.uid, groupKey: group.key) { (success) in
            if success {
                self.txtMessage.isEnabled = true
                self.txtMessage.text = ""
                self.btnSend.isEnabled = true
            }
        }
    }
    
    // MARK: Private
}

extension GroupFeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupFeedCell", for: indexPath) as? GroupFeedCell else { return UITableViewCell() }
        let message = messages[indexPath.row]
        DataService.instance.getUsername(uid: message.senderId) { (email) in
            cell.configureCell(imgProfile: #imageLiteral(resourceName: "defaultProfileImage"), email: email, content: message.content)
        }
        return cell
    }
    
    
}
