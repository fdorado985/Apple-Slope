//
//  LoginVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: IBActions
    
    @IBAction func btnSignInTapped(_ sender: UIButton) {
        guard let email = txtEmail.text, !email.isEmpty, let password = txtPassword.text, !password.isEmpty else { return }
        login(email: email, password: password)
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: Functions
    
    private func login(email: String, password: String) {
        AuthService.instance.loginUser(email: email, password: password) { (success, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                self.register(email: email, password: password)
            } else if success {
                self.dismiss(animated: true)
            }
        }
    }
    
    private func register(email: String, password: String) {
        AuthService.instance.registerUser(email: email, password: password, completion: { (success, registrationError) in
            if let error = registrationError {
                debugPrint(error.localizedDescription)
            } else if success {
                self.login(email: email, password: password)
            }
        })
    }
}

extension LoginVC: UITextFieldDelegate {
    
    
}
