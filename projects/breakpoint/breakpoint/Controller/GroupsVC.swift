//
//  GroupsVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class GroupsVC: UIViewController {
    
    // MARK: IBOutlets

    @IBOutlet weak var tableGroups: UITableView!
    
    // MARK: Properties
    
    private var groups = [Group]()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.getAllGroups { (groups) in
            self.groups = groups
            self.tableGroups.reloadData()
        }
    }
}

extension GroupsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as? GroupCell else { return UITableViewCell() }
        let group = groups[indexPath.row]
        cell.configureCell(title: group.title, description: group.description, members: group.membersCount)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "GroupFeedVC") as? GroupFeedVC else { return }
        let group = groups[indexPath.row]
        vc.group = group
        presentDetail(vc)
    }
    
    
}
