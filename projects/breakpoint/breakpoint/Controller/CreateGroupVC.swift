//
//  CreateGroupVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class CreateGroupVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak private var txtTitle: UITextField!
    @IBOutlet weak private var txtDescription: UITextField!
    @IBOutlet weak private var txtEmailSearch: UITextField!
    @IBOutlet weak private var lblGroupMember: UILabel!
    @IBOutlet weak private var tableEmails: UITableView!
    @IBOutlet weak private var btnDone: UIButton!
    
    // MARK: Properties
    
    private var emails = [String]()
    private var chosenUsers = [String]()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmailSearch.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    // MARK: IBActions
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        guard let title = txtTitle.text, !title.isEmpty else { return }
        guard let description = txtDescription.text, !description.isEmpty else { return }
        DataService.instance.getIDs(for: chosenUsers) { (ids) in
            var userIds = ids
            guard let currentUser = Auth.auth().currentUser else { return }
            userIds.append(currentUser.uid)
            
            DataService.instance.createGroup(title: title, description: description, userIds: userIds, completion: { (success) in
                if success {
                    self.dismiss(animated: true)
                } else {
                    debugPrint("Error creating group")
                }
            })
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: Private
    
    @objc private func textFieldDidChange() {
        guard let query = txtEmailSearch.text, !query.isEmpty else {
            emails = []
            tableEmails.reloadData()
            return
        }
        
        DataService.instance.getEmail(searchQuery: query) { (emails) in
            self.emails = emails
            self.tableEmails.reloadData()
        }
    }
    
}

extension CreateGroupVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell else { return UITableViewCell() }
        let profileImage = #imageLiteral(resourceName: "defaultProfileImage")
        let email = emails[indexPath.row]
        let containsEmail = chosenUsers.contains(email)
        cell.configureCell(profileImg: profileImage, email: email, isSelected: containsEmail ? true : false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? UserCell else { return }
        if !chosenUsers.contains(cell.email) {
            chosenUsers.append(cell.email)
            lblGroupMember.text = chosenUsers.joined(separator: ", ")
            btnDone.isHidden = false
        } else {
            chosenUsers = chosenUsers.filter { $0 != cell.email }
            lblGroupMember.text = chosenUsers.count >= 1 ? chosenUsers.joined(separator: ", ") : "add people to your group"
            btnDone.isHidden = chosenUsers.count == 0
        }
    }
}

extension CreateGroupVC: UITextFieldDelegate {
    
}
