//
//  CreatePostVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class CreatePostVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtPost: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSend.bindToKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblEmail.text = Auth.auth().currentUser?.email
    }

    // MARK: IBActions
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        guard let postMessage = txtPost.text, postMessage != "Say something here...", let uid = Auth.auth().currentUser?.uid else { return }
        btnSend.isEnabled = false
        DataService.instance.uploadPost(message: postMessage, uid: uid, groupKey: nil) { (success) in
            if success {
                self.btnSend.isEnabled = true
                self.dismiss(animated: true)
            } else {
                self.btnSend.isEnabled = true
                debugPrint("There was an error posting")
            }
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

extension CreatePostVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
}
