//
//  MeVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblEmail.text = Auth.auth().currentUser?.email
    }

    // MARK: IBActions
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .actionSheet)
        let btnLogoutAction = UIAlertAction(title: "Logout", style: .destructive) { (action) in
            do {
                try Auth.auth().signOut()
                guard let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? AuthVC else { return }
                self.present(loginVC, animated: true)
            } catch let error {
                debugPrint("Couldn't be able to log out: \(error.localizedDescription)")
            }
        }
        let btnCancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(btnLogoutAction)
        alert.addAction(btnCancelAction)
        present(alert, animated: true)
    }
}
