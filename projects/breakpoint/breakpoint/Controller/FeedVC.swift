//
//  FeedVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class FeedVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var tableFeeds: UITableView!
    
    // MARK: Properties
    
    private var messages = [Message]()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.getAllFeedMessages { (messages) in
            self.messages = messages.reversed()
            self.tableFeeds.reloadData()
        }
    }
}

extension FeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath) as? FeedCell else { return UITableViewCell() }
        let profileImage = #imageLiteral(resourceName: "defaultProfileImage")
        let message = messages[indexPath.row]
        
        DataService.instance.getUsername(uid: message.senderId) { (email) in
            cell.configureCell(profileImage: profileImage, email: email, content: message.content)
        }
        
        return cell
    }
}

