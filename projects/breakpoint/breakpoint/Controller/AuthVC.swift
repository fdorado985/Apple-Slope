//
//  AuthVC.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 8/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Firebase

class AuthVC: UIViewController {

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            dismiss(animated: true)
        }
    }

    // MARK: IBActions
    
    @IBAction func btnEmailSignInTapped(_ sender: UIButton) {
        guard let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") else { return }
        present(loginVC, animated: true)
    }
    
    @IBAction func btnFacebookSignInTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnGoogleSignInTapped(_ sender: UIButton) {
    }
}
