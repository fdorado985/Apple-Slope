//
//  UIView+Ext.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIView {
    
    func bindToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc private func keyboardWillChange(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double else { return }
        guard let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt else { return }
        guard let beginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else { return }
        guard let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        let deltaY = endFrame.origin.y - beginFrame.origin.y
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0.0,
            options: UIViewKeyframeAnimationOptions(rawValue: curve),
            animations: {
                self.frame.origin.y += deltaY
            },
            completion: nil)
    }
}
