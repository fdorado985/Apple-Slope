//
//  Group.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/4/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Group {
    
    // MARK: Properties
    
    private(set) var title: String
    private(set) var description: String
    private(set) var members: [String]
    private(set) var key: String
    
    var membersCount: Int {
        return members.count
    }
    
    // MARK: Init
    
    init(key: String, title: String, description: String, members: [String]) {
        self.key = key
        self.title = title
        self.description = description
        self.members = members
    }
}
