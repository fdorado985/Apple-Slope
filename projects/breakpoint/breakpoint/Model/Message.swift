//
//  Message.swift
//  breakpoint
//
//  Created by Juan Francisco Dorado Torres on 9/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Message {
    
    // MARK: Properties
    
    private(set) var content: String
    private(set) var senderId: String
    
    // MARK: Init
    
    init(content: String, senderId: String) {
        self.content = content
        self.senderId = senderId
    }
}
