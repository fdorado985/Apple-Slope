# Breakpoint
This app works like a Facebook Wall... where you can be able to write some posts, log in, create accounts.

## Wrap Up
On this app you will see...
* Firebase
* Cocoapods
* Extensions
* Custom Views
* Custom Transitions

## Demo
![breakpoint_demo](screenshots/breakpoint_demo.gif)
