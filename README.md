<p align="center">
  <img src="Assets/main/logo.png" title="Logo">
</p>

---

[![platform_ios](Assets/main/platform.png)](https://www.apple.com/ios/ios-11-preview/)
[![ide_xcode9](Assets/main/ide.png)](https://developer.apple.com/xcode)
[![twitter](Assets/main/twitter.png)](https://twitter.com/fdorado985)
[![instagram](Assets/main/instagram.png)](https://www.instagram.com/juan_fdorado)

On this repo we are going to save some little projects, that we help us to learn the most basics steps of iOS Development, there is gonna be a `README` file to explain some little stuffs on each project.

I invite you to take a look at them

Here are the projects and a little description of what are they made it for. Enjoy it and most important KEEP CODING!

All of this project has been worked day by day, month by month learning new stuffs from iOS Development from different sources.

So here is a big collection! 👍

## iOS 12 & Swift
* [Hustle-Mode](projects/hustle-mode) - Getting Started with iOS 12
* [The-Basics](projects/the-basics) - Intro to Programming & Swift
* [Version Control](projects/git_version_control) - `Git` & `Version Control`
* [Swoosh](projects/swoosh) - Intro to `Interface Builder` & `Auto Layout`
* [Dev-Profile](projects/dev-profile) - Digging Deeper With `Auto Layout`
* [Window-Shopper](projects/window-shooper) - Fully Functional iOS App
* [Coder-Swag](projects/coder-swag) - `Tables` and `Data` on iOS
* [Intro-To-MVC](projects/Intro-To-MVC) - Intro To `Model View Controller`
* [XYX-Machine](projects/xyz-machine) - Working with iOS `Accelerometer`
* [Splitter](projects/splitter) - Working with `UISplitViewController`
* [Smack](projects/smack) - Creating a Slack App & Working With `APIs` and `Sockets`
* [Pixel-City](projects/pixel-city) - Mastering `Maps` in iOS
* [GoalPost-App](projects/goalpost-app) - Data Persistence in iOS With `CoreData`
* [Treads](projects/treads) - Data Persistence in iOS With `Realm`
* [Breakpoint](projects/breakpoint) - Building a Full Stack App With `Firebase`
* [RNDM](projects/rndm) - Using the **NEW** `Firebase Cloud Firestore`
* [Foodzilla](projects/foodzilla) - Mastering iOS `In-App Purchases`
* [Secure-Notes](projects/secure-notes) - Working With iOS `TouchID` and `FaceID`.
* [Scribe](projects/scribe) - Working With iOS `Speech Recognition API`.
* [Stickerzilla](projects/Stickerzilla) - iOS `Sticker Packs` For `iMessage`
* [Shortcut](projects/shortcut) - Working With `3D Touch` on iOS.
* [Screenie](projects/screenie) - Using `ReplayKit` in iOS.
* [Binary](projects/binary) - Computers & Code: Crash Course in Computer Science for Beginners.
* [Diving Deeper With Swift](projects/diving-deeper-with-swift) - Advanced `Swift 4`, `Data Structures` & `Algorithms`.
* [Protocol-Oriented-Programming](projects/protocol-oriented-programming) - Advanced Design Patterns: `Protocol Oriented Programming`.

## Advanced iOS & Firebase: Uber Clone App
* [HTCHHKR](projects/htchhkr) - An advanced and complex app to clone Uber... `MapKit`, `CLLocations`, `Apple Maps`, `Annotations`... and much more.

## Comprehensive macOS Development
* [Weathered](projects/weathered) - Weather Status App
* [Smack](projects/smack-macos) - Creating a Slack App & Working With `APIs` and `Sockets`

## Target Topics
* [Daily-Dose](projects/daily-dose) - Intro to `In-App Purchases` and `Ads`
* [Accelerometron](projects/accelerometron) - `Accelerometer` for a `Parallax Effect`
* [TestWatch](projects/testWatch) - This is not an app... but it includes some rules and basics about `Apple Watch Development`.
* [RunningMan](projects/RunningMan) - This is a simple app that track your running... It is not an iOS App... it is an `Apple Watch`!.
* [Mapbox-Navigation](projects/mapbox-navigation-demo) - `Mapbox` (third-party) the new `MapKit`.

## Test Driven iOS Development
* [First Demo](projects/test-driven-ios-development/FirstDemo) - Create the first TDD demo app.
* [To-Do](projects/test-driven-ios-development/todo_tdd) - Simple TODO App, Using TDD.
